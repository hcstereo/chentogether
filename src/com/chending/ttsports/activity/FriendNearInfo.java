package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendNearVO;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.util.AsyncImageLoader;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FriendNearInfo extends Activity implements OnClickListener{
	private ImageView iv_info_head;
	private TextView tv_info_username,tv_info_dis,tv_info_sports_type,tv_info_rundis,tv_info_gender;
	private TextView tv_info_hobby,tv_info_signature;
	private LinearLayout lin_info_add,lin_info_chat,lin_info_back,lin_info_sendmsg,lin_info_not_friend;
	private Dialog addFriendloadingDialog;
	private FriendService friendService;
	private SharedPreferencesTool preferencesTool;
	private FriendNearVO friendNear;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friendnear_info);
		init();
		addListener();
	}
	private void init(){
		friendService=new FriendService(this);
		addFriendloadingDialog=new Dialog(this,R.style.FullHeightDialog);
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(this);
		iv_info_head=(ImageView) findViewById(R.id.iv_info_head);
		tv_info_username=(TextView) findViewById(R.id.tv_info_username);
		tv_info_dis=(TextView) findViewById(R.id.tv_info_dis);
		tv_info_sports_type=(TextView) findViewById(R.id.tv_info_sports_type);
		tv_info_rundis=(TextView) findViewById(R.id.tv_info_rundis);
		tv_info_gender=(TextView) findViewById(R.id.tv_info_gender);
		tv_info_hobby=(TextView) findViewById(R.id.tv_info_hobby);
		tv_info_signature=(TextView)findViewById(R.id.tv_info_signature);
		lin_info_add=(LinearLayout) findViewById(R.id.lin_info_add);
		lin_info_chat=(LinearLayout) findViewById(R.id.lin_info_chat);
		lin_info_back=(LinearLayout) findViewById(R.id.lin_info_back);
		lin_info_sendmsg=(LinearLayout) findViewById(R.id.lin_info_sendmsg);
		lin_info_not_friend=(LinearLayout) findViewById(R.id.lin_info_not_friend);
		//初始化数据显示
		friendNear=(FriendNearVO) getIntent().getSerializableExtra("friendNear");
		setBottomBar(friendNear.getUserId());
		AsyncImageLoader asyncImageLoader=new AsyncImageLoader();
		if(friendNear!=null){
			asyncImageLoader.loadImage(friendNear.getAvatar(), iv_info_head);
			tv_info_username.setText(friendNear.getUsername());
			tv_info_dis.setText(String.valueOf(new Integer((int)friendNear.getDisFrom())));
			String typrStr=null;
			switch (friendNear.getRectype()) {
			case 0:
				typrStr="运动 ";
				break;
			case 1:
				typrStr="步行 ";
				break;
			case 2:
				typrStr="跑步 ";
				break;
			case 3:
				typrStr="滑冰 ";
				break;
			case 4:
				typrStr="滑雪 ";
				break;
			case 5:
				typrStr="骑行 s";
				break;
			}
			tv_info_sports_type.setText(typrStr);
			tv_info_rundis.setText(String.valueOf(new Integer((int)friendNear.getDistanceRecent())));
			tv_info_gender.setText(friendNear.getGender());
			tv_info_hobby.setText(friendNear.getHobby());
			tv_info_signature.setText(friendNear.getSignature());
		}
	}
	private void addListener(){
		lin_info_add.setOnClickListener(this);
		lin_info_chat.setOnClickListener(this);
		lin_info_back.setOnClickListener(this);
		lin_info_sendmsg.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.lin_info_add:
			addFriendloadingDialog.show();
			addFriendloadingDialog.setContentView(R.layout.activity_login_loading_dialog_view);
			addFriendloadingDialog.setCancelable(false);
			friendService.addFriendService(Integer.valueOf(preferencesTool.getUserPrefences().get("userId")),friendNear.getUserId(),addFriendloadingDialog);
			break;
		case R.id.lin_info_chat:
			Intent intent1=new Intent(FriendNearInfo.this,ChatActivity.class);
			Bundle bundle1=new Bundle();
			bundle1.putString("username", friendNear.getUsername());
			bundle1.putInt("friendId", friendNear.getUserId());
			bundle1.putString("avatarOp", friendNear.getAvatar());
			intent1.putExtras(bundle1);
			startActivity(intent1);
			break;
		case R.id.lin_info_back:
			finish();
			break;
		case R.id.lin_info_sendmsg:
			Intent intent=new Intent(FriendNearInfo.this,ChatActivity.class);
			Bundle bundle=new Bundle();
			bundle.putString("username", friendNear.getUsername());
			bundle.putInt("friendId", friendNear.getUserId());
			bundle.putString("avatarOp", friendNear.getAvatar());
			intent.putExtras(bundle);
			startActivity(intent);
			break;
		default:
			break;
		}
	}
	
	private void setBottomBar(int userId){
		for(FriendsVO friendsVO:FriendService.friendsVOs){//查到的是自己的朋友
			if(friendsVO.getFriendsId()==friendNear.getUserId()){
				lin_info_not_friend.setVisibility(View.GONE);
				lin_info_sendmsg.setVisibility(View.VISIBLE);
				break;
			}
		}
	}
}
