package com.chending.ttsports.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.chaowen.yixin.R;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.service.HistoryService;
import com.chending.ttsports.util.AsyncImageLoader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentHistory extends Fragment implements OnItemClickListener,OnClickListener{
	public static final String TAG= "FragmentHistory";
	private ListView lv_his_list;
	private HistoryService historyService;
	private SharedPreferencesTool preferencesTool;
	private TextView tv_his_username,tv_his_sum_dis,tv_his_sum_time,tv_his_sum_cal;
	private ImageView headImageView;
	private List<TextView> textViews=new ArrayList<TextView>();
	public static boolean isListNeedRef=false;
     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.fragment_history, container, false);
    	init(view);
    	addListener();
    	return view;
    }
     private void init(View view){
    	 tv_his_username=(TextView) view.findViewById(R.id.tv_his_username);
    	 headImageView=(ImageView) view.findViewById(R.id.headImageView);
    	 tv_his_sum_dis=(TextView) view.findViewById(R.id.tv_his_sum_dis);
    	 tv_his_sum_time=(TextView) view.findViewById(R.id.tv_his_sum_time);
    	 tv_his_sum_cal=(TextView) view.findViewById(R.id.tv_his_sum_cal);
    	 historyService=new HistoryService((MainActivity)getActivity());
    	 textViews.add(tv_his_sum_dis);
    	 textViews.add(tv_his_sum_time);
    	 textViews.add(tv_his_sum_cal);
    	 lv_his_list=(ListView) view.findViewById(R.id.lv_his_list);
    	 preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(getActivity());
    	 Map<String, String> map=preferencesTool.getUserPrefences();
    	 String sumDis=map.get("sumDis");
    	 if(sumDis==null){
    		 sumDis="0";
    	 }
    	 String sumTime=map.get("sumTime");
    	 if(sumTime==null){
    		 sumTime="0";
    	 }
    	 String sumCal=map.get("sumCal");
    	 if(sumCal==null){
    		 sumCal="0";
    	 }
    	 String username=map.get("username");
    	 String avatar=map.get("avatar");
    	 tv_his_sum_dis.setText(sumDis);
    	 tv_his_sum_time.setText(sumTime);
    	 tv_his_sum_cal.setText(sumCal);
    	 tv_his_username.setText(username);
    	 AsyncImageLoader asyncImageLoader=new AsyncImageLoader();
    	 asyncImageLoader.loadImage(avatar, headImageView);
    	 historyService.historyService(Integer.parseInt(map.get("userId")), lv_his_list,textViews);
    	 
     }
     private void addListener(){
    	 lv_his_list.setOnItemClickListener(this);
     }
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		default:
			break;
		}
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
	}
	@Override
	public void onResume() {
		super.onResume();
		if(isListNeedRef){
			Map<String, String> map=preferencesTool.getUserPrefences();
			historyService.historyService(Integer.parseInt(map.get("userId")), lv_his_list,textViews);
		}
	}
}
