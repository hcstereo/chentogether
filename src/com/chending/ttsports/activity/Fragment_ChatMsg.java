
package com.chending.ttsports.activity;
import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.FriendMsg;
import com.chending.ttsports.service.ChatService;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.swiplistview.BaseSwipeListViewListener;
import com.chending.ttsports.swiplistview.SwipeListView;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class Fragment_ChatMsg extends Fragment {
	public static final String TAG="Fragment_ChatMsg";
	private SwipeListView lv_msg_list;
	private String username;
	private int userId;
	private ChatService chatService;
//	public static View headerViewSearch;
	public static String friendName;
	public static int friendId;
	private LinearLayout ll_constact_serach;
	private Button btn_msg_delete;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_chatmsg, container,
				false);
		Constant.IS_MSG_THREAD_SHOULD_END=false;
		Constant.TAG_ENTER_CHACK_DATA=-1;
		init(view);
		ChatService.friendMsgs.clear();
		return view;
	}

	private void init(View view) {
		SharedPreferencesTool preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(getActivity());
		chatService=new ChatService(getActivity());
		username=preferencesTool.getUserPrefences().get("username");
		userId=Integer.parseInt(preferencesTool.getUserPrefences().get("userId"));
		username=SharedPreferencesTool.getSharedPreferencesToolInstance(getActivity()).getUserPrefences().get("username");
		lv_msg_list = (SwipeListView) view.findViewById(R.id.lv_msg_list);
//		headerViewSearch = LayoutInflater.from(getActivity()).inflate(
//				R.layout.msg_search, null);
//		ll_constact_serach=(LinearLayout) headerViewSearch.findViewById(R.id.ll_constact_serach);
//		lv_msg_list.addHeaderView(headerViewSearch);// 给listview添加头部
		// list的数据如果没有新的消息，那么应该是从本地数据库获取的信息
		lv_msg_list.setSwipeListViewListener(new BaseSwipeListViewListener(){

			@Override
			public void onClickFrontView(int position) {
				super.onClickFrontView(position);
				Constant.TAG_ENTER_CHACK_DATA=0;
				Constant.IS_MSG_THREAD_SHOULD_END=true;
				FriendMsg friendMsg=ChatService.friendMsgs.get(position);
				ChatService.friendMsgs.get(position).setHasUnRead(false);
				chatService.friendMsgs.get(position).setUnReadQuantity(0);
				Bundle bundle=new Bundle();
				Fragment_ChatMsg.friendId=friendMsg.getUserId();
				Fragment_ChatMsg.friendName=friendMsg.getUsername();
				bundle.putString("username", friendMsg.getUsername());
				bundle.putInt("friendId", friendMsg.getUserId());
				bundle.putString("avatarOp", friendMsg.getAvatar());
				Intent intent=new Intent(getActivity(),ChatActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
			}
			
		});
		chatService.initFriendMsg(lv_msg_list, username,userId,1);
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("onResume here is running!");
		lv_msg_list.setSelection(0);
		Constant.IS_MSG_THREAD_SHOULD_END=false;
		if(Constant.TAG_ENTER_CHACK_DATA==1){
			ChatService.friendMsgs.clear();
			chatService.initFriendMsg(lv_msg_list, username,userId,1);
			Constant.TAG_ENTER_CHACK_DATA=-1;
		}else if(Constant.TAG_ENTER_CHACK_DATA==0){
			//ChatService.friendMsgs.clear();
//			if(Constant.IS_MSG_THREAD_SHOULD_END){
			Constant.TAG_ENTER_CHACK_DATA=-1;
			
				for(int i=0;i<ChatService.friendMsgs.size();i++){
					int friendId=ChatService.friendMsgs.get(i).getUserId();
					if(Fragment_ChatMsg.friendId==friendId){
						SqliteDatabaseHelper databaseHelper=new SqliteDatabaseHelper(getActivity());
						Cursor cursor = databaseHelper
								.execQuery(
										"select * from chatmsg where (fromUserId=? and toUserId=? and mainUser=?) or (fromUserId=? and toUserId=?  and mainUser=?) order by msgId desc limit ?",
										new String[] {String.valueOf(userId),String.valueOf(friendId),String.valueOf(userId),
												String.valueOf(friendId),String.valueOf(userId),String.valueOf(userId),
												String.valueOf(1) });// 从SQLite中获取用户的聊天信息
						if(cursor.moveToFirst()){
							String recentMsg=cursor.getString(cursor.getColumnIndex("msgContent"));
							if(recentMsg!=null){
								ChatService.friendMsgs.get(i).setRecentMsg(recentMsg);
							}
						}
					}
				}
				chatService.initFriendMsg(lv_msg_list, username,userId,-1);
//			}
		}
	}
}

