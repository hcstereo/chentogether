package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.service.HistoryService;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Toast;

public class FragmentTitleBarHistory extends Fragment implements OnClickListener{
	public static final String TAG="FragmentTitleBarHistory";
	private Button btn_history_show;
	public static ProgressBar pbar_his_refresh;
	private View history_pop_up_win;
	private PopupWindow popupWindow;
	private View lin_his_bar;
	private DisplayMetrics displayMetrics=new DisplayMetrics();
	private int heigh,width;
	private ImageButton ivTitleBtnLeft;
	private Button btn_his_pop_date,btn_his_pop_cal,btn_his_pop_time,btn_his_pop_dis;
	private HistoryService historyService;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		heigh=displayMetrics.heightPixels;
		width=displayMetrics.widthPixels;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		historyService=new HistoryService(getActivity());
		View view = inflater.inflate(R.layout.title_bar_history, container,
				false);
		lin_his_bar=view.findViewById(R.id.lin_his_bar);
		pbar_his_refresh=(ProgressBar) view.findViewById(R.id.pbar_his_refresh);
		btn_history_show = (Button)view.findViewById(R.id.btn_history_show);
		btn_history_show.setOnClickListener(this);
		history_pop_up_win=inflater.inflate(R.layout.history_pop_up_win, null);
		btn_his_pop_date=(Button) history_pop_up_win.findViewById(R.id.btn_his_pop_date);
		btn_his_pop_cal=(Button) history_pop_up_win.findViewById(R.id.btn_his_pop_cal);
		btn_his_pop_time=(Button) history_pop_up_win.findViewById(R.id.btn_his_pop_time);
		btn_his_pop_dis=(Button) history_pop_up_win.findViewById(R.id.btn_his_pop_dis);
		ivTitleBtnLeft=(ImageButton) view.findViewById(R.id.ivTitleBtnLeft);
		btn_his_pop_dis.setOnClickListener(this);
		btn_his_pop_time.setOnClickListener(this);
		btn_his_pop_cal.setOnClickListener(this);
		btn_his_pop_date.setOnClickListener(this);
		ivTitleBtnLeft.setOnClickListener(this);
		popupWindow = new PopupWindow(history_pop_up_win,(width)*2/5,heigh*2/7,true);
		popupWindow.setTouchable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_history_show:
			if(HistoryService.getStatus().size()==0){
				Toast.makeText(getActivity(), "没有记录", Toast.LENGTH_SHORT).show();
				return;
			}
			if(popupWindow.isShowing()){
				popupWindow.dismiss();
			}else{
				popupWindow.showAsDropDown(v);
				popupWindow.showAtLocation(lin_his_bar, Gravity.CENTER, 20,
		                   20);
			}
			break;
		case R.id.btn_his_pop_date:
			historyService.sortService(1);
			popupWindow.dismiss();
			break;
		case R.id.btn_his_pop_cal:
			historyService.sortService(3);
			popupWindow.dismiss();
			break;
		case R.id.btn_his_pop_time:
			historyService.sortService(4);
			popupWindow.dismiss();
			break;
		case R.id.btn_his_pop_dis:
			historyService.sortService(2);
			popupWindow.dismiss();
			break;
		case R.id.ivTitleBtnLeft:
			//点击标题左边按钮弹出左侧菜单
			MainActivity ra = (MainActivity) getActivity();
			ra.mSlidingMenu.showMenu(true);
			break;
		default:
			break;
		}
		
	}

}
