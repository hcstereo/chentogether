package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GpsOpen extends Activity {
	private TextView tv_gpsopen_queding,tv_gpsopen_cancel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gpsopen);
		findViewById();
		setListener();
		tv_gpsopen_queding.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO 自动生成的方法存根
				//btn_gerenxinxi_logout.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_gerenxinxi_logout));
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gpsopen_queding.setBackgroundResource(R.color.white);
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gpsopen_queding.setBackgroundResource(R.color.gray); 
		
	            } 
	            else{
	            	tv_gpsopen_queding.setBackgroundResource(R.color.white);
	            }
				return false;
			}});
		tv_gpsopen_cancel.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO 自动生成的方法存根
				//btn_gerenxinxi_logout.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_gerenxinxi_logout));
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gpsopen_cancel.setBackgroundResource(R.color.white);
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gpsopen_cancel.setBackgroundResource(R.color.gray); 
		
	            } 
	            else{
	            	tv_gpsopen_cancel.setBackgroundResource(R.color.white);
	            }
				return false;
			}});
	}
	private void findViewById() {
		tv_gpsopen_queding = (TextView) findViewById(R.id.tv_gpsopen_queding);
		tv_gpsopen_cancel = (TextView) findViewById(R.id.tv_gpsopen_cancel);
	}
	private void setListener() {
		/*
		 * 确定
		 */
		tv_gpsopen_queding.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
 				startActivity(intent);
				finish();
			}
		});
		/*
		 * 取消
		 */
		tv_gpsopen_cancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public boolean onTouchEvent(MotionEvent event){
		finish();
		return true;
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // 获取back键
			this.finish();  
		}
		else if (keyCode == KeyEvent.KEYCODE_MENU) { // 获取 Menu键
			this.finish();  
		}
		return false;
		
	}
}
