package com.chending.ttsports.activity;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.model.User;
import com.chending.ttsports.model.UserLoginWidgetModel;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.service.RegisterService;
import com.chending.ttsports.service.SportService;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.chending.ttsports.common.RoundAngleImageView;
public class RegisterActivity extends Activity {
	private EditText edt_register_username,edt_register_password;
	private EditText edt_register_repeatpassword;
	private TextView tv_register_return;
	private Button btn_register_submit; 
	private RegisterService registerService;
	private Dialog registerLoadingDialog;
	private View layout;	
	private PopupWindow menuWindow;
	private LayoutInflater inflater;
	private RoundAngleImageView raiv_register_touxiang;
	private Button btn_photo_choosephoto,btn_photo_paizhao,btn_photo_cancel;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		init();
		addListener();
	}
	/**
	 * 初始化
	 */
	public void init(){
		Constant.IMG_PATH_AFTER_CROP="";
		edt_register_username = (EditText) findViewById(R.id.edt_register_username);
		edt_register_password = (EditText) findViewById(R.id.edt_register__password);
		edt_register_repeatpassword = (EditText) findViewById(R.id.edt_register_repeatpassword);
		btn_register_submit = (Button) findViewById(R.id.btn_register_submit);
		tv_register_return = (TextView)findViewById(R.id.tv_register_return);
		raiv_register_touxiang = (RoundAngleImageView)findViewById(R.id.raiv_register_touxiang);
		registerService=new RegisterService(this);
		registerLoadingDialog=new Dialog(this,R.style.FullHeightDialog);
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_register_submit.setOnClickListener(new btnClickListener());
		tv_register_return.setOnClickListener(new btnClickListener());
		raiv_register_touxiang.setOnClickListener(new btnClickListener());
	}

	private class btnClickListener implements OnClickListener{
		@SuppressWarnings("deprecation")
		@SuppressLint("SimpleDateFormat")
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			
			case R.id.btn_register_submit:
				if(edt_register_password.getText().toString().trim().length()<8){
					Toast.makeText(RegisterActivity.this, "密码长度不可小于8", Toast.LENGTH_SHORT).show();return;
				}
				if(edt_register_username.getText().toString().trim().equals("")){
					Toast.makeText(RegisterActivity.this, "用户名不能为空", Toast.LENGTH_SHORT).show();return;
				}
				if(edt_register_password.getText().toString().trim().equals("")){
					Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();return;
				}
				if(!edt_register_password.getText().toString().trim().equals(edt_register_repeatpassword.getText().toString().trim())){
					Toast.makeText(RegisterActivity.this, "两次密码不一致", Toast.LENGTH_SHORT).show();return;
				}
				if(Constant.IMG_PATH_AFTER_CROP.equals("")){
					Toast.makeText(RegisterActivity.this, "请先选择头像", Toast.LENGTH_SHORT).show();return;
				}
				registerLoadingDialog.show();
				registerLoadingDialog.setCancelable(false);
				registerLoadingDialog.setContentView(R.layout.activity_login_loading_dialog_view);
				User user=new User();
				user.setUsername(edt_register_username.getText().toString().trim());
				System.out.println("activity---"+user.getUsername());
				user.setPassword(edt_register_password.getText().toString().trim());
				user.setGender("男");
				user.setHeight(175);
				user.setWeight(70);
				user.setSignature("我还没有签名");
				user.setHobby("没有兴趣");
				user.setRegTime(new Date());
				SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				Date date=new Date();
				try {
					date=simpleDateFormat.parse(simpleDateFormat.format(date));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				user.setRegTime(date);
				registerService.registerService(user, registerLoadingDialog);
				break;
			case R.id.tv_register_return:
				Intent intent = new Intent(RegisterActivity.this,
						LoginActivity.class);
				startActivity(intent);
				finish();
				break;
			case R.id.raiv_register_touxiang://点击头像
	 	    	inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
	 	    	layout = inflater.inflate(R.layout.activity_alter_register_pthoto, null);
	 	    	menuWindow = new PopupWindow(layout,LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT); //后两个参数是width和height
	 	    	menuWindow.showAsDropDown(layout, 1, 1); //设置弹出效果
				menuWindow.setFocusable(true);
				menuWindow.setOutsideTouchable(true);
				menuWindow.update();
				menuWindow.setBackgroundDrawable(new BitmapDrawable());
	 	    	menuWindow.showAtLocation(findViewById(R.id.register_activity), Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
	 	    	btn_photo_choosephoto = (Button)layout.findViewById(R.id.btn_photo_choosephoto);
	 	    	btn_photo_paizhao = (Button)layout.findViewById(R.id.btn_photo_paizhao);	
	 	    	btn_photo_cancel = (Button)layout.findViewById(R.id.btn_photo_cancel);
				
				/*
				 * 选择照片
				 */
				btn_photo_choosephoto.setOnClickListener (new OnClickListener() {					
					@Override
					public void onClick(View arg0) {
						final String IMAGE_TYPE = "image/*";
						Intent getAlbum = new Intent(Intent.ACTION_GET_CONTENT);
						getAlbum.setType(IMAGE_TYPE);
						menuWindow.dismiss();
						startActivityForResult(getAlbum, Constant.REQUESTCODE_PHOTO);
						}
					});	
					
				/*
				 * 拍照上传
				 */
				btn_photo_paizhao.setOnClickListener (new OnClickListener() {					
					@SuppressLint("SdCardPath")
					@Override
					public void onClick(View arg0) {
						Intent intent = null;
						intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						File dir = new File("/sdcard/ttsports/Camera/");
						if (!dir.exists()) {
							dir.mkdirs();
						}
						Constant.mUploadPhotoPath = "/sdcard/ttsports/Camera/"
								+ UUID.randomUUID().toString();
						//UUID.randomUUID().toString()
						File file = new File(
								Constant.mUploadPhotoPath);
						if (!file.exists()) {
							try {
								file.createNewFile();
							} catch (IOException e) {

							}
						}
						menuWindow.dismiss();
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(file));
						startActivityForResult(intent,
										Constant.REQUESTCODE_UPLOADPHOTO_CAMERA);
						}
					});	
				/*
				 *取消
				 */
				btn_photo_cancel.setOnClickListener (new OnClickListener() {					
					@Override
					public void onClick(View arg0) {
						menuWindow.dismiss();
						}
					});	
				break;
			}
		}
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
			/*
			 * 相册返回事件
			 */
			if(requestCode == Constant.REQUESTCODE_PHOTO){
				 String[] proj = {MediaStore.Images.Media.DATA};
				if(data == null){
				}
				else{
					Uri originalUri = data.getData();        //获得图片的uri  
					//好像是android多媒体数据库的封装接口，具体的看Android文档
					@SuppressWarnings("deprecation")
					Cursor cursor = managedQuery(originalUri, proj, null, null, null); 
					//按我个人理解 这个是获得用户选择的图片的索引值
					int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					//将光标移至开头 ，这个很重要，不小心很容易引起越界
					cursor.moveToFirst();
					//最后根据索引值获取图片路径
					Constant.mUploadPhotoPath = cursor.getString(column_index);
					Bundle mBundle = new Bundle();
					mBundle.putString("path",Constant.mUploadPhotoPath);  
					Intent intent = new Intent(this,ImageFilterCropActivity.class);
					intent.putExtras(mBundle); 
					startActivityForResult(intent,Constant.REQUESTCODE_CROP_PHOTO);
				}
			}
			/*
			 * 相机返回事件
			 */
			else if(requestCode == Constant.REQUESTCODE_UPLOADPHOTO_CAMERA){
				Bundle mBundle = new Bundle();
				mBundle.putString("path",Constant.mUploadPhotoPath);  
				Intent intent = new Intent(this,ImageFilterCropActivity.class);
				intent.putExtras(mBundle); 
				startActivityForResult(intent,Constant.REQUESTCODE_CROP_PHOTO);
				
			}
			/*
			 * 裁剪完返回事件
			 */
			else if(requestCode == Constant.REQUESTCODE_CROP_PHOTO){
				if(resultCode == RESULT_OK){
					String myJpgPath = data.getStringExtra("path");
					Constant.IMG_PATH_AFTER_CROP = myJpgPath;
           		 	Bitmap bm = BitmapFactory.decodeFile(myJpgPath); 
                    //将图片显示到ImageView中  
           		 	raiv_register_touxiang.setImageBitmap(bm); 
				}
				else if(resultCode == RESULT_CANCELED){
					Toast.makeText(this, "选择图片失败", Toast.LENGTH_SHORT).show();
				}
			}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK){
			Intent intent = new Intent(RegisterActivity.this,
					LoginActivity.class);
			startActivity(intent);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
  }
