package com.chending.ttsports.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.ChatMsgVO;
import com.chending.ttsports.common.ChatMsgAdapter;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.PullToRefreshViewMsg;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.ChatMsgEntity;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChatService;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.util.ModelTool;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class ChatActivity extends Activity
		implements
		com.chending.ttsports.common.PullToRefreshViewMsg.OnHeaderRefreshListener {
	private ListView lv_chat_list;
	private Button btn_chat_send;
	private ChatMsgAdapter mAdapter;
	//private List<ChatMsgEntity> mDataArrays = new ArrayList<ChatMsgEntity>();
	private List<ChatMsgEntity> mDataArrays = new Vector<ChatMsgEntity>();
	private SqliteDatabaseHelper databaseHelper;
	private SharedPreferencesTool preferencesTool;
	private TextView tv_chat_username;
	private String username;// 好友的用户名
	private int friendId;//朋友的id
	private String myName;// 用户自己的用户名
	private int myId;//用户id
	private String avatarOp;
	private EditText edt_chat_input;
	@SuppressLint("SimpleDateFormat")
	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private RelativeLayout ral_chat_bar;
	private LinearLayout lin_addfriend_back;
	private Date dateTag = null;
	private int hasMsg = 0;
	private PullToRefreshViewMsg friends_pull_refresh_view_msg;
	private boolean isSetSelection=false;
	private ChatService chatService;
	private User user=new User();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);
		Constant.IS_IN_CHAT_ACTIVITY=true;
		Constant.IS_CHAT_THREAD_SHOULD_END=false;
		try {
			init();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		addListener();
		new ChatThreadFromWeb(handler1,mDataArrays).start();//接收消息线程
	}

	private void init() throws ParseException {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);// 防止软件盘挡住输入框
		// findviewById
		lin_addfriend_back=(LinearLayout) findViewById(R.id.lin_addfriend_back);
		friends_pull_refresh_view_msg = (PullToRefreshViewMsg) findViewById(R.id.friends_pull_refresh_view_msg);
		ral_chat_bar = (RelativeLayout) findViewById(R.id.ral_chat_bar);
		btn_chat_send = (Button) findViewById(R.id.btn_chat_send);
		edt_chat_input = (EditText) findViewById(R.id.edt_chat_input);
		tv_chat_username = (TextView) findViewById(R.id.tv_chat_username);
		lv_chat_list = (ListView) findViewById(R.id.lv_chat_list);
		chatService=new ChatService(this);
		databaseHelper = new SqliteDatabaseHelper(this);
		preferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(this);
		user=preferencesTool.toUserFromSharePreferences(preferencesTool.getUserPrefences());
		System.out.println("avatar========"+user.getAvatar());
		Bundle bundle = this.getIntent().getExtras();
		friendId=bundle.getInt("friendId");
		username = bundle.getString("username");// 获取Bundle传来的用户名
		avatarOp=bundle.getString("avatarOp");
		myName = preferencesTool.getUserPrefences().get("username");
		myId=Integer.parseInt(preferencesTool.getUserPrefences().get("userId"));
		tv_chat_username.setText(username);// 设置对方用户名
		mDataArrays=getMsgEntityByPage(mDataArrays,Constant.MSG_PAGE_SIZE);// 分页查询sqlite
		mAdapter = new ChatMsgAdapter(this, mDataArrays,lv_chat_list);
		lv_chat_list.setAdapter(mAdapter);// 为listview设置adapter
		lv_chat_list.setSelection(mDataArrays.size()-1);
	}

	private void addListener() {
		btn_chat_send.setOnClickListener(new BtnClickListener());
		edt_chat_input.addTextChangedListener(new OnTextChangeListener());
		edt_chat_input.setOnFocusChangeListener(new OnFocusChangeListener());
		friends_pull_refresh_view_msg.setOnHeaderRefreshListener(this);
		lin_addfriend_back.setOnClickListener(new BtnClickListener());
	}

	/**
	 * 单击事件监听
	 * 
	 * @author Chen
	 * 
	 */
	private class BtnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_chat_send:
				if (!edt_chat_input.getText().toString().trim().equals("")) {
					sendMsg();
				}
				break;
			case R.id.lin_addfriend_back:
				finish();
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 用户点击发送，需要做一下事情 1.显示到页面 2.存到本地数据库 3.发送到服务器
	 */
	private void sendMsg() {
		ChatMsgEntity chatMsgEntity = new ChatMsgEntity();
		chatMsgEntity.setComMeg(false);
		chatMsgEntity.setName(myName);
		chatMsgEntity.setText(edt_chat_input.getText().toString().trim());
		String dd = getDate();
		try {
			chatMsgEntity.setSameTime(setChatTime(dd));
			System.out.println(chatMsgEntity.isSameTime());
			chatMsgEntity.setDate(ModelTool.dateExchange(dd));
		} catch (ParseException e) {
			e.printStackTrace();
		}// 最后一条记录的发送时间
		chatMsgEntity.setImagUrl(user.getAvatar());
		mDataArrays.add(chatMsgEntity);
		mAdapter.notifyDataSetChanged();
		edt_chat_input.setText("");
		lv_chat_list.setSelection(mDataArrays.size() - 1);
		ChatMsgVO chatMsg=null;
		try {
			chatMsg = new ChatMsgVO(myId,friendId,chatMsgEntity.getText(),1,1,dateFormat.parse(dd),myName,username,user.getAvatar(),myId,avatarOp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		saveMsg(chatMsg);
		//发送服务器
		chatMsg.setMsgType(1);
		chatService.sendMsgService(chatMsg);
	}

	/**
	 * 发送的信息保存单本地数据库SQLite
	 * 
	 * @param chatMsg
	 *            需要保存的信息实体
	 */
	private void saveMsg(ChatMsgVO chatMsg) {
		databaseHelper
				.execSql("insert into chatmsg (fromUserId,toUserId,msgContent,fromUsername,toUsername,createTime,avatar,mainUser,avatarOp) values ("
						+chatMsg.getFromUserId()
						+ ","
						+chatMsg.getToUserId()
						+ ",'"
						+ chatMsg.getMsgContent()
						+ "','"
						+ chatMsg.getFromUsername()
						+ "','"
						+ chatMsg.getToUsername()
						+ "','"
						+ dateFormat.format(chatMsg.getCreateTime()) 
						+ "','"
						+chatMsg.getAvatar()
						+ "',"
						+chatMsg.getMainUser()
						+",'"
						+chatMsg.getAvatarOp()
						+"')");
		System.out.println("ss");
	}

	/**
	 * 获得当前如期的字符串形式
	 * 
	 * @return 当前时间
	 */
	private String getDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String dateString=Constant.CURRENT_TIME;
		Date date=new Date();
		try {
			date=dateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date date2=new Date(date.getTime()+Constant.CURRENT_SECONDS*1000);
		System.out.println("current second is "+Constant.CURRENT_SECONDS);
		return dateFormat.format(date2);
	}

	/**
	 * 文本框输入监听
	 * 
	 * @author Chen
	 * 
	 */
	private class OnTextChangeListener implements TextWatcher {

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			lv_chat_list.setSelection(mDataArrays.size() - 1);
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	}

	/**
	 * 触摸事件的监听
	 * 
	 * @author Chen
	 * 
	 */
	private class OnFocusChangeListener implements
			android.view.View.OnFocusChangeListener {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			switch (v.getId()) {
			case R.id.edt_chat_input:
				ral_chat_bar
						.setBackgroundResource(R.drawable.chat_input_bar_bg_active);// 更改输入框的的背景
				
				break;
			default:
				break;
			}
		}
	}
	/**
	 * 判断是否显示时间
	 * 
	 * @param date
	 *            当前消息的发送时间字符串
	 * @return 返回true表示不需要显示时间
	 * @throws ParseException
	 */
	private boolean setChatTime(String date) throws ParseException {
		boolean isSameTime = false;
		if (dateTag == null) {
			try {
				dateTag = dateFormat.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} else {
			SimpleDateFormat dateFormat1 = new SimpleDateFormat("yy-MM-dd");
			SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
			String ymd1 = dateFormat1.format(dateTag);
			String ymd2 = dateFormat1.format(dateFormat.parse(date));

			if (ymd1.equals(ymd2)) {// 年月日都相同
				String d1 = dateFormat2.format(dateTag);
				String d2 = dateFormat2.format(dateFormat.parse(date));
				String[] d1s = d1.split(":");
				String[] d2s = d2.split(":");
				int mm1 = Integer.parseInt(d1s[1]);
				int mm2 = Integer.parseInt(d2s[1]);
				if (d1s[0].equals(d2s[0]) && (mm2 - mm1) < 15) {// 小时相同,分钟之小于15分钟
					isSameTime = true;
					dateTag = dateFormat.parse(date);
				} else {
					isSameTime = false;
					dateTag = dateFormat.parse(date);
				}
			} else {
				isSameTime = false;
				dateTag = dateFormat.parse(date);
			}
		}
		return isSameTime;
	}

	private List<ChatMsgEntity> getMsgEntityByPage(List<ChatMsgEntity> mDataArrays,int pageSize)
			throws ParseException {
		System.out.println("hasMsg="+hasMsg);
		//List<ChatMsgEntity> temp=new ArrayList<ChatMsgEntity>();
		List<ChatMsgEntity> temp=new Vector<ChatMsgEntity>();
		// 从数据库中聊天信息，返回cursor对象，通过游标遍历
		Cursor cursor = databaseHelper
				.execQuery(
						//"select * from chatmsg where (fromUsername=? and toUsername=?) or (fromUsername=? and toUsername=?) order by createTime desc limit ? offset ?",
						"select * from chatmsg where (fromUserId=? and toUserId=? and mainUser=?) or (fromUserId=? and toUserId=? and mainUser=?) order by msgId desc limit ? offset ?",
						new String[] { String.valueOf(myId), String.valueOf(friendId),String.valueOf(myId), String.valueOf(friendId), String.valueOf(myId),String.valueOf(myId),
								String.valueOf(pageSize),
								String.valueOf(hasMsg) });// 从SQLite中获取用户的聊天信息
		// 遍历数据得到list集合
		for (cursor.moveToLast(); !cursor.isBeforeFirst(); cursor.moveToPrevious()) {
			ChatMsgEntity entity = new ChatMsgEntity();
			String from = cursor.getString(cursor
					.getColumnIndex("fromUsername"));
			String date = cursor.getString(cursor.getColumnIndex("createTime"));
			String content = cursor.getString(cursor
					.getColumnIndex("msgContent"));
			String avatar=cursor.getString(cursor
					.getColumnIndex("avatar"));
			System.out.println("avatar"+avatar);
			entity.setSameTime(setChatTime(date));
			if (from.equals(myName)) {
				entity.setName(myName);
				entity.setDate(ModelTool.dateExchange(date));
				entity.setText(content);
				entity.setComMeg(false);
				entity.setImagUrl(avatar);
			} else {
				entity.setName(username);
				entity.setDate(ModelTool.dateExchange(date));
				entity.setText(content);
				entity.setComMeg(true);
				entity.setImagUrl(avatar);
			}
			temp.add(entity);
		}


		for(ChatMsgEntity chatMsgEntity:mDataArrays){
			temp.add(chatMsgEntity);
		}
		this.hasMsg += pageSize;
		cursor.close();
		return temp;
	}
	private List<ChatMsgEntity> getMsgEntityFromWeb(List<ChatMsgEntity> mDataArrays,List<ChatMsgVO> chatMsgVOs)
			throws ParseException {
		// 遍历数据得到list集合
		for(int i=0;i<chatMsgVOs.size();i++){
			ChatMsgVO chatMsgVO=chatMsgVOs.get(i);
			if(chatMsgVO.getFromUserId()==friendId){
				isSetSelection=true;
				ChatMsgEntity entity = new ChatMsgEntity();
				String date = dateFormat.format(chatMsgVO.getCreateTime());
				String content = chatMsgVO.getMsgContent();
				String avatar=chatMsgVO.getAvatar();
				String avatarOp=chatMsgVO.getAvatarOp();
				entity.setSameTime(setChatTime(date));
				entity.setText(content);
				entity.setDate(date);
				entity.setName(username);
				entity.setImagUrl(avatar);
				ChatMsgVO chatMsg = new ChatMsgVO(chatMsgVO.getFromUserId(),chatMsgVO.getToUserId(),entity.getText(),1,1,chatMsgVO.getCreateTime(),username,myName,avatar,myId,user.getAvatar());
				saveMsg(chatMsg);
				hasMsg++;
				ChatService.chatMsgVOs.remove(i);
				mDataArrays.add(entity);
			}
		}
		return mDataArrays;
	}
	/**
	 * 用户下拉刷新，获取更多聊天信息（本地数据库）
	 */
	@Override
	public void onHeaderRefresh(PullToRefreshViewMsg view) {
		new ChatThreadFromDb(handler,mDataArrays,Constant.MSG_PAGE_SIZE).start();
	}
	@SuppressLint("HandlerLeak")
	private Handler handler=new Handler(){
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			mDataArrays.clear();
			mDataArrays.addAll((List<ChatMsgEntity>) msg.obj);
			if(mDataArrays!=null){
				friends_pull_refresh_view_msg.onHeaderRefreshComplete();
				mAdapter.notifyDataSetChanged();
				//lv_chat_list.setSelection(mDataArrays.size()-hasMsg+15);
			}
		}
	};
	@SuppressLint("HandlerLeak")
	private Handler handler1=new Handler(){
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			mDataArrays=(List<ChatMsgEntity>) msg.obj;
			if(mDataArrays!=null){
				mAdapter.notifyDataSetChanged();
				if(isSetSelection){
					lv_chat_list.setSelection(mDataArrays.size() - 1);
				}
				isSetSelection=false;
			}
		}
	};
	private class ChatThreadFromDb extends Thread{
		private Handler handler;
		private List<ChatMsgEntity> mDataArrays;
		private int pageSize;
		public ChatThreadFromDb(Handler handler,List<ChatMsgEntity> mDataArrays,int pageSize){
			this.handler=handler;
			this.mDataArrays=mDataArrays;
			this.pageSize=pageSize;
		}
		@Override
		public void run() {
			super.run();
			try {
				sleep(200);
				mDataArrays=getMsgEntityByPage(mDataArrays,pageSize);
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Message msg=new Message();
			msg.obj=mDataArrays;
			handler.sendMessage(msg);
		}
	}
	/**
	 * 从服务器获取数据，把消息数据返回给handler
	 * @author Chen
	 *
	 */
	private class ChatThreadFromWeb extends Thread{
		private Handler handler;
		private List<ChatMsgEntity> mDataArrays;
		public ChatThreadFromWeb(Handler handler,List<ChatMsgEntity> mDataArrays){
			this.handler=handler;
			this.mDataArrays=mDataArrays;
		}
		@Override
		public void run() {
			try {
				while(!Constant.IS_CHAT_THREAD_SHOULD_END){
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					mDataArrays=getMsgEntityFromWeb(mDataArrays,ChatService.chatMsgVOs);//将从服务器获取的聊天信息添加到listview
					
					Message msg=new Message();
					msg.obj=mDataArrays;
					handler.sendMessage(msg);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Constant.IS_CHAT_THREAD_SHOULD_END=true;
		Constant.IS_IN_CHAT_ACTIVITY=false;
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(KeyEvent.KEYCODE_BACK==keyCode){
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	
	
}
