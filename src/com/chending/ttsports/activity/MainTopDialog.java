package com.chending.ttsports.activity;

import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.chaowen.yixin.R;
import com.chending.ttsports.model.PointOverlay;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainTopDialog extends Activity {
	private LinearLayout layout;
	private ImageView walking_top_dialog,runing_top_dialog,skating_top_dialog,skiing_top_dialog,cycling_top_dialog;
	public static int how_sport_flag = 2;//定义何种运动方式的标志位， 并初始化为1  步行方式 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_top_dialog);
		//dialog=new MyDialog(this);
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		init();
		addListener();
	}
	 private void init() {
		 walking_top_dialog = (ImageView)findViewById(R.id.walking_top_dialog);
		 runing_top_dialog = (ImageView)findViewById(R.id.runing_top_dialog);
		 skating_top_dialog = (ImageView)findViewById(R.id.skating_top_dialog);
		 skiing_top_dialog = (ImageView)findViewById(R.id.skiing_top_dialog);
		 cycling_top_dialog = (ImageView)findViewById(R.id.cycling_top_dialog);
	}
    /**
	 * 添加监听
	 */
	private void addListener(){
		walking_top_dialog.setOnClickListener(new btnClickListener());	
		runing_top_dialog.setOnClickListener(new btnClickListener());	
		skating_top_dialog.setOnClickListener(new btnClickListener());	
		skiing_top_dialog.setOnClickListener(new btnClickListener());	
		cycling_top_dialog.setOnClickListener(new btnClickListener());	
	}
	private class btnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.walking_top_dialog: 
				how_sport_flag = 1;
				finish();
				break;
			case R.id.runing_top_dialog: 
				how_sport_flag = 2;
				finish();
				break;
			case R.id.skating_top_dialog: 
				how_sport_flag = 3;
				finish();
				break;
			case R.id.skiing_top_dialog:
				how_sport_flag = 4;
				finish();
				break;
			case R.id.cycling_top_dialog: 
				how_sport_flag = 5;
				finish();
				break;
			default:
				break;
			}
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent event){
		finish();
		return true;
	}
}
