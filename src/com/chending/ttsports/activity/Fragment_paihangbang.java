package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.service.ChartsService;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class Fragment_paihangbang extends Fragment{
	public static final String TAG="Fragment_paihangbang";
	private ListView lv_charts_list;
	ChartsService chartsService;
     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.fragment_paihangbang, container, false);
    	init(view);
    	addListenner();
    	return view;
    }
    private void init(View view){
    	chartsService = new ChartsService(getActivity());
    	lv_charts_list = (ListView) view.findViewById(R.id.lv_charts_list);
    	chartsService.chartsService(lv_charts_list);
    } 
    public void addListenner(){
    	lv_charts_list.setOnItemClickListener(new ClickListenner());
    }
    
    private class ClickListenner implements OnItemClickListener{
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int i,
				long arg3) {
			switch (i) {
			case 0:
				Toast.makeText(getActivity(), "点击了第"+i+"条记录", Toast.LENGTH_LONG).show();
				break;
			case 1:
				Toast.makeText(getActivity(), "点击了第"+i+"条记录", Toast.LENGTH_LONG).show();
				break;
			case 2:
				Toast.makeText(getActivity(), "点击了第"+i+"条记录", Toast.LENGTH_LONG).show();
				break;
			default:
				break;
			}
			
		}
    	
    }
    private void switchFragment(Fragment fragment,int i,String tagMain,String tagBar) {
		if (getActivity() == null)
			return;	
			MainActivity ra = (MainActivity) getActivity();
			ra.switchContent(fragment,i,tagMain,tagBar);
		
	}
}
