package com.chending.ttsports.activity;

import com.chaowen.yixin.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

public class FragmentTitleBarMsg extends Fragment implements OnClickListener{
	public static final String TAG="FragmentTitleBarMsg";
	public static ProgressBar pbar_msg_refresh;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.title_bar_chatmsg, container,
				false);
		ImageButton imageButton=(ImageButton) view.findViewById(R.id.ivTitleBtnLeft);
		pbar_msg_refresh=(ProgressBar) view.findViewById(R.id.pbar_msg_refresh);
		imageButton.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivTitleBtnLeft:
			//点击标题左边按钮弹出左侧菜单
			MainActivity ra = (MainActivity) getActivity();
			ra.mSlidingMenu.showMenu(true);
			break;
		default:
			break;
		}
		
	}


}
