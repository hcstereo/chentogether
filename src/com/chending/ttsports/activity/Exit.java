package com.chending.ttsports.activity;


import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.util.CropImageView;
import com.chending.ttsports.util.PhotoUtil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Exit extends Activity {
	private LinearLayout layout;
	private TextView tv_exit_queding,tv_exit_cancel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exit_dialog);
		findViewById();
		setListener();
		tv_exit_queding.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO 自动生成的方法存根
				//btn_gerenxinxi_logout.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_gerenxinxi_logout));
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_exit_queding.setBackgroundResource(R.color.white);
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_exit_queding.setBackgroundResource(R.color.gray); 
		
	            } 
	            else{
	            	tv_exit_queding.setBackgroundResource(R.color.white);
	            }
				return false;
			}});
		tv_exit_cancel.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO 自动生成的方法存根
				//btn_gerenxinxi_logout.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_gerenxinxi_logout));
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_exit_cancel.setBackgroundResource(R.color.white);
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_exit_cancel.setBackgroundResource(R.color.gray); 
		
	            } 
	            else{
	            	tv_exit_cancel.setBackgroundResource(R.color.white);
	            }
				return false;
			}});
	}
	private void findViewById() {
		tv_exit_queding = (TextView) findViewById(R.id.tv_exit_queding);
		tv_exit_cancel = (TextView) findViewById(R.id.tv_exit_cancel);
	}
	private void setListener() {
		/*
		 * 确定
		 */
		tv_exit_queding.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				/*
				 * 完全退出程序
				 */
				Intent intent = new Intent(Intent.ACTION_MAIN);  
                intent.addCategory(Intent.CATEGORY_HOME);  
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);  
                startActivity(intent);  
                android.os.Process.killProcess(android.os.Process.myPid());
				finish();
			}
		});
		/*
		 * 取消
		 */
		tv_exit_cancel.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public boolean onTouchEvent(MotionEvent event){
		finish();
		return true;
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // 获取back键
			this.finish();  
		}
		else if (keyCode == KeyEvent.KEYCODE_MENU) { // 获取 Menu键
			this.finish();  
		}
		return false;
		
	}
}
