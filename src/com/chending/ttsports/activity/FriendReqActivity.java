package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.FriendReqAdapter;
import com.chending.ttsports.service.FriendService;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class FriendReqActivity extends Activity implements OnItemClickListener{
	private ListView lv_friendreq_list;
	private LinearLayout lin_checkdata_back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friendreq);
		init();
		addListener();
	}
	private void init(){
		lv_friendreq_list=(ListView) findViewById(R.id.lv_friendreq_list);
		FriendReqAdapter adapter=new FriendReqAdapter(this, FriendService.friendRelVOs, lv_friendreq_list);
		System.out.println("FriendService.friendRelVOs size is "+FriendService.friendRelVOs.size());
		lv_friendreq_list.setAdapter(adapter);
		lin_checkdata_back=(LinearLayout) findViewById(R.id.lin_checkdata_back);
	}
	private void addListener(){
		lv_friendreq_list.setOnItemClickListener(this);
		lin_checkdata_back.setOnClickListener(new BtnClickListener());
	}
	private class BtnClickListener implements android.view.View.OnClickListener{
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.lin_checkdata_back:
				finish();
				break;
			default:
				break;
			}
		}
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
	}
	
}
