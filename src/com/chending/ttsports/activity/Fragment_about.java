package com.chending.ttsports.activity;


import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.chaowen.yixin.R;
import com.chending.ttsports.activity.MainActivity;

public class Fragment_about extends Fragment {
	public static final String TAG="Fragment_about";
	

     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.fragment_about, container, false);    	
    	return view;
    	
    }
    
}
