package com.chending.ttsports.activity;


import com.chaowen.yixin.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Fragment_plan extends Fragment {
	public static final String TAG="Fragment_plan";
	
     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.fragment_plan, container, false);
    	return view;
    }
}
