﻿package com.chending.ttsports.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.PopupWindow;
import android.widget.Chronometer.OnChronometerTickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.Geometry;
import com.baidu.mapapi.map.Graphic;
import com.baidu.mapapi.map.GraphicsOverlay;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MKMapViewListener;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.Symbol;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.chaowen.yixin.R;
import com.chending.ttsports.activity.StartActivity.returnThread;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.List_LineInfo;
import com.chending.ttsports.model.PointOverlay;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.model.SymbolTools;
import com.chending.ttsports.protocol.LoginProtocol;
import com.chending.ttsports.service.LoginService;
import com.chending.ttsports.service.SportService;


public class SportActivity extends Activity implements OnClickListener{
	private  Dialog sendStatusDialog;
	private View layout;
	private PopupWindow menuWindow;
	private LayoutInflater inflater;
	private MapView mMapView = null;	
	private MapController mMapController = null;
	private MKMapViewListener mMapListener = null;	
	private LocationClient mLocClient;
	public LocationListenner myListener = new LocationListenner();  //监听函数
	private MyLocationOverlay myLocationOverlay = null;         	//定位图层
	private LocationData locData = null;                            //位置信息
	private PointOverlay pointOVerlay = null;
	private OverlayItem overlay_item_start = null;
	private OverlayItem overlay_item_end = null;	
	private GraphicsOverlay graphicsoverlay;
	private Double startLat,startLng,endLat,endLng;                //定义开始结束经纬度
	private String latlng_start, latlng_end;
	private String[] startlatlng = new String[2];
	private String[] endlatlng = new String[2];
	public double distance_sum = 0.00;                             //定义总路程
	public double now_speed = 0.00,now_speed_show = 0.00;          //定义时速
	public double distance_show = 0.00;                            //显示的总路程
	public double calorie_sum = 0.0;                               //定义消耗卡路里
	public int average_speed = 0;                                  //定义平均时速
	private static final double EARTH_RADIUS = 6378137;            //计算两点距离所需常量
	private TextView distanceTextView,calorieTextView,speedTextView,nowspeedTextView;
	private TextView overButton,pauseButton;
	private Button btn_sport_oversport,btn_sport_cancle_menu,btn_sport_end_sport_nosend;
	private Chronometer chronometer = null;                        //计时器
	private long recordingTime = 0;                                // 暂停时记录 计时器的时间
	private int time_sum = -1;                                     //用于记录运动时间 单位为妙
	private boolean thread_flag = true;                            //用于定义线程标志位,默认为true
	private boolean over_flag = false; 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentHistory.isListNeedRef=true;
        DemoApplication app = (DemoApplication)this.getApplication();
        if (app.mBMapManager == null) {
            app.mBMapManager = new BMapManager(this);
            app.mBMapManager.init(DemoApplication.strKey,new DemoApplication.MyGeneralListener());
        }
        setContentView(R.layout.sport_next);
        try {
        	mLocClient = new LocationClient(this);
        	locData = new LocationData();
        	mLocClient.registerLocationListener( myListener );        //注册监听事件
        	LocationClientOption option = new LocationClientOption();
        	option.setOpenGps(true);                                  // 打开gps
        	option.setCoorType("bd09ll");                             // 设置坐标类型        
        	option.setScanSpan(1000);
        	option.setPriority(LocationClientOption.GpsFirst);        // 设置GPS优先 
        	mLocClient.setLocOption(option);
        	mLocClient.start();
		} catch (Exception e) {
			Toast.makeText(app, "GPS信号差", Toast.LENGTH_SHORT).show();
		}
        mapinit();
        /**
    	 *  MapView的生命周期与Activity同步，当activity挂起时需调用MapView.onPause()
    	 */
        mMapListener = new MKMapViewListener() {
			@Override
			public void onMapMoveFinish() {
				/**
				 * 在此处理地图移动完成回调
				 * 缩放，平移等操作完成后，此回调被触发
				 */
			}
			
			@Override
			public void onClickMapPoi(MapPoi mapPoiInfo) {
				/**
				 * 在此处理底图poi点击事件
				 * 显示底图poi名称并移动至该点
				 * 设置过： mMapController.enableClick(true); 时，此回调才能被触发
				 * 
				 */
				String title = "哈哈哈";
				if (mapPoiInfo != null){
					title = mapPoiInfo.strText;
					Toast.makeText(SportActivity.this,title,Toast.LENGTH_SHORT).show();
					mMapController.animateTo(mapPoiInfo.geoPt);
				}
			}
			@Override
			public void onGetCurrentMap(Bitmap b) {
				/**
				 *  当调用过 mMapView.getCurrentMap()后，此回调会被触发
				 *  可在此保存截图至存储设备
				 */
			}
			@Override
			public void onMapAnimationFinish() {
				/**
				 *  地图完成带动画的操作（如: animationTo()）后，此回调被触发
				 */
			}
			@Override
			public void onMapLoadFinish() {
				// TODO 自动生成的方法存根
				
			}
		}; 
		init();
		new distanceThread().start();//显示距离线程
        chronometer.setOnChronometerTickListener(new OnChronometerTickListener() 
        { 
            @Override 
            public void onChronometerTick(Chronometer ch) 
            { 
            	time_sum++;
            } 
        }); 
    }
    private void mapinit(){
    	 mMapView = (MapView)findViewById(R.id.bmapView1);
         mMapController = mMapView.getController();
         mMapController.enableClick(true);
         mMapController.setZoom(17);
         myLocationOverlay = new MyLocationOverlay(mMapView);
         locData = new LocationData();
         myLocationOverlay.setData(locData);
         mMapView.getOverlays().add(myLocationOverlay);//添加位置图层
         pointOVerlay = new PointOverlay(null, this, mMapView);//开始和结束图标图层初始化
         mMapView.getOverlays().add(pointOVerlay);//添加开始和结束图标图层
 		 graphicsoverlay = new GraphicsOverlay(mMapView);
 		 mMapView.getOverlays().add(graphicsoverlay);
         myLocationOverlay.enableCompass();
         mMapView.refresh();    
    }
    private void  init() {
    	mMapView.regMapViewListener(DemoApplication.getInstance().mBMapManager, mMapListener);
		nowspeedTextView = (TextView)findViewById(R.id.txt_nowspead);
		overButton = (TextView)findViewById(R.id.btn_stop);
		pauseButton = (TextView)this.findViewById(R.id.btn_pause);
		overButton.setOnClickListener(this);
		pauseButton.setOnClickListener(this);
		distanceTextView = (TextView)findViewById(R.id.txt_distance);
		calorieTextView = (TextView)findViewById(R.id.txt_calorie);
		speedTextView = (TextView)findViewById(R.id.txt_speed);
		chronometer = (Chronometer) findViewById(R.id.chronometer);
		chronometer.setFormat("00:%s");//设置时间格式
		chronometer.start();//计时器开始
	}
    public class distanceThread extends Thread {
        @SuppressWarnings("deprecation")
		@Override
        public void run () {
            do {
                try {
                    Thread.sleep(100);
                    Message msg = new Message();
                    msg.what = Constant.MESSAGE_START;
                    mHandler.sendMessage(msg);
                    /*
                     * 再地图上放置结束图标
                     */
                    if(Constant.end_flag == 1){
            			Constant.end_flag = 0;
            			DrawMarker_Sport(locData.latitude, locData.longitude,
            					Constant.MARKERT_END);
            		}
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while(thread_flag);
        }
    }
    @SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
        @Override
        public void handleMessage (Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case Constant.MESSAGE_START:
                	distance_show =  (double)Math.round(distance_sum*100)/100;//保留两位小数
                	now_speed_show = (double)Math.round(now_speed*100)/100;
    				calorie_sum = Calorie();//消耗卡路里的计算公式
    				calorie_sum = (double)Math.round(calorie_sum*100)/100;//保留两位小数
                    average_speed = (int) (3600.0*distance_sum/time_sum);
                    if(time_sum%3 == 0){//每三秒显示一次
                    	speedTextView.setText(String.valueOf(average_speed));
                    	calorieTextView.setText(String.valueOf(calorie_sum));
                    	nowspeedTextView.setText(String.valueOf(now_speed_show));
                        distanceTextView.setText(String.valueOf(distance_show));
                    }
                    break;
                default:
                    break;
            }
        }
    };
    /**
     * 监听函数，有新位置的时候，格式化成字符串，输出到屏幕中
     */
    public class LocationListenner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null)
                return;
            locData.latitude = location.getLatitude();
            locData.longitude = location.getLongitude();
            locData.accuracy = location.getRadius();
            locData.direction = location.getDerect();
            myLocationOverlay.setData(locData);
            mMapView.refresh();
            mMapController.animateTo(new GeoPoint((int) (locData.latitude * 1e6), (int) (locData.longitude * 1e6)));
            if (Constant.move_flag == 1) {
    			if (Constant.start_flag == 1) {   				
    				latlng_start = locData.latitude + "," + locData.longitude;
    				Constant.start_flag = 0;			
    				DrawMarker_Sport(locData.latitude, locData.longitude,
    						Constant.MARKERT_START);
    			} else {
    				latlng_end = locData.latitude + "," + locData.longitude;
    				List_LineInfo item = new List_LineInfo(latlng_end, latlng_start);
    				DrawLine(item);
    				latlng_start = latlng_end;		
    				distance_sum += (getDistance(item)/1000.0);//累计距离
    				now_speed = (getDistance(item)/1000.0)/2.0;
    			}
    		}
        }
        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
                return;
            }
        }
    }
    /*
     * 计算消耗卡路里公式
     */
    private double Calorie() {
    	if(MainTopDialog.how_sport_flag == 1){
    		calorie_sum = 65*distance_sum*0.3774;//走路消耗卡路里的计算公式
    	}
    	else if(MainTopDialog.how_sport_flag == 2){
    		calorie_sum = 65*distance_sum*1.036;//跑步消耗卡路里的计算公式
    	}
    	else if(MainTopDialog.how_sport_flag == 3){
    		calorie_sum = 65*distance_sum*0.518;//滑冰消耗卡路里的计算公式
    	}
    	else if(MainTopDialog.how_sport_flag == 4)
    	{
    		calorie_sum = 65*distance_sum*0.888;//滑雪消耗卡路里的计算公式
    	}
    	else{
    		calorie_sum = 65*distance_sum*0.9694;//骑行消耗卡路里的计算公式
    	}
    	return calorie_sum;
		
	}
    @SuppressWarnings("unused")
	private void stopRequestLocation() {
        if (mLocClient != null) {
            mLocClient.unRegisterLocationListener(myListener);
            mLocClient.stop();
        }
    }
    @SuppressWarnings("unused")
	private void startRequestLocation() {
        // this nullpoint check is necessary
        if (mLocClient != null) {
            mLocClient.registerLocationListener(myListener);
            mLocClient.start();
            mLocClient.requestLocation();
        }
    }
    @Override
    protected void onPause() {
    	/**
    	 *  MapView的生命周期与Activity同步，当activity挂起时需调用MapView.onPause()
    	 */
        mMapView.onPause();
        super.onPause();
    }
    @Override
    protected void onResume() {
    	/**
    	 *  MapView的生命周期与Activity同步，当activity恢复时需调用MapView.onResume()
    	 */
        mMapView.onResume();
        super.onResume();
    }
    @Override
    protected void onDestroy() {
    	/**
    	 *  MapView的生命周期与Activity同步，当activity销毁时需调用MapView.destroy()
    	 */
        mMapView.destroy();
        super.onDestroy();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	mMapView.onSaveInstanceState(outState);
    	
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	mMapView.onRestoreInstanceState(savedInstanceState);
    }
 // 用于任务中画轨迹
 	private boolean DrawLine(List_LineInfo item) {
 		Symbol lineSymbol = SymbolTools.getSymbol_line(0, 197, 205, 255, 7);//画线 颜色 和宽度
 		Graphic lineGraphic = new Graphic(getGeometrytLine(item), lineSymbol);
 		graphicsoverlay.setData(lineGraphic);
 		mMapView.refresh();
 		return true;
 	}

 	// 根据两点经纬度坐标得到一条线
 	private Geometry getGeometrytLine(List_LineInfo item) {
 		endlatlng = item.getEnd_latlng().split(",");
 		endLng = Double.parseDouble(endlatlng[1]);
 		endLat = Double.parseDouble(endlatlng[0]);
 		startlatlng = item.getStart_latlng().split(",");
 		startLng = Double.parseDouble(startlatlng[1]);
 		startLat = Double.parseDouble(startlatlng[0]);
 		// 构建点并显示
 		Geometry linegeometry = new Geometry();
 		GeoPoint[] linegeopoint = new GeoPoint[2];
 		linegeopoint[0] = new GeoPoint((int) (endLat * 1E6),
 				(int) (endLng * 1E6));
 		linegeopoint[1] = new GeoPoint((int) (startLat * 1E6),
 				(int) (startLng * 1E6));
 		linegeometry.setPolyLine(linegeopoint);
 		return linegeometry;
 	}
 	//根据两点经纬坐标计算两点之间的距离
 	private double getDistance(List_LineInfo item) {
 		endlatlng = item.getEnd_latlng().split(",");
 		endLng = Double.parseDouble(endlatlng[1]);
 		endLat = Double.parseDouble(endlatlng[0]);
 		startlatlng = item.getStart_latlng().split(",");
 		startLng = Double.parseDouble(startlatlng[1]);
 		startLat = Double.parseDouble(startlatlng[0]);
 		double radLat1 = rad(startLat);
        double radLat2 = rad(endLat);
        double a = radLat1 - radLat2;
        double b = rad(startLng) - rad(endLng);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + 
         Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
 		return s;
 	}
 	//计算距离调用的方法
 	 private static double rad(double d)
     {
        return d * Math.PI / 180.0;
     }
	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_pause://点击暂停按钮
			Constant.move_flag = -Constant.move_flag;//移动标志位置反
			if(!over_flag){
				if(Constant.pausecontinue_flag == 1){//按钮显示的是继续按钮 处于暂停状态 点击变到开始状态
					chronometer.setBase(SystemClock.elapsedRealtime() - recordingTime);//设置为上次暂停时的时间
					chronometer.start();//计时器开始
					Constant.pausecontinue_flag = 0;
					pauseButton.setText("暂停");
					pauseButton.setTextColor(Color.rgb(241, 241, 241));
				}
				else{//按钮显示的是暂停按钮 处于运动状态 点击变到暂停状态
					if(Constant.end_flag == 0){
						recordingTime = SystemClock.elapsedRealtime()
								- chronometer.getBase();// 保存这次记录了的时间;//获取当前定时器的时间 
						chronometer.stop();//计时器暂停
						Constant.pausecontinue_flag = 1;
						pauseButton.setText("继续");
						pauseButton.setTextColor(Color.rgb(255, 236, 139));
					}
				}
			}
			break;
		case R.id.btn_stop://点击结束按钮
			
			if(over_flag){//若已经停止运动，此时按钮上显示的返回，这是返回按钮的点击事件
				finish();
			}
			else{
				inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
				layout = inflater.inflate(R.layout.end_dialog_from_sport, null);				
				menuWindow = new PopupWindow(layout,LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT); //后两个参数是width和height
				menuWindow.showAsDropDown(layout); //设置弹出效果
				menuWindow.setFocusable(true);
				menuWindow.setOutsideTouchable(true);
				menuWindow.update();
				menuWindow.setBackgroundDrawable(new BitmapDrawable());
				menuWindow.showAtLocation(this.findViewById(R.id.sportnext), Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置					
				btn_sport_oversport = (Button)layout.findViewById(R.id.btn_sport_end_sport);
				btn_sport_cancle_menu = (Button)layout.findViewById(R.id.btn_sport_cancel);
				btn_sport_end_sport_nosend = (Button)layout.findViewById(R.id.btn_sport_end_sport_nosend);
				/*
				 * 完成运动按钮(上传运动数据)
				 */
				btn_sport_oversport.setOnClickListener (new OnClickListener() {					
					@Override
					public void onClick(View arg0) {
						over_flag = true;
						chronometer.stop();//计时器停止		
						Constant.end_flag = 1;//结束标志位置1
						Constant.move_flag = 0;
						thread_flag = false;//线程标志位置false
						menuWindow.dismiss(); //响应点击事件之后关闭Menu
						if((distance_sum-0.0)<1e-8){
							Toast.makeText(SportActivity.this, "未采集到有效数据", Toast.LENGTH_SHORT).show();
						}
						else{
							sendStatusDialog = new Dialog(SportActivity.this);
							sendStatusDialog.show();
							sendStatusDialog.setCancelable(false);
							sendStatusDialog.setContentView(R.layout.activity_login_loading_dialog_view);
							Status status  = new Status(); 
							status.setDistance(distance_show);
							status.setCalories(calorie_sum);
							status.setType(MainTopDialog.how_sport_flag);
							status.setRunTime(time_sum);
							new SportService(SportActivity.this).sportService(status, sendStatusDialog);
						}
						overButton.setText("返回");
						overButton.setTextColor(Color.rgb(255, 236, 139));
						}
					});	
				/*
				 * 完成运动按钮(不上传运动数据)
				 */
				btn_sport_end_sport_nosend.setOnClickListener(new OnClickListener() {		
					@Override
					public void onClick(View arg0) {
						over_flag = true;
						chronometer.stop();//计时器停止		
						Constant.end_flag = 1;//结束标志位置1
						Constant.move_flag = 0;
						thread_flag = false;//线程标志位置false
						menuWindow.dismiss(); //响应点击事件之后关闭Menu
						overButton.setText("返回");
						overButton.setTextColor(Color.rgb(255, 236, 139));
					}
				});
				/*
				 * 取消按钮
				 */
				btn_sport_cancle_menu.setOnClickListener(new OnClickListener() {		
					@Override
					public void onClick(View arg0) {
						menuWindow.dismiss(); //响应点击事件之后关闭Menu					
					}
				});
				break;
			}
		}
	}
	// 画Markert
	private boolean DrawMarker_Sport(double lat, double lng, int index) {
		GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
		if (index == Constant.MARKERT_START) {//等于开始点  开始画线，会在图上显示开始点
			overlay_item_start = new OverlayItem(point, "起点", "起点");
			overlay_item_start.setMarker(getResources().getDrawable(
			R.drawable.bubble_start));
			pointOVerlay.addItem(overlay_item_start);
		} else {//显示终点
			overlay_item_end = new OverlayItem(point, "终点", "终点");
			overlay_item_end.setMarker(getResources().getDrawable(
			R.drawable.bubble_end));		
			pointOVerlay.addItem(overlay_item_end);
		}
		mMapView.refresh();
			return true;
	}
	/*
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {  //获取 back键
        }
    	else if(keyCode == KeyEvent.KEYCODE_MENU){   //获取 Menu键		
    		return false;
    	}
    	return false;
	}
}
