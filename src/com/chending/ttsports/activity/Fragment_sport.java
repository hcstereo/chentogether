﻿package com.chending.ttsports.activity;
import java.util.HashMap;
import java.util.Map;
import android.location.GpsStatus;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.Geometry;
import com.baidu.mapapi.map.Graphic;
import com.baidu.mapapi.map.GraphicsOverlay;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.Symbol;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKShareUrlResult;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.Projection;
import com.baidu.platform.comapi.map.l;
import com.baidu.platform.comapi.map.v;
import com.chaowen.yixin.R;
import com.chending.ttsports.activity.DemoApplication;
import com.chending.ttsports.activity.StartActivity.returnThread;
import com.chending.ttsports.activity.StartActivity.showThread;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.PointOverlay;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChangeInfoService;
import android.R.integer;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;  
import android.graphics.Color;  
import android.graphics.Paint;  
import android.graphics.Paint.Style;  
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Path;  
import android.graphics.Point;  
import java.lang.Math;
import org.ksoap2.serialization.SoapObject;
import com.chending.ttsports.model.WebServiceUtils;
import com.chending.ttsports.model.WebServiceUtils.WebServiceCallBack;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenedListener;

public class Fragment_sport extends Fragment{
	public static final String TAG="Fragment_sport";
	private  Dialog sendInfoDialog;
	private  static MapView mmMapView = null;
    private MapController mMapController = null;
	private LocationClient mLocClient;
    public MyLocationListenner myListener = new MyLocationListenner();//监听函数
    private MyLocationOverlay myLocationOverlay = null;
    private LocationData locData = null;
    private ImageButton ib_sport_choose;
    private PointOverlay pointOVerlay = null;
    private LinearLayout lin_sport_start_sport;
    private ImageView iv_sport_start_sport;
    private OverlayItem overlay_item_mylocation = null;
    private SharedPreferencesTool preferencesTool;
    private String methodName = "getWeatherbyCityName";
	private TextView w1, w2,w3,w4;
	private TextView tv_sport_cishu, tv_sport_time,tv_sport_distance,tv_sport_caluli;
	private ImageView img1, img2,img3, img4;
	private int regional_number = 0;
	private MKSearch mSearch = null;                 // 搜索模块，也可去掉地图模块独立使用
    static Fragment_sport newInstance() {
        Fragment_sport f = new Fragment_sport();
        return f;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DemoApplication app = (DemoApplication) getActivity().getApplication();
        if (app.mBMapManager == null) {
            app.mBMapManager = new BMapManager(getActivity());
            app.mBMapManager.init(DemoApplication.strKey, new DemoApplication.MyGeneralListener());
        }
        initListener();
        // 初始化搜索模块，注册事件监听
        mSearch = new MKSearch();
        mSearch.init(app.mBMapManager, new MKSearchListener() {

			@Override
			public void onGetAddrResult(MKAddrInfo res, int error) {
				// TODO 自动生成的方法存根
				if (error != 0) {
					return;
				}
				if (res.type == MKAddrInfo.MK_REVERSEGEOCODE) {
					// 反地理编码：通过坐标点检索详细地址及周边poi
					String strInfo = res.strAddr;
					int i = strInfo.indexOf("省");
					int j = strInfo.indexOf("市");
					strInfo	 = strInfo.substring(i+1,j);
					Constant.city = strInfo;
				}
			}
			@Override
			public void onGetBusDetailResult(MKBusLineResult arg0, int arg1) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetDrivingRouteResult(MKDrivingRouteResult arg0,
					int arg1) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetPoiDetailSearchResult(int arg0, int arg1) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetPoiResult(MKPoiResult arg0, int arg1, int arg2) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetShareUrlResult(MKShareUrlResult arg0, int arg1,
					int arg2) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetTransitRouteResult(MKTransitRouteResult arg0,
					int arg1) {
				// TODO 自动生成的方法存根
				
			}
			@Override
			public void onGetWalkingRouteResult(MKWalkingRouteResult arg0,
					int arg1) {
				// TODO 自动生成的方法存根
			}
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sport, container, false);
        mmMapView = (MapView) v.findViewById(R.id.bmapView);
        initmap();
		ib_sport_choose  = (ImageButton)v.findViewById(R.id.ib_sport_choose);
		lin_sport_start_sport = (LinearLayout)v.findViewById(R.id.lin_sport_start_sport);
		iv_sport_start_sport = (ImageView)v.findViewById(R.id.iv_sport_start_sport);
		tv_sport_cishu = (TextView)v.findViewById(R.id.tv_sport_cishu);
		tv_sport_time = (TextView)v.findViewById(R.id.tv_sport_time);
		tv_sport_distance = (TextView)v.findViewById(R.id.tv_sport_distance);
		tv_sport_caluli = (TextView)v.findViewById(R.id.tv_sport_caluli);
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(getActivity());
		String timeString = preferencesTool.getUserPrefences().get("sumTime");
		if(timeString == null){
			tv_sport_time.setText("0");
		}
		else{
			tv_sport_time.setText(timeString);
		}
		String distanceString = preferencesTool.getUserPrefences().get("sumDis");
		if(distanceString == null){
			tv_sport_distance.setText("0");
		}
		else{
			tv_sport_distance.setText(distanceString);
		}
		String calString = preferencesTool.getUserPrefences().get("sumCal");
		if(calString == null){
			tv_sport_caluli.setText("0");
		}
		else{
			tv_sport_caluli.setText(calString);
		}
		String cishuString = preferencesTool.getUserPrefences().get("sumTimes");
		if(cishuString == null){
			tv_sport_cishu.setText("0");
		}
		else{
			tv_sport_cishu.setText(cishuString);
		}
		addListener();
		initwether(v);
		init();
        return v;
    }
    private void initListener(){
    	 mLocClient = new LocationClient(getActivity());
         locData = new LocationData();
         mLocClient.registerLocationListener( myListener );//注册监听事件
         LocationClientOption option = new LocationClientOption();
         option.setOpenGps(true);// 打开gps
         option.setCoorType("bd09ll"); // 设置坐标类型        option.setScanSpan(5000);
         option.setScanSpan(1000);
         option.setPriority(LocationClientOption.GpsFirst); // 设置GPS优先 
         mLocClient.setLocOption(option);
         mLocClient.start();
    }
    /*
     * 初始化天气
     */
    private void initwether(View v){
    	w1 = (TextView) v.findViewById(R.id.weather1);
		w2 = (TextView) v.findViewById(R.id.weather2);
		w3 = (TextView) v.findViewById(R.id.weather3);
		w4 = (TextView) v.findViewById(R.id.weather4);
		img1 = (ImageView) v.findViewById(R.id.w_img1);
		img2 = (ImageView) v.findViewById(R.id.w_img2);
		img3 = (ImageView) v.findViewById(R.id.w_img3);
		img4 = (ImageView) v.findViewById(R.id.w_img4);
		img1.setImageDrawable(GetWetherImage(Constant.Image_wether1));
		img2.setImageDrawable(GetWetherImage(Constant.Image_wether2));
		img3.setImageDrawable(GetWetherImage(Constant.Image_wether3));
		img4.setImageDrawable(GetWetherImage(Constant.Image_wether4));
		w1.setText(Constant.w1);
		w3.setText(Constant.w3);
		w2.setText(Constant.w2);
		w4.setText(Constant.w4);
    }
    /*
     * 初始化地图
     */
    private void initmap() {
    	 mMapController = mmMapView.getController();
         mmMapView.getController().setZoom(16);//设置地图放大级别
         mmMapView.getController().enableClick(true);
        // mMapView.setBuiltInZoomControls(true); // 设置是否出现放大缩小的丑陋按钮
         myLocationOverlay = new MyLocationOverlay(mmMapView);
         locData = new LocationData();
         myLocationOverlay.setData(locData);
         mmMapView.getOverlays().add(myLocationOverlay);
         pointOVerlay = new PointOverlay(null,getActivity(), mmMapView);//图层初始化
         mmMapView.getOverlays().add(pointOVerlay);//添加图标图层
         myLocationOverlay.enableCompass();
         mmMapView.refresh();
	}
    /*
     * 初始化天气
     */
    private void init() {
		HashMap<String, String> properties = new HashMap<String, String>();
		properties.put("theCityName", Constant.city);
		WebServiceUtils.CallWebService(methodName, properties,
				new WebServiceCallBack() {

					@Override
					public void CallBack(SoapObject result) {
						if (result != null) {
							SoapObject detail = (SoapObject) result
									.getProperty(methodName + "Result");
							ParseSoapObject(detail);
						} else {
							//Toast.makeText(getActivity(),
								//	"获取天气数据错误", Toast.LENGTH_SHORT)
									//.show();
						}
					}
				});
	}
    /**
	 * 添加监听
	 */
	private void addListener(){
		lin_sport_start_sport.setOnClickListener(new btnClickListener());
	}
	
	private class btnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.lin_sport_start_sport: //点击开始运动按钮
				if(!isOPen()){
					Intent intent = new Intent(getActivity(),GpsOpen.class);
					startActivity(intent);
    			}
				else{
					Intent intent = new Intent(getActivity(),SportActivity.class);
					startActivity(intent);
				}
				break;
			default:
				break;
			}
		}
	}
   
    /**
     * 监听函数，有新位置的时候，格式化成字符串，输出到屏幕中
     */
    public class MyLocationListenner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null)
                return;
            locData.latitude = location.getLatitude();
            locData.longitude = location.getLongitude();
            locData.accuracy = location.getRadius();
            locData.direction = location.getDerect();
            myLocationOverlay.setData(locData);
            mmMapView.refresh();
            mMapController.animateTo(new GeoPoint((int) (locData.latitude * 1e6), (int) (locData.longitude * 1e6)));
            DrawMarker_Fsport(locData.latitude, locData.longitude);//画出我的位置
            alterLatLong();//更新我的位置
            GeoPoint point = new GeoPoint((int) (locData.latitude * 1E6), (int) (locData.longitude * 1E6));
            mSearch.reverseGeocode(point);
            stopRequestLocation();
        }
        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
                return;
            }
        }
    }
    private void  alterLatLong(){
    	 /*
		 * 修改本地数据库
		 */
    	regional_number = regional_number(locData.latitude,locData.longitude);
		Map<String, String> map=new HashMap<String, String>();
		map.put("longitude",String.valueOf(locData.longitude));
		map.put("latitude",String.valueOf(locData.latitude));
		map.put("areaNo",String.valueOf(regional_number));
		preferencesTool.saveUserPrefences(map);
		/*
		 * 修改服务器数据
		 */
		sendInfoDialog = new Dialog(getActivity());
		//sendInfoDialog.show();
		sendInfoDialog.setCancelable(false);
		sendInfoDialog.setContentView(R.layout.activity_login_loading_dialog_view);
		User user  = new User(); 
		String avatar=preferencesTool.getUserPrefences().get("avatar");
		String arrays[]=avatar.split("/");
		avatar="/file/1/avastar/"+arrays[arrays.length-1];
    	String weight=preferencesTool.getUserPrefences().get("weight");
    	String phone=preferencesTool.getUserPrefences().get("phone");
    	String gender=preferencesTool.getUserPrefences().get("gender");
    	String height=preferencesTool.getUserPrefences().get("height");
    	String hobby=preferencesTool.getUserPrefences().get("hobby");
    	String signature=preferencesTool.getUserPrefences().get("signature");
    	String regTime=preferencesTool.getUserPrefences().get("regTime");
    	String userId=preferencesTool.getUserPrefences().get("userId");
    	String password=preferencesTool.getUserPrefences().get("password");
    	String username=preferencesTool.getUserPrefences().get("username");
    	String latitude=preferencesTool.getUserPrefences().get("latitude");
    	String longitude=preferencesTool.getUserPrefences().get("longitude");
    	int areaNo =  regional_number;
    	user.setLatitude(Double.parseDouble(latitude));
    	user.setLongitude(Double.parseDouble(longitude));
    	user.setUsername(username);
    	user.setUserId(Integer.valueOf(userId));
    	user.setAvatar(avatar);
    	user.setGender(gender);
    	user.setHeight(Double.valueOf(height));
    	user.setAreaNo(areaNo);
    	if(weight!=null){
    		user.setWeight(Double.valueOf(weight));
    	}
    	user.setHobby(hobby);
    	user.setPassword(password);;
    	user.setPhone(phone);
    	user.setSignature(signature);
		new ChangeInfoService(getActivity()).changeLocService(user,sendInfoDialog);
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser == true) {
            startRequestLocation();
        } else if (isVisibleToUser == false) {
            stopRequestLocation();
        }
    }
    private void stopRequestLocation() {
        if (mLocClient != null) {
            mLocClient.unRegisterLocationListener(myListener);
            mLocClient.stop();
        }
    }
    private void startRequestLocation() {
        if (mLocClient != null) {
            mLocClient.registerLocationListener(myListener);
            mLocClient.start();
            mLocClient.requestLocation();
        }
    }
    @Override
    public void onPause() {
        mmMapView.onPause();
        super.onPause();
    }
    @Override
    public void onResume() {
        mmMapView.onResume();
        super.onResume();
        /*
         * 改变底部运动图标
         */
        if(MainTopDialog.how_sport_flag == 1)
        	iv_sport_start_sport.setBackgroundResource(R.drawable.walking_top_dialog);
        else if(MainTopDialog.how_sport_flag == 2)
        	iv_sport_start_sport.setBackgroundResource(R.drawable.runing_top_dialog);
        else if(MainTopDialog.how_sport_flag == 3)
        	iv_sport_start_sport.setBackgroundResource(R.drawable.skating_top_dialog);
        else if(MainTopDialog.how_sport_flag == 4)
        	iv_sport_start_sport.setBackgroundResource(R.drawable.skiing_top_dialog);
        else {
        	iv_sport_start_sport.setBackgroundResource(R.drawable.cycling_top_dialog);
		}
    }
    @Override
    public void onDestroy() {
        if (mLocClient != null)
            mLocClient.stop();
        mmMapView.destroy();
        super.onDestroy();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mmMapView.onSaveInstanceState(outState);
    }
 // 画Markert
  	private boolean DrawMarker_Fsport(double lat, double lng) {
  		GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
  		overlay_item_mylocation = new OverlayItem(point, "人", "人");
  			overlay_item_mylocation.setMarker(getResources().getDrawable(
  			R.drawable.item_fragment_sport_mylocation));
  			pointOVerlay.addItem(overlay_item_mylocation);
  		mmMapView.refresh();
  			return true;
  	}
  	/*
  	 * 计算属于哪个区域
  	 */
  	public int regional_number(double lat, double lng){//纬度  经度
  		int i = 0,j = 0,num = 0,temp,count = 0;
  		i = (int) ((lat-4)/(0.5)+1);//属于哪一行
  		j = (int) ((lng-73)/(0.6)+1);//属于哪一列
  		temp = j;
  		while(temp != 0)
  		{
  			temp = temp/10;
  			count++;
  		}
  		num = (int) (i*Math.pow(10.0,(double)count)+j);
  		return num;
  	}
  	/**
	 * 解析SoapObject对象 - 城市今日天气数据
	 */
	private void ParseSoapObject(SoapObject detail) {
		// 数据解析
		String sb = "";
		String temp = "";
		String temp1 = "";
		sb += "今日天气：" + detail.getProperty(5) ;
		Constant.w1 = sb.toString();
		w1.setText(sb.toString());
		sb = detail.getProperty(6) + " " + detail.getProperty(7);
		Constant.w3 = sb.toString();
		w3.setText(sb.toString());
		sb = detail.getProperty(11) + "\n";
		int i = sb.indexOf("运动指数");
		int j = sb.indexOf("洗车指数");
		if(1!=-1&&j!=-1){
			System.out.println(i);
			System.out.println(j);
			System.out.println(sb.length());
			System.out.println(sb);
			temp = sb.substring(i,j);
			i = 0;
			int k = temp.indexOf("。");
			temp1 = temp.substring(i,k);
			Constant.w2 = temp1.toString();
			w2.setText(temp1.toString());
			temp1 = temp.substring(k+1);
			Constant.w4 = temp1.toString();
			w4.setText(temp1.toString());
			Constant.Image_wether1 = detail.getProperty(8).toString();
			Constant.Image_wether2 = detail.getProperty(9).toString();
			Constant.Image_wether3 = detail.getProperty(15).toString();
			Constant.Image_wether4 = detail.getProperty(20).toString();
			img1.setImageDrawable(GetWetherImage(Constant.Image_wether1));
			img2.setImageDrawable(GetWetherImage(Constant.Image_wether2));
			img3.setImageDrawable(GetWetherImage(Constant.Image_wether3));
			img4.setImageDrawable(GetWetherImage(Constant.Image_wether4));
		}
	}
	/**
	 * 获取图片资源ID
	 * 
	 * @param imageName
	 * @return
	 */
	private Drawable GetWetherImage(String imageName) {
		imageName = "a_" + imageName.substring(0, imageName.indexOf('.')); // 对应图片名称
		return getResource(imageName);
	}

	/**
	 * 获取图片名称获取图片的资源id的方法
	 * 
	 * @param imageName
	 * @return
	 */
	private Drawable getResource(String imageName) {
		int resId = getResources().getIdentifier(imageName, "drawable",
				getActivity().getPackageName());
		Drawable image = null;
		if (resId <= 0) {
			image = getActivity().getBaseContext().getResources().getDrawable(R.drawable.b_nothing);
		} else {
			image = getActivity().getResources().getDrawable(resId);
		}
		return image;
	}
	/**
	 * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
	 * @param context
	 * @return true 表示开启
	 */
	public boolean isOPen() {
		LocationManager locationManager 
		                         = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
		boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (gps ) {
			return true;
		}
		return false;
	}
}
