package com.chending.ttsports.activity;

import java.util.ArrayList;
import java.util.List;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.common.FriendListAdapter;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.util.ModelTool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class SearchNativeFriendActivity extends Activity{
	private ListView lv_search_friend_list;
	private EditText edt_search_native_input; 
	private TextView tv_search_friend_cancel;
	private List<FriendsVO> friendsVOs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_searchnative_friend);
		init();
		addListener();
	}
	private void init(){
		lv_search_friend_list=(ListView) findViewById(R.id.lv_search_friend_list);
		edt_search_native_input=(EditText) findViewById(R.id.edt_search_native_input);
		tv_search_friend_cancel=(TextView) findViewById(R.id.tv_search_friend_cancel);
	}
	private void addListener(){
		edt_search_native_input.addTextChangedListener(new OnTextChangeListener());
		tv_search_friend_cancel.setOnClickListener(new btnClickListener());
		lv_search_friend_list.setOnItemClickListener(new ItemClickListener());
	}
	private class btnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.tv_search_friend_cancel:
				finish();
				break;

			default:
				break;
			}
		}
		
	}
	private class OnTextChangeListener implements TextWatcher {

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			
		}

		@Override
		public void afterTextChanged(Editable s) {
			friendsVOs=new ArrayList<FriendsVO>();
			for(FriendsVO friendsVO:FriendService.friendsVOs){
				if(!s.toString().trim().equals("")&&friendsVO.getUsername().contains(s.toString())){
					friendsVOs.add(friendsVO);
				}
			}
			FriendListAdapter adapter=new FriendListAdapter(SearchNativeFriendActivity.this, ModelTool.toImageAndText(friendsVOs), lv_search_friend_list);
			lv_search_friend_list.setAdapter(adapter);
		}
	}
	private class ItemClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			FriendsVO friendsVO=friendsVOs.get(position);
			Intent intent=new Intent(SearchNativeFriendActivity.this,CheckDataActivity.class);
			FriendService.getUser().setUsername(friendsVO.getUsername());
			FriendService.getUser().setGender((friendsVO.getGender()));
			FriendService.getUser().setAvatar((friendsVO.getAvatar()));
			FriendService.getUser().setSignature((friendsVO.getSignature()));
			FriendService.getUser().setHobby((friendsVO.getHobby()));
			startActivity(intent);
			finish();
		}
	}
}
