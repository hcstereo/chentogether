package com.chending.ttsports.activity;

import java.security.PublicKey;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.GraphicsOverlay;
import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MKMapViewListener;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.Symbol;
import com.baidu.mapapi.map.TextItem;
import com.baidu.mapapi.map.TextOverlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.Projection;
import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendNearVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.PointOverlay;
import com.chending.ttsports.service.YuepaoService;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.common.MyOverlayItem;;

public class ShowNearbyPeopleActivity extends Activity {
	private MapView mMapView = null;	
	private MapController mMapController = null;
	private MKMapViewListener mMapListener = null;	
	private LocationClient mLocClient;
	public LocationListenner myListener = new LocationListenner();//监听函数
	private MyLocationOverlay myLocationOverlay = null;//	//定位图层
	private LocationData locData = null;//位置信息
	private PointOverlay pointOVerlay = null;//附近的人为之图层
	private LinearLayout lin_show_nearby_people_back;
	private TextView tv_show_people_gxqm,tv_show_people_username,tv_show_people_distance,tv_show_people_sex;
	private MyOverlayItem[] overlay_item = new MyOverlayItem[100];  
	private ImageView iv_show_people_touxiang;
	private ImageButton imbtnChat;
	private int friendId;
	private String username;
	private String avatarOp;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DemoApplication app = (DemoApplication)this.getApplication();
        if (app.mBMapManager == null) {
            app.mBMapManager = new BMapManager(this);
            app.mBMapManager.init(DemoApplication.strKey,new DemoApplication.MyGeneralListener());
        }
        setContentView(R.layout.show_nearby_people);
        init();
        addListenner();
        try {
        	mLocClient = new LocationClient(this);
        	locData = new LocationData();
        	mLocClient.registerLocationListener( myListener );//注册监听事件
        	LocationClientOption option = new LocationClientOption();
        	option.setOpenGps(true);// 打开gps
        	option.setCoorType("bd09ll"); // 设置坐标类型        
        	option.setScanSpan(1000);
        	option.setPriority(LocationClientOption.GpsFirst); // 设置GPS优先 
        	mLocClient.setLocOption(option);
        	mLocClient.start();
		} catch (Exception e) {
			Toast.makeText(app, "GPS信号差", Toast.LENGTH_SHORT).show();
		}
        mMapView = (MapView)findViewById(R.id.show_nearby_people_mapView);
        mMapController = mMapView.getController();
        mMapController.enableClick(true);
        mMapController.setZoom(15);
        myLocationOverlay = new MyLocationOverlay(mMapView);
        locData = new LocationData();
        myLocationOverlay.setData(locData);
        mMapView.getOverlays().add(myLocationOverlay);//添加位置图层
        pointOVerlay = new PointOverlay(null, this, mMapView);//开始和结束图标图层初始化
        mMapView.getOverlays().add(pointOVerlay);//添加开始和结束图标图层
        myLocationOverlay.enableCompass();
        if(!YuepaoService.friendNearVOs.isEmpty()){
        	 for(int i=0;i<YuepaoService.friendNearVOs.size();i++)    {   
        		 FriendNearVO  friendnearvo = YuepaoService.friendNearVOs.get(i);   
        		// DrawMarker(friendnearvo.getLatitude(), friendnearvo.getLongitude());
        		 Double lat = friendnearvo.getLatitude(),lng =  friendnearvo.getLongitude();
        		 GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
      			OverlayItem overlayitem1 = new OverlayItem(point,String.valueOf(i),String.valueOf(i)); 
      			Drawable mark= getResources().getDrawable(R.drawable.show_nearby_people); 
      			overlay_item[i] = new MyOverlayItem(mark, mMapView);    
      			mMapView.getOverlays().add(overlay_item[i]);    
      			overlay_item[i].addItem(overlayitem1);  
      			//overlay_item[i].onTap(i);
      			mMapView.refresh();  
      			Constant.friendnum = i;
        	   }   
        }
        FriendNearVO  friendnearvo1 = YuepaoService.friendNearVOs.get(0);  
        tv_show_people_distance.setText(String.valueOf(friendnearvo1.getDistanceRecent()));
        tv_show_people_username.setText(friendnearvo1.getUsername());
        tv_show_people_gxqm.setText(friendnearvo1.getSignature());
        tv_show_people_sex.setText(friendnearvo1.getGender());
        friendId=friendnearvo1.getUserId();
   	 	username=friendnearvo1.getUsername();
   	 	avatarOp=friendnearvo1.getAvatar();
        AsyncImageLoader.loadImage(friendnearvo1.getAvatar(), iv_show_people_touxiang);
        Double lat = friendnearvo1.getLatitude(),lng =  friendnearvo1.getLongitude();
   	 	GeoPoint myPoint = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
   	 	mMapController.setCenter(myPoint);
        // 添加图层到地图  
        mMapView.refresh();
        new TopThread().start();
        /**
    	 *  MapView的生命周期与Activity同步，当activity挂起时需调用MapView.onPause()
    	 */
        mMapListener = new MKMapViewListener() {
			@Override
			public void onMapMoveFinish() {
				/**
				 * 在此处理地图移动完成回调
				 * 缩放，平移等操作完成后，此回调被触发
				 */
			}
			
			@Override
			public void onClickMapPoi(MapPoi mapPoiInfo) {
				/**
				 * 在此处理底图poi点击事件
				 * 显示底图poi名称并移动至该点
				 * 设置过： mMapController.enableClick(true); 时，此回调才能被触发
				 * 
				 */
				String title = "哈哈哈";
				if (mapPoiInfo != null){
					title = mapPoiInfo.strText;
					Toast.makeText(ShowNearbyPeopleActivity.this,title,Toast.LENGTH_SHORT).show();
					mMapController.animateTo(mapPoiInfo.geoPt);
				}
			}
			@Override
			public void onGetCurrentMap(Bitmap b) {
				/**
				 *  当调用过 mMapView.getCurrentMap()后，此回调会被触发
				 *  可在此保存截图至存储设备
				 */
			}
			@Override
			public void onMapAnimationFinish() {
				/**
				 *  地图完成带动画的操作（如: animationTo()）后，此回调被触发
				 */
			}
			@Override
			public void onMapLoadFinish() {
				// TODO 自动生成的方法存根
				
			}
		}; 
		mMapView.regMapViewListener(DemoApplication.getInstance().mBMapManager, mMapListener);
    }
    public class TopThread extends Thread {
        @SuppressWarnings("deprecation")
		@Override
        public void run () {
            do {
                try {
                    Thread.sleep(200);
                    Message msg = new Message();
                    msg.what = 10;
                    if(Constant.istop == true){
                    	Constant.istop = false;
                    	mHandler.sendMessage(msg);
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while(true);
        }
    }
    @SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
        @Override
        public void handleMessage (Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 10:
                	for(int i=0;i<YuepaoService.friendNearVOs.size();i++){
                		if(overlay_item[i].istap){
                			FriendNearVO  friendnearvo = YuepaoService.friendNearVOs.get(i);   
                       	 	Double lat = friendnearvo.getLatitude(),lng =  friendnearvo.getLongitude();
                       	 	GeoPoint myPoint = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
                       	 	mMapController.setCenter(myPoint);
                       	 	overlay_item[i].istap = false;
                       	 	tv_show_people_gxqm.setText(friendnearvo.getSignature());
                       	 	tv_show_people_username.setText(friendnearvo.getUsername());
                       	 	tv_show_people_sex.setText(friendnearvo.getGender());
                       	 	tv_show_people_distance.setText(String.valueOf(friendnearvo.getDistanceRecent()));
                       	 	AsyncImageLoader.loadImage(friendnearvo.getAvatar(), iv_show_people_touxiang);
                       	 	friendId=friendnearvo.getUserId();
                       	 	username=friendnearvo.getUsername();
                       	 	avatarOp=friendnearvo.getAvatar();
                		}
                	}
                    break;
                default:
                    break;
            }
        }
    };
    private void init() {
    	lin_show_nearby_people_back = (LinearLayout)findViewById(R.id.lin_show_nearby_people_back);
    	tv_show_people_sex = (TextView)findViewById(R.id.tv_show_people_sex);
    	tv_show_people_username = (TextView)findViewById(R.id.tv_show_people_username);
    	tv_show_people_distance = (TextView)findViewById(R.id.tv_show_people_distance);
    	tv_show_people_gxqm = (TextView)findViewById(R.id.tv_show_people_gxqm);
    	iv_show_people_touxiang= (ImageView)findViewById(R.id.iv_show_people_touxiang);
    	imbtnChat=(ImageButton) findViewById(R.id.imbtnChat);
	}
    private void addListenner() {
    	lin_show_nearby_people_back.setOnClickListener(new BtnClickListener());
    	imbtnChat.setOnClickListener(new BtnClickListener());
	}
    private class BtnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.lin_show_nearby_people_back:
				finish();
				break;
			case R.id.imbtnChat:
				Intent intent=new Intent(ShowNearbyPeopleActivity.this,ChatActivity.class);
				Bundle bundle=new Bundle();
				bundle.putString("username", username);
				bundle.putInt("friendId", friendId);
				bundle.putString("avatarOp", avatarOp);
				intent.putExtras(bundle);
				startActivity(intent);
				break;
			default:
				break;
			}
		}
	}
    /**
     * 监听函数
     */
    public class LocationListenner implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (location == null)
                return;
            locData.latitude = location.getLatitude();
            locData.longitude = location.getLongitude();
            locData.accuracy = location.getRadius();
            locData.direction = location.getDerect();
            myLocationOverlay.setData(locData);
            mMapView.refresh();
            mMapController.animateTo(new GeoPoint((int) (locData.latitude * 1e6), (int) (locData.longitude * 1e6)));
            DrawMarker_My(locData.latitude, locData.longitude);
            stopRequestLocation();
            
        }
        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
                return;
            }
        }
    }
	private void stopRequestLocation() {
        if (mLocClient != null) {
            mLocClient.unRegisterLocationListener(myListener);
            mLocClient.stop();
        }
    }
    @SuppressWarnings("unused")
	private void startRequestLocation() {
        // this nullpoint check is necessary
        if (mLocClient != null) {
            mLocClient.registerLocationListener(myListener);
            mLocClient.start();
            mLocClient.requestLocation();
        }
    }
    @Override
    protected void onPause() {
    	/**
    	 *  MapView的生命周期与Activity同步，当activity挂起时需调用MapView.onPause()
    	 */
        mMapView.onPause();
        super.onPause();
    }
    @Override
    protected void onResume() {
    	/**
    	 *  MapView的生命周期与Activity同步，当activity恢复时需调用MapView.onResume()
    	 */
        mMapView.onResume();
        super.onResume();
    }
    @Override
    protected void onDestroy() {
    	/**
    	 *  MapView的生命周期与Activity同步，当activity销毁时需调用MapView.destroy()
    	 */
        mMapView.destroy();
        super.onDestroy();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	mMapView.onSaveInstanceState(outState);
    	
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	mMapView.onRestoreInstanceState(savedInstanceState);
    }
 // 画Markert
 	private boolean DrawMarker(double lat, double lng) {
 		GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
 			OverlayItem overlayitem = new OverlayItem(point,"item1","item1"); 
 			Drawable mark= getResources().getDrawable(R.drawable.show_nearby_people); 
 			//创建IteminizedOverlay    
 			MyOverlayItem overlay_item = new MyOverlayItem(mark, mMapView);    
 			//将IteminizedOverlay添加到MapView中    
 			mMapView.getOverlays().clear();    
 			mMapView.getOverlays().add(overlay_item);    
 			//现在所有准备工作已准备好，使用以下方法管理overlay.    
 			//添加overlay, 当批量添加Overlay时使用addItem(List<OverlayItem>)效率更高    
 			overlay_item.addItem(overlayitem);    
 			mMapView.refresh();    
 			return true;
 	}
 	private boolean DrawMarker_My(double lat, double lng) {
 		GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
 		OverlayItem overlayitem = new OverlayItem(point,"item1","item1"); 
			Drawable mark= getResources().getDrawable(R.drawable.show_my_location); 
			//创建IteminizedOverlay    
			MyOverlayItem overlay_item = new MyOverlayItem(mark, mMapView);    
			//将IteminizedOverlay添加到MapView中    
			mMapView.getOverlays().add(overlay_item);    
			//现在所有准备工作已准备好，使用以下方法管理overlay.    
			//添加overlay, 当批量添加Overlay时使用addItem(List<OverlayItem>)效率更高    
			overlay_item.addItem(overlayitem);   
 	        mMapView.refresh();
 			return true;
 	}
}
