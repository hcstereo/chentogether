package com.chending.ttsports.activity;

import java.util.HashMap;
import java.util.Map;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChangeInfoService;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AlterSignatureActivity extends Activity{
	Button btn_end_alter_signature;
	TextView tv_signature_return_gerenxinxi;
	EditText edt_alter_gerenxinxi_signature;
	private  Dialog sendInfoDialog;
	private SharedPreferencesTool preferencesTool;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alter_gerenxinxi_signature);
		init();
		addListener();
		String signature=preferencesTool.getUserPrefences().get("signature");
		edt_alter_gerenxinxi_signature.setHint(signature);
	}
	private void init() {
		btn_end_alter_signature = (Button)findViewById(R.id.btn_end_alter_signature);
		tv_signature_return_gerenxinxi = (TextView)findViewById(R.id.tv_signature_return_gerenxinxi);
		edt_alter_gerenxinxi_signature = (EditText)findViewById(R.id.edt_alter_gerenxinxi_signature);
		preferencesTool = SharedPreferencesTool.getSharedPreferencesToolInstance(this);
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_end_alter_signature.setOnClickListener(new BtnClickListener());
		tv_signature_return_gerenxinxi.setOnClickListener(new BtnClickListener());
	}
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_end_alter_signature:
				/*
				 * 修改本地数据库
				 */
				String temp_signature = edt_alter_gerenxinxi_signature.getText().toString();
				Map<String, String> map=new HashMap<String, String>();
				map.put("signature", temp_signature);
				preferencesTool.saveUserPrefences(map);
				/*
				 * 修改服务器数据库
				 */
				sendInfoDialog = new Dialog(AlterSignatureActivity.this);
				sendInfoDialog.show();
				sendInfoDialog.setCancelable(false);
				sendInfoDialog.setContentView(R.layout.activity_login_loading_dialog_view);
				User user  = new User(); 
				String avatar=preferencesTool.getUserPrefences().get("avatar");
	        	String weight=preferencesTool.getUserPrefences().get("weight");
	        	String gender=preferencesTool.getUserPrefences().get("gender");
	        	String phone=preferencesTool.getUserPrefences().get("phone");
	        	String hobby=preferencesTool.getUserPrefences().get("hobby");
	        	String height=preferencesTool.getUserPrefences().get("height");
	        	String regTime=preferencesTool.getUserPrefences().get("regTime");
	        	String userId=preferencesTool.getUserPrefences().get("userId");
	        	String password=preferencesTool.getUserPrefences().get("password");
	        	String username=preferencesTool.getUserPrefences().get("username");
	        	String latitude=preferencesTool.getUserPrefences().get("latitude");
	        	String longitude=preferencesTool.getUserPrefences().get("longitude");
	        	user.setLatitude(Double.parseDouble(latitude));
	        	user.setLongitude(Double.parseDouble(longitude));
	        	user.setUsername(username);
	        	user.setUserId(Integer.valueOf(userId));
	        	user.setAvatar(avatar);
	        	user.setGender(gender);
	        	user.setHeight(Double.valueOf(height));
	        	user.setWeight(Double.valueOf(weight));
	        	user.setHobby(hobby);
	        	user.setPassword(password);
	        	user.setPhone(phone);
	        	user.setSignature(temp_signature);
				new ChangeInfoService(AlterSignatureActivity.this).changeInfoService(user,sendInfoDialog);
				finish();
				break;
			case R.id.tv_signature_return_gerenxinxi://返回个人信息
				finish();
				break;
			default:
				break;
			}
		}
	}
	
	
}
