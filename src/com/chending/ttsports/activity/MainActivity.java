package com.chending.ttsports.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.service.MsgService;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

public class MainActivity extends SlidingFragmentActivity {
	public SlidingMenu mSlidingMenu;// 左边的SlidingMenu
	private Fragment mContent, titleBarMain;
	static final int NUM_ITEMS = 10;
	private PopupWindow menuWindow;
	private View layout;
	private boolean menu_display = false;
	private LayoutInflater inflater;
	private Fragment mainFragment, barFragment;

	@Override
	public void finish() {
		super.finish();
		unbindService(connection);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initSlidingMenu();
		SharedPreferencesTool preferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(this);
		if (preferencesTool.getUserPrefences().get("username") != null
				&& !Constant.IS_THREAD_START) {// has login
			Constant.IS_THREAD_START = true;
			Constant.IS_THREAD_SHUOULD_END = false;
			System.out.println("线程1开了");
			Intent intent = new Intent(this, MsgService.class);
			startService(intent);
			bindService(intent, connection, BIND_AUTO_CREATE);
		}
		Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			public void run() {
				if (!Constant.CURRENT_TIME.equals("")) {
					Constant.CURRENT_SECONDS++;
					System.out.println("Constant.CURRENT_SECONDS=="
							+ Constant.CURRENT_SECONDS);
				}
			}
		};
		timer.schedule(task, 1000, 1000);
		setContentView(R.layout.activity_main);
		initView();

	}

	private ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {

		}
	};

	/**
	 * 初始化左侧菜单
	 */
	private void initSlidingMenu() {
		DisplayMetrics ds = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(ds);
		int width = ds.widthPixels;
		mContent = new Fragment_sport();// 初始化桌面 为
		mainFragment = mContent;
		titleBarMain = new FragmentTitleBarMain();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, mContent,Fragment_sport.TAG)// activity_main中的framelayout
				.replace(R.id.title_bar_frame, titleBarMain,FragmentTitleBarMain.TAG).commit();
		setBehindContentView(R.layout.main_left_layout);// 设置左边的菜单布局
		FragmentTransaction mFragementTransaction = getSupportFragmentManager()
				.beginTransaction();
		Fragment mFrag = new LeftSlidingMenuFragment();
		mFragementTransaction.replace(R.id.main_left_fragment, mFrag);
		mFragementTransaction.commit();
		mSlidingMenu = getSlidingMenu();
		mSlidingMenu.setShadowDrawable(R.drawable.shadow);// 设置左菜单阴影图片
		mSlidingMenu.setFadeEnabled(true);// 设置滑动时菜单的是否淡入淡出
		mSlidingMenu.setFadeDegree(0.95f);// 设置淡入淡出的比例
		mSlidingMenu.setShadowWidth(30);
		mSlidingMenu.setBehindScrollScale(0.0f);// 设置滑动时拖拽效果
		mSlidingMenu.setBehindWidth((int) (width * 0.8));
		mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		Bundle bun = this.getIntent().getExtras();
		String SERVER_RUN_IN_BACHGROUND = bun
				.getString("SERVER_RUN_IN_BACHGROUND");
		if (SERVER_RUN_IN_BACHGROUND.equals("1")) {// 点击状态栏进入MainActivity
			Fragment newContent = new Fragment_ChatMsg();
			String TAGMAIN = Fragment_ChatMsg.TAG;
			String TAGBAR = FragmentTitleBarMsg.TAG;
			switchContent(newContent, 7, TAGMAIN, TAGBAR);
		}
	}

	/**
	 * 左侧菜单点击切换首页的内容
	 */
	public void switchContent(Fragment fragment, int i, String tagMain,
			String tagBar) {
		Fragment FRAGEMNTMain = getSupportFragmentManager().findFragmentByTag(
				tagMain);
		Fragment FRAGEMNTBar = getSupportFragmentManager().findFragmentByTag(
				tagBar);
		if (FRAGEMNTMain != null) {
			mContent = FRAGEMNTMain;
			System.out.println("Fragment is not null");
		} else {
			mContent = fragment;
		}
		Fragment titleBarFragment = null;
		switch (i) {
		case 0:
			titleBarFragment = new FragmentTitleBarMain();
			break;
		case 1:
			titleBarFragment = new FragmentTitleBarFriend();
			break;
		case 3:
			titleBarFragment = new FragmentTitleBarYuepao();
			break;
		case 4:
			titleBarFragment = new FragmentTitleBarPaiHangBang();
			break;
		case 5:
			titleBarFragment = new FragmentTitleBarGerenxinxi();
			break;
		case 6:
			titleBarFragment = new FragmentTitleBarAbout();
			break;
		case 7:
			titleBarFragment = new FragmentTitleBarMsg();
			break;
		case 8:
			titleBarFragment = new FragmentTitleBarHistory();
			break;
		case 9:
			titleBarFragment = new FragmentTitleBarGerenxinxi();
		default:
			break;
		}
		if (FRAGEMNTBar != null) {
			barFragment = FRAGEMNTBar;
		} else {
			barFragment = titleBarFragment;
		}
		if (mainFragment != null && mainFragment != mContent) {
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			if (i == 0) {
				if(FRAGEMNTMain!=null){
					transaction.remove(FRAGEMNTMain).commit();
					getSupportFragmentManager()
					.beginTransaction().hide(mainFragment)
					.add(R.id.content_frame, fragment, tagMain)
					.commit();
				}
				mainFragment = fragment;
			} else {
				if (!mContent.isAdded()) {
					transaction.hide(mainFragment)
							.add(R.id.content_frame, mContent, tagMain)
							.commit();
				} else {
					transaction.hide(mainFragment).show(mContent).commit(); // 隐藏当前的fragment，显示下一个
				}
				mainFragment = mContent;
			}
			
		}

		if (titleBarMain != null && titleBarMain != barFragment) {
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			if (!barFragment.isAdded()) {
				transaction.hide(titleBarMain)
						.add(R.id.title_bar_frame, barFragment, tagBar)
						.commit();
			} else {
				transaction.hide(titleBarMain).show(barFragment).commit(); // 隐藏当前的fragment，显示下一个
			}
			titleBarMain = barFragment;
		}
		getSlidingMenu().showContent();
	}

	private void initView() {
	}

	/*
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // 获取back键
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Exit.class);
			startActivity(intent);
		} else if (keyCode == KeyEvent.KEYCODE_MENU) { // 获取 Menu键
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, Exit.class);
			startActivity(intent);
		}
		return false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		Bundle bun = this.getIntent().getExtras();
		String SERVER_RUN_IN_BACHGROUND = bun
				.getString("SERVER_RUN_IN_BACHGROUND");
		if (SERVER_RUN_IN_BACHGROUND.equals("1")) {// 点击状态栏进入MainActivity
			Fragment newContent = new Fragment_ChatMsg();
			String TAGMAIN = Fragment_ChatMsg.TAG;
			String TAGBAR = FragmentTitleBarMsg.TAG;
			switchContent(newContent, 7, TAGMAIN, TAGBAR);
		}
	}

}
