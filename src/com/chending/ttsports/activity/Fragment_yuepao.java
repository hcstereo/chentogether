package com.chending.ttsports.activity;

import java.util.Map;
import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendNearVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.service.YuepaoService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Fragment_yuepao extends Fragment implements OnItemClickListener{
	public static final String TAG= "Fragment_yuepao";
	private YuepaoService service;
	private ListView lv_yuepao_list;
	private SharedPreferencesTool preferencesTool;
	private Map<String, String> map;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_yuepao, container, false);
		init(view);
		return view;
	}
	private void init(View view){
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(getActivity());
		map=preferencesTool.getUserPrefences();
		service=new YuepaoService(getActivity());
		lv_yuepao_list=(ListView) view.findViewById(R.id.lv_yuepao_list);
		int regionalNumber=service.regional_number(Double.parseDouble(map.get("latitude")),Double.parseDouble( map.get("longitude")));
		service.initService(regionalNumber, lv_yuepao_list);
		lv_yuepao_list.setOnItemClickListener(this);
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Constant.TAG_ENTER_CHACK_DATA=1;
		FriendNearVO friendNearVO=YuepaoService.friendNearVOs.get(position);
		Intent intent=new Intent(getActivity(),FriendNearInfo.class);
		Bundle bundle=new Bundle();
		//bundle.putStringArray("friendNear", new String[]{friendNearVO.getAvatar(),friendNearVO.getUsername(),friendNearVO.getRectype(),friendNearVO.getDistanceRecent(),friendNearVO.getGender(),friendNearVO.getHobby(),friendNearVO.getSignature()});
		bundle.putSerializable("friendNear", friendNearVO);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
}
