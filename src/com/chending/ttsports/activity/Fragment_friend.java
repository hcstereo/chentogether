package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.common.PullToRefreshViewFriend;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.common.PullToRefreshViewFriend.OnHeaderRefreshListener;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.FriendService;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Fragment_friend extends Fragment implements
		OnHeaderRefreshListener {
	public static final String TAG = "Fragment_friend";
	private ListView lv_friend_list;
	FriendService friendService;
	private PullToRefreshViewFriend friends_pull_refresh_view;
	public static View viewPullFresh;
	public static View headerViewSearch;
	private LinearLayout ll_constact_serach, lin_friends_main;
	private LinearLayout ll_friend_new;
	private static View view;
	private ImageView iv_friend_has_new;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_friend, container, false);
		init(view);
		addListener();
		return view;
	}
	private void initView(){
		
	}
	private void init(View view) {
		friendService = new FriendService((MainActivity) getActivity());
		lin_friends_main = (LinearLayout) view
				.findViewById(R.id.lin_friends_main);
		friends_pull_refresh_view = (PullToRefreshViewFriend) lin_friends_main
				.findViewById(R.id.friends_pull_refresh_view);
		friends_pull_refresh_view.setOnHeaderRefreshListener(this);
		headerViewSearch = LayoutInflater.from(getActivity()).inflate(
				R.layout.fragment_friends_search, null);
		iv_friend_has_new=(ImageView) headerViewSearch.findViewById(R.id.iv_friend_has_new);
		ll_constact_serach = (LinearLayout) headerViewSearch
				.findViewById(R.id.ll_constact_serach);
		lv_friend_list = (ListView) view.findViewById(R.id.lv_friend_list);
		lv_friend_list.addHeaderView(headerViewSearch);
		viewPullFresh = view.findViewById(R.id.friends_pull_refresh_view);
		ll_friend_new = (LinearLayout) view.findViewById(R.id.ll_friend_new);
		if (FriendService.friendsVOs.size() > 0) {
			// FragmentTitleBarFriend.pbar_friend_refresh.setVisibility(View.GONE);
			friendService.noRefreshFriendService(lv_friend_list);
		} else {
			friendService.friendService(
					Integer.parseInt(SharedPreferencesTool
							.getSharedPreferencesToolInstance(getActivity())
							.getUserPrefences().get("userId")), lv_friend_list);
		}
		
	}

	public void addListener() {
		ll_constact_serach.setOnClickListener(new BtnClickListener());
		lv_friend_list.setOnItemClickListener(new ItemClickListener());
		ll_friend_new.setOnClickListener(new BtnClickListener());
	}

	private class BtnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.ll_constact_serach:
				Intent intent = new Intent(getActivity(),
						SearchNativeFriendActivity.class);
				startActivity(intent);
				break;
			case R.id.ll_friend_new:
				Intent intent2 = new Intent(getActivity(),
						FriendReqActivity.class);
				startActivity(intent2);
				break;
			default:
				break;
			}
		}

	}

	private class ItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (position >= 1) {
				FriendsVO friendsVO = FriendService.friendsVOs
						.get(position - 1);
				if (friendsVO != null) {
					System.out.println(friendsVO.getUsername());
				}
				Intent intent = new Intent(getActivity(),
						CheckDataActivity.class);
				// while(FriendService.user == null);
				FriendService.getUser().setUsername(friendsVO.getUsername());
				FriendService.getUser().setGender((friendsVO.getGender()));
				FriendService.getUser().setAvatar((friendsVO.getAvatar()));
				FriendService.getUser()
						.setSignature((friendsVO.getSignature()));
				FriendService.getUser().setHobby((friendsVO.getHobby()));
				FriendService.getUser().setUserId(friendsVO.getFriendsId());
				startActivity(intent);
			}
		}
	}

	@Override
	public void onHeaderRefresh(PullToRefreshViewFriend view) {
		friendService.friendService(
				Integer.parseInt(SharedPreferencesTool
						.getSharedPreferencesToolInstance(getActivity())
						.getUserPrefences().get("userId")), lv_friend_list,
				friends_pull_refresh_view);
	}

	@Override
	public void onResume() {
		super.onResume();
		friendService.noRefreshFriendService(lv_friend_list);
		if(FriendService.friendRelVOs.size()>0){
			if(iv_friend_has_new!=null){
				iv_friend_has_new.setVisibility(View.VISIBLE);
			}
		}else{
			iv_friend_has_new.setVisibility(View.GONE);
		}
		
		
	}
}
