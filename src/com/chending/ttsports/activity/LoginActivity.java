package com.chending.ttsports.activity;
import java.util.Map;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.UserLoginWidgetModel;
import com.chending.ttsports.protocol.LoginProtocol;
import com.chending.ttsports.service.ChatService;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.service.LoginService;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
/**
 * 用户登录页面Activity,处理用户登录
 */
public class LoginActivity extends Activity {
	TextView tv_login_login, tv_login_register, tv_login_try;// 分别获取登陆，注册和试用版的 按钮
	private Dialog loginLoadingDialog;
	private RelativeLayout rl_user;
	private Context mContext;
	private EditText edt_login_username,edt_login_password;
	private LoginService loginService=new LoginService(this);
	private SharedPreferencesTool preferencesTool;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//清除用到的静态信息
		mContext=this;
		setContentView(R.layout.activity_login);
		init();
		addListener();
		Map<String, String> params=preferencesTool.getUserPrefences();
		if(params.get("username")!=null){//已经登录过
			Intent intent=new Intent(LoginActivity.this,MainActivity.class);
			Bundle bundle=new Bundle();
            bundle.putString("SERVER_RUN_IN_BACHGROUND", "0");
            intent.putExtras(bundle);
			startActivity(intent);
			finish();
		}
	}
	/**
	 * 初始化组件
	 */
	public void init(){
		rl_user=(RelativeLayout) findViewById(R.id.rl_user);
		tv_login_login = (TextView) findViewById(R.id.tv_login_login);
		tv_login_register = (TextView) findViewById(R.id.tv_login_register);
		edt_login_username=(EditText) findViewById(R.id.edt_login_username);
		edt_login_password=(EditText) findViewById(R.id.edt_login_password);
		/*
		 * 设置动画
		 */
		Animation anim=AnimationUtils.loadAnimation(mContext, R.anim.login_anim);
		anim.setFillAfter(true);
		rl_user.startAnimation(anim);
		loginLoadingDialog=new Dialog(this,R.style.FullHeightDialog);
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(this);
		
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		tv_login_login.setOnClickListener(new BtnClickListener());
		tv_login_register.setOnClickListener(new BtnClickListener());
	}
	
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.tv_login_login://登录
				if(edt_login_username.getText().toString().trim().equals("")||edt_login_password.getText().toString().trim().equals("")){
					Toast.makeText(LoginActivity.this, "用户名和密码不能为空", Toast.LENGTH_SHORT).show();
					return;
				}
				UserLoginWidgetModel userLoginWidgetModel=new UserLoginWidgetModel();
				userLoginWidgetModel.setPassword(edt_login_password);
				userLoginWidgetModel.setUsername(edt_login_username);
				loginLoadingDialog.show();
				loginLoadingDialog.setCancelable(false);
				loginLoadingDialog.setTitle(null);
				loginLoadingDialog.setContentView(R.layout.activity_login_loading_dialog_view);
				loginService.loginService(userLoginWidgetModel,loginLoadingDialog);
				break;
			case R.id.tv_login_register://注册
				Intent intent1 = new Intent(LoginActivity.this,
						RegisterActivity.class);
				startActivity(intent1);
				finish();
				break;
			default:
				break;
			}
		}
	}
}
