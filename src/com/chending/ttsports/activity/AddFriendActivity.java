package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.service.FriendService;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AddFriendActivity extends Activity{
	private Button btn_addfriend_findfriend;
	private LinearLayout lin_addfriend_back,lin_addfriend_add;
	private EditText edt_addfriend_username;
	private FriendService friendService;
	private Dialog searchLoadingDialog;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addfriend);
		init();
		addListener();
	}
	/**
	 * 初始化组件
	 */
	public void init(){
		btn_addfriend_findfriend = (Button)findViewById(R.id.btn_addfriend_findfriend);
		lin_addfriend_back=(LinearLayout) findViewById(R.id.lin_addfriend_back);
		edt_addfriend_username=(EditText) findViewById(R.id.edt_addfriend_username);
		lin_addfriend_add=(LinearLayout) findViewById(R.id.lin_addfriend_add);
		friendService=new FriendService(this);
		searchLoadingDialog=new Dialog(this,R.style.FullHeightDialog);
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_addfriend_findfriend.setOnClickListener(new btnClickListener());	
		lin_addfriend_back.setOnClickListener(new btnClickListener());
	}
	
	private class btnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_addfriend_findfriend: //查找好友
				//int[] location = new  int[2] ;
				//Window window=searchLoadingDialog.getWindow();
				//lin_addfriend_add.getLocationOnScreen(location);
				//window.setGravity(location[0]);
				if(edt_addfriend_username.getText().toString().trim().equals("")){
					Toast.makeText(AddFriendActivity.this, "用户名不能为空", Toast.LENGTH_SHORT).show();
					return;
				}
				searchLoadingDialog.show();
				searchLoadingDialog.setTitle(null);
				searchLoadingDialog.setCancelable(false);
				searchLoadingDialog.setContentView(R.layout.activity_addfriend_loading_dialog_view);
				friendService.searchFriendService(edt_addfriend_username.getText().toString(), searchLoadingDialog);
				break;
			case R.id.lin_addfriend_back:
				finish();
				break;
			default:
				break;
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK){
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}

