package com.chending.ttsports.activity;

import java.util.HashMap;
import java.util.Map;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.model.UserLoginWidgetModel;
import com.chending.ttsports.service.ChangeInfoService;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AlterGenderActivity extends Activity{
	private LinearLayout layout;
	private Button btn_gender_man,btn_gender_woman,btn_gender_cancel;
	private ImageView iv_gender_man,iv_gender_woman;
	private SharedPreferencesTool preferencesTool;
	private String 	gender = "男";
	private boolean gender_flag = true; 
	private  Dialog sendInfoDialog;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alter_gerenxinxi_gender);
		init();
		addListener();
		if(Fragment_gerenxinxi.sex == true){//男
			iv_gender_man.setVisibility(0);
			iv_gender_woman.setVisibility(4);
		}
		else{//女
			iv_gender_man.setVisibility(4);
			iv_gender_woman.setVisibility(0);
		}
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "提示：点击窗口外部关闭窗口！", 
						Toast.LENGTH_SHORT).show();	
			}
		});
	}

	private void init() {
		layout = (LinearLayout)findViewById(R.id.ll_gender_select);
		btn_gender_man = (Button)findViewById(R.id.btn_gender_man); 
		btn_gender_woman = (Button)findViewById(R.id.btn_gender_woman); 
		btn_gender_cancel = (Button)findViewById(R.id.btn_gender_cancel); 
		iv_gender_man = (ImageView)findViewById(R.id.iv_gender_man);
		iv_gender_woman = (ImageView)findViewById(R.id.iv_gender_woman);
		preferencesTool = SharedPreferencesTool.getSharedPreferencesToolInstance(this);
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_gender_man.setOnClickListener(new BtnClickListener());
		btn_gender_woman.setOnClickListener(new BtnClickListener());
		btn_gender_cancel.setOnClickListener(new BtnClickListener());
	}
	
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_gender_man://选择男
				iv_gender_man.setVisibility(0);//0表示可见 4表示不可见
				iv_gender_woman.setVisibility(4);
				gender_flag = true;
				Fragment_gerenxinxi.sex = true;
				break;
			case R.id.btn_gender_woman://选择女
				iv_gender_man.setVisibility(4);
				iv_gender_woman.setVisibility(0);
				gender_flag = false;
				Fragment_gerenxinxi.sex = false;
				break;
			case R.id.btn_gender_cancel://取消
				finish();
				break;
			default:
				break;
			}
			if(gender_flag == true){
				gender = "男";
			}
			else{
				gender = "女";
			}
			/*
			 * 修改本地数据库
			 */
			Map<String, String> map=new HashMap<String, String>();
			map.put("gender",gender);
			preferencesTool.saveUserPrefences(map);
			/*
			 * 修改服务器数据
			 */
			sendInfoDialog = new Dialog(AlterGenderActivity.this);
			sendInfoDialog.show();
			sendInfoDialog.setCancelable(false);
			sendInfoDialog.setContentView(R.layout.activity_login_loading_dialog_view);
			User user  = new User(); 
			String avatar=preferencesTool.getUserPrefences().get("avatar");
        	String weight=preferencesTool.getUserPrefences().get("weight");
        	String phone=preferencesTool.getUserPrefences().get("phone");
        	String height=preferencesTool.getUserPrefences().get("height");
        	String hobby=preferencesTool.getUserPrefences().get("hobby");
        	String signature=preferencesTool.getUserPrefences().get("signature");
        	String regTime=preferencesTool.getUserPrefences().get("regTime");
        	String userId=preferencesTool.getUserPrefences().get("userId");
        	String password=preferencesTool.getUserPrefences().get("password");
        	String username=preferencesTool.getUserPrefences().get("username");
        	String latitude=preferencesTool.getUserPrefences().get("latitude");
        	String longitude=preferencesTool.getUserPrefences().get("longitude");
        	user.setLatitude(Double.parseDouble(latitude));
        	user.setLongitude(Double.parseDouble(longitude));
        	user.setUsername(username);
        	user.setUserId(Integer.valueOf(userId));
        	user.setAvatar(avatar);
        	user.setGender(gender);
        	user.setHeight(Double.valueOf(height));
        	user.setWeight(Double.valueOf(weight));
        	user.setHobby(hobby);
        	user.setPassword(password);
        	user.setPhone(phone);
        	user.setSignature(signature);
			new ChangeInfoService(AlterGenderActivity.this).changeInfoService(user,sendInfoDialog);
			finish();
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent event){
		finish();
		return true;
	}
	
	
}
