package com.chending.ttsports.activity;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.util.AsyncImageLoader;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
/*
 * 此Activity再搜索到好友之后，查看好友的资料，并选择是否添加
 */
import android.widget.TextView;
public class CheckDataActivity extends Activity{
	private TextView tv_checkdata_username,tv_checkdata_gender,tv_checkdata_hobby,tv_checkdata_signature;
	private LinearLayout lin_checkdata_addfriend;
	private RelativeLayout rl_checkdata_add;
	private Dialog addFriendloadingDialog;
	private FriendService friendService;
	private SharedPreferencesTool preferencesTool;
	private LinearLayout lin_checkdata_back;
	private Button btn_checkdata_addfriend;
	private TextView tv_checkdata_add;
	private ImageView iv_chackdata_head;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkdata);
		Constant.TAG_ENTER_CHACK_DATA=1;
		init();
		addListener();
	}
	/**
	 * 初始化组件
	 */
	public void init(){
		iv_chackdata_head=(ImageView) findViewById(R.id.iv_chackdata_head);
		tv_checkdata_username=(TextView) findViewById(R.id.tv_checkdata_username);
		tv_checkdata_gender=(TextView) findViewById(R.id.tv_checkdata_gender);
		tv_checkdata_hobby=(TextView) findViewById(R.id.tv_checkdata_hobby);
		tv_checkdata_signature=(TextView) findViewById(R.id.tv_checkdata_signature);
		tv_checkdata_username.setText(FriendService.getUser().getUsername());
		tv_checkdata_gender.setText(FriendService.getUser().getGender());
		tv_checkdata_hobby.setText(FriendService.getUser().getHobby());
		tv_checkdata_signature.setText(FriendService.getUser().getSignature());
		lin_checkdata_addfriend=(LinearLayout) findViewById(R.id.lin_checkdata_addfriend);
		addFriendloadingDialog=new Dialog(this,R.style.FullHeightDialog);
		friendService=new FriendService(this);
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(this);
		lin_checkdata_back=(LinearLayout) findViewById(R.id.lin_checkdata_back);
		rl_checkdata_add=(RelativeLayout) findViewById(R.id.rl_checkdata_add);
		btn_checkdata_addfriend=(Button) findViewById(R.id.btn_checkdata_addfriend);
		tv_checkdata_add=(TextView) findViewById(R.id.tv_checkdata_add);
		
		checkFriend();
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		lin_checkdata_addfriend.setOnClickListener(new BtnClickListener());
		lin_checkdata_back.setOnClickListener(new BtnClickListener());
	}
	
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.lin_checkdata_addfriend://添加为好友
				if(tv_checkdata_add.getText().toString().trim().equals("发送消息")){
					Intent intent=new Intent(CheckDataActivity.this,ChatActivity.class);
					Bundle bundle=new Bundle();
					bundle.putString("username", FriendService.getUser().getUsername());
					bundle.putInt("friendId", FriendService.getUser().getUserId());
					bundle.putString("avatarOp", FriendService.getUser().getAvatar());
					intent.putExtras(bundle);
					startActivity(intent);
					
					//finish();
					
				}else{
					addFriendloadingDialog.show();
					addFriendloadingDialog.setContentView(R.layout.activity_login_loading_dialog_view);
					addFriendloadingDialog.setCancelable(false);
					friendService.addFriendService(Integer.valueOf(preferencesTool.getUserPrefences().get("userId")),FriendService.getUser().getUserId(),addFriendloadingDialog);
				}
				break;	
			case R.id.lin_checkdata_back:
				finish();
				break;
			
			default:
				break;
			}
		}
	}
	private void checkFriend(){
		for(FriendsVO friendsVO:FriendService.friendsVOs){//查到的是自己的朋友
			if(friendsVO.getUsername().equals(FriendService.getUser().getUsername())){
				btn_checkdata_addfriend.setBackgroundResource(R.drawable.friend_send_message);
				tv_checkdata_add.setText("发送消息");
				break;
			}
		}
		if(preferencesTool.getUserPrefences().get("username").equals(FriendService.getUser().getUsername())){//查到的是自己
			rl_checkdata_add.setVisibility(View.GONE);
		}
		System.out.println("FriendService.getUser().getAvatar()="+FriendService.getUser().getAvatar());
		AsyncImageLoader.loadImage(FriendService.getUser().getAvatar(), iv_chackdata_head);
	}
}
