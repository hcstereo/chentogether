package com.chending.ttsports.activity;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChangeInfoService;
import com.chending.ttsports.service.ChatService;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.service.MsgService;
import com.chending.ttsports.util.AsyncImageLoader;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_gerenxinxi extends Fragment{
	public static final String TAG="Fragment_gerenxinxi";
	private SharedPreferencesTool preferencesTool;
	private TextView tv_gerenxinxi_username,tv_gerenxinxi_gender,tv_gerenxinxi_height,tv_gerenxinxi_weight,tv_gerenxinxi_hobby,tv_gerenxinxi_signature;
	private RelativeLayout rl_gerenxinxi_touxiang,rl_gerenxinxi_username,rl_gerenxinxi_gender,rl_gerenxinxi_height,rl_gerenxinxi_weight,rl_gerenxinxi_hobby,rl_gerenxinxi_signature;
	private TextView tv_gerenxinxi_touxiang_zhushi,tv_gerenxinxi_username_zhushi,tv_gerenxinxi_gender_zhushi,tv_gerenxinxi_height_zhushi,tv_gerenxinxi_weight_zhushi,tv_gerenxinxi_hobby_zhushi,tv_gerenxinxi_signature_zhushi;
	private Button btn_gerenxinxi_logout;
	private Builder exitDialog;
	private Button btn_gerenxinxi_photo_choosephoto,btn_gerenxinxi_photo_paizhao,btn_gerenxinxi_photo_cancel;
	private View layout;	
	private PopupWindow menuWindow;
	public static boolean sex = true;
	private LayoutInflater inflater;
	private ImageView iv_grxx_head;
	private User user;
	private  Dialog changeInfoDialog;
	private ImageView headImageView;
	public static boolean touxianig_flag = false;
     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.fragment_gerenxinxi, container, false);
    	init(view);
    	addListener();
    	btn_gerenxinxi_logout.setOnTouchListener(new OnTouchListener(){  	
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO 自动生成的方法存根
					//btn_gerenxinxi_logout.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_gerenxinxi_logout));
					if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
						btn_gerenxinxi_logout.setTextColor(Color.rgb(255, 48, 48));  
						btn_gerenxinxi_logout.setBackgroundResource(R.drawable.btn_gerenxinxi_logout_normal);
						
		            }   
		            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
		            	btn_gerenxinxi_logout.setTextColor(Color.rgb(255,255,255));  
						btn_gerenxinxi_logout.setBackgroundResource(R.drawable.btn_gerenxinxi_logout_press); 
			
		            } 
		            else{
		            	btn_gerenxinxi_logout.setTextColor(Color.rgb(255, 48, 48));  
						btn_gerenxinxi_logout.setBackgroundResource(R.drawable.btn_gerenxinxi_logout_normal);
		            }
					return false;
				}});
    	rl_gerenxinxi_touxiang.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_touxiang_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_touxiang.setBackgroundResource(R.color.white);
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_touxiang_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_touxiang.setBackgroundResource(R.color.blue); 
		
	            } 
	            else{
	            	tv_gerenxinxi_touxiang_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_touxiang.setBackgroundResource(R.color.white);
	            }
				return false;
			}});
    	rl_gerenxinxi_username.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_username_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_username.setBackgroundResource(R.color.white);
					tv_gerenxinxi_username.setTextColor(Color.rgb(129, 129, 129));  
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_username_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_username.setBackgroundResource(R.color.blue); 
	            	tv_gerenxinxi_username.setTextColor(Color.rgb(255, 255, 255));  
	            } 
	            else{
	            	tv_gerenxinxi_username_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_username.setBackgroundResource(R.color.white);
	            	tv_gerenxinxi_username.setTextColor(Color.rgb(129, 129, 129));  
	            }
				return false;
			}});
    	rl_gerenxinxi_gender.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_gender_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_gender.setBackgroundResource(R.color.white);
					tv_gerenxinxi_gender.setTextColor(Color.rgb(129, 129, 129));  
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_gender_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_gender.setBackgroundResource(R.color.blue); 
	            	tv_gerenxinxi_gender.setTextColor(Color.rgb(255, 255, 255));  
	            } 
	            else{
	            	tv_gerenxinxi_gender_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_gender.setBackgroundResource(R.color.white);
	            	tv_gerenxinxi_gender.setTextColor(Color.rgb(129, 129, 129));  
	            }
				return false;
			}});
    	rl_gerenxinxi_height.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_height_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_height.setBackgroundResource(R.color.white);
					tv_gerenxinxi_height.setTextColor(Color.rgb(129, 129, 129));  
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_height_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_height.setBackgroundResource(R.color.blue); 
	            	tv_gerenxinxi_height.setTextColor(Color.rgb(255, 255, 255));  
	            } 
	            else{
	            	tv_gerenxinxi_height_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_height.setBackgroundResource(R.color.white);
	            	tv_gerenxinxi_height.setTextColor(Color.rgb(129, 129, 129));  
	            }
				return false;
			}});
    	rl_gerenxinxi_weight.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_weight_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_weight.setBackgroundResource(R.color.white);
					tv_gerenxinxi_weight.setTextColor(Color.rgb(129, 129, 129));  
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_weight_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_weight.setBackgroundResource(R.color.blue); 
	            	tv_gerenxinxi_weight.setTextColor(Color.rgb(255, 255, 255));  
	            } 
	            else{
	            	tv_gerenxinxi_weight_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_weight.setBackgroundResource(R.color.white);
	            	tv_gerenxinxi_weight.setTextColor(Color.rgb(129, 129, 129));  
	            }
				return false;
			}});
    	rl_gerenxinxi_hobby.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_hobby_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_hobby.setBackgroundResource(R.color.white);
					tv_gerenxinxi_hobby.setTextColor(Color.rgb(129, 129, 129));  
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_hobby_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_hobby.setBackgroundResource(R.color.blue); 
	            	tv_gerenxinxi_hobby.setTextColor(Color.rgb(255, 255, 255));  
	            } 
	            else{
	            	tv_gerenxinxi_hobby_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_hobby.setBackgroundResource(R.color.white);
	            	tv_gerenxinxi_hobby.setTextColor(Color.rgb(129, 129, 129));  
	            }
				return false;
			}});
    	rl_gerenxinxi_signature.setOnTouchListener(new OnTouchListener(){  	
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_UP){//抬手  
					tv_gerenxinxi_signature_zhushi.setTextColor(Color.rgb(8, 8, 8));  
					rl_gerenxinxi_signature.setBackgroundResource(R.color.white);
					tv_gerenxinxi_signature.setTextColor(Color.rgb(129, 129, 129));  
					
	            }   
	            if(event.getAction() == MotionEvent.ACTION_DOWN){//触摸  
	            	tv_gerenxinxi_signature_zhushi.setTextColor(Color.rgb(255,255,255));  
	            	rl_gerenxinxi_signature.setBackgroundResource(R.color.blue); 
	            	tv_gerenxinxi_signature.setTextColor(Color.rgb(255, 255, 255));  
	            } 
	            else{
	            	tv_gerenxinxi_signature_zhushi.setTextColor(Color.rgb(8, 8, 8));  
	            	rl_gerenxinxi_signature.setBackgroundResource(R.color.white);
	            	tv_gerenxinxi_signature.setTextColor(Color.rgb(129, 129, 129));  
	            }
				return false;
			}});
    	return view;
    }
     @SuppressLint("HandlerLeak")
	private void init(View view){
    	 headImageView=(ImageView) view.findViewById(R.id.headImageView);
    	 btn_gerenxinxi_logout=(Button) view.findViewById(R.id.btn_gerenxinxi_logout);
    	 preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(getActivity());
    	 tv_gerenxinxi_username=(TextView) view.findViewById(R.id.tv_gerenxinxi_username);
    	 tv_gerenxinxi_gender=(TextView) view.findViewById(R.id.tv_gerenxinxi_gender);
    	 tv_gerenxinxi_height=(TextView) view.findViewById(R.id.tv_gerenxinxi_height);
    	 tv_gerenxinxi_weight=(TextView) view.findViewById(R.id.tv_gerenxinxi_weight);
    	 tv_gerenxinxi_hobby=(TextView) view.findViewById(R.id.tv_gerenxinxi_hobby);
    	 tv_gerenxinxi_signature=(TextView) view.findViewById(R.id.tv_gerenxinxi_signature);
    	 rl_gerenxinxi_touxiang=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_touxiang);
    	 rl_gerenxinxi_username=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_username);
    	 rl_gerenxinxi_gender=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_gender);
    	 rl_gerenxinxi_height=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_height);
    	 rl_gerenxinxi_weight=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_weight);
    	 rl_gerenxinxi_hobby=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_hobby);
    	 rl_gerenxinxi_signature=(RelativeLayout) view.findViewById(R.id.rl_gerenxinxi_signature);
    	 tv_gerenxinxi_touxiang_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_touxiang_zhushi);
    	 tv_gerenxinxi_username_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_username_zhushi);
    	 tv_gerenxinxi_gender_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_gender_zhushi);
    	 tv_gerenxinxi_height_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_height_zhushi);
    	 tv_gerenxinxi_weight_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_weight_zhushi);
    	 tv_gerenxinxi_hobby_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_hobby_zhushi);
    	 tv_gerenxinxi_signature_zhushi=(TextView) view.findViewById(R.id.tv_gerenxinxi_signature_zhushi);
    	 iv_grxx_head=(ImageView) view.findViewById(R.id.iv_grxx_head);//头像
    	 user=preferencesTool.toUserFromSharePreferences(preferencesTool.getUserPrefences());
    	 AsyncImageLoader.loadImage(user.getAvatar(), iv_grxx_head);
    	 String username=user.getUsername();
    	 if(username!=null){
    		 //String avatar=preferencesTool.getUserPrefences().get("avatar");
        	 String height=preferencesTool.getUserPrefences().get("height");
        	 String weight=preferencesTool.getUserPrefences().get("weight");
        	 String gender=preferencesTool.getUserPrefences().get("gender");
        	 //String phone=preferencesTool.getUserPrefences().get("phone");
        	 String hobby=preferencesTool.getUserPrefences().get("hobby");
        	 String signature=preferencesTool.getUserPrefences().get("signature");
        	 tv_gerenxinxi_username.setText(username);
        	 tv_gerenxinxi_gender.setText(gender);
        	 tv_gerenxinxi_height.setText(height);
        	 tv_gerenxinxi_weight.setText(weight);
        	 tv_gerenxinxi_hobby.setText(hobby);
        	 tv_gerenxinxi_signature.setText(signature);
        	 if(gender.equals("男")){
        		 Fragment_gerenxinxi.sex = true;
        	 }
        	 else{
        		 Fragment_gerenxinxi.sex = false;
        	 }
    	 }
    	 exitDialog=new AlertDialog.Builder(getActivity()).setTitle("提示").setIcon(null).setCancelable(false).setMessage("您确定退出吗？")
    			 .setPositiveButton("确定",  new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Map<String, String> params=new HashMap<String, String>();
						preferencesTool.saveUserPrefences(params);
						preferencesTool.clearUserPreferences();//清除用户信息
						Constant.IS_THREAD_SHUOULD_END=true;
						getActivity().stopService(new Intent(getActivity(),MsgService.class));
						Constant.IS_THREAD_START=false;
						FriendService.friendsVOs.clear();
						FriendService.friendRelVOs.clear();
						FriendService.setUser(null);
						ChatService.chatMsgVOs.clear();
						ChatService.friendMsgs.clear();
						Constant.IMG_PATH_AFTER_CROP="";
						Constant.USER_HAS_LOGIN=false;
						Intent intent=new Intent(getActivity(),LoginActivity.class);
						//getActivity().unbindService(MainActivity.connection);
						
						startActivity(intent);
						getActivity().finish();
					}
				})
				.setNegativeButton("取消", new DialogInterface.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
     }
     private void addListener(){
    	 btn_gerenxinxi_logout.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_touxiang.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_username.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_gender.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_height.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_weight.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_hobby.setOnClickListener(new BtnClickListener());
    	 rl_gerenxinxi_signature.setOnClickListener(new BtnClickListener());
     }
     private class BtnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_gerenxinxi_logout:
				AlertDialog alertDialog=exitDialog.create();
				alertDialog.show();
				break;
			case R.id.rl_gerenxinxi_touxiang://头像点击事件
				inflater = (LayoutInflater)getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
	 	    	layout = inflater.inflate(R.layout.fragment_alter_gerenxinxi_photo, null);
	 	    	menuWindow = new PopupWindow(layout,LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT); //后两个参数是width和height
	 	    	menuWindow.showAsDropDown(layout, 1, 1); //设置弹出效果
				menuWindow.setFocusable(true);
				menuWindow.setOutsideTouchable(true);
				menuWindow.update();
				menuWindow.setBackgroundDrawable(new BitmapDrawable());
	 	    	menuWindow.showAtLocation(getActivity().findViewById(R.id.fragment_gerenxinxi), Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0); //设置layout在PopupWindow中显示的位置
	 	    	btn_gerenxinxi_photo_choosephoto = (Button)layout.findViewById(R.id.btn_gerenxinxi_photo_choosephoto);
	 	    	btn_gerenxinxi_photo_paizhao = (Button)layout.findViewById(R.id.btn_gerenxinxi_photo_paizhao);	
	 	    	btn_gerenxinxi_photo_cancel = (Button)layout.findViewById(R.id.btn_gerenxinxi_photo_cancel);
	 	    	/*
				 * 选择照片
				 */
	 	    	btn_gerenxinxi_photo_choosephoto.setOnClickListener (new OnClickListener() {					
					@Override
					public void onClick(View arg0) {
						final String IMAGE_TYPE = "image/*";
						Intent getAlbum = new Intent(Intent.ACTION_GET_CONTENT);
						getAlbum.setType(IMAGE_TYPE);
						menuWindow.dismiss();
						startActivityForResult(getAlbum, Constant.REQUESTCODE_PHOTO);
						}
					});	
					
				/*
				 * 拍照上传
				 */
	 	    	btn_gerenxinxi_photo_paizhao.setOnClickListener (new OnClickListener() {					
					@SuppressLint("SdCardPath")
					@Override
					public void onClick(View arg0) {
						Intent intent = null;
						intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						File dir = new File("/sdcard/ttsports/Camera/");
						if (!dir.exists()) {
							dir.mkdirs();
						}
						Constant.mUploadPhotoPath = "/sdcard/ttsports/Camera/"
								+ UUID.randomUUID().toString();
						File file = new File(
								Constant.mUploadPhotoPath);
						if (!file.exists()) {
							try {
								file.createNewFile();
							} catch (IOException e) {

							}
						}
						menuWindow.dismiss();
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(file));
						startActivityForResult(intent,
										Constant.REQUESTCODE_UPLOADPHOTO_CAMERA);
						}
					});	
				/*
				 *取消
				 */
	 	    	btn_gerenxinxi_photo_cancel.setOnClickListener (new OnClickListener() {					
					@Override
					public void onClick(View arg0) {
						menuWindow.dismiss();
						}
					});	
				break;
			case R.id.rl_gerenxinxi_username:
				Intent intent = new Intent(getActivity(),AlterUsernameActicity.class);
				startActivityForResult(intent, Constant.REQUESTCODE_GRXX_USERNAMAE);
				break;
			case R.id.rl_gerenxinxi_gender:
				Intent intent1 = new Intent (getActivity(),AlterGenderActivity.class);			
				startActivityForResult(intent1, Constant.REQUESTCODE__GRXX_GENDER);
				break;
			case R.id.rl_gerenxinxi_height:
				Intent intent2 = new Intent(getActivity(),AlterHeightActivity.class);
				startActivityForResult(intent2, Constant.REQUESTCODE_GRXX_HEIGHT );
				break;
			case R.id.rl_gerenxinxi_weight:
				Intent intent3 = new Intent(getActivity(),AlterWeightActivity.class);
				startActivityForResult(intent3, Constant.REQUESTCODE_CROP_WEIGHT);
				break;
			case R.id.rl_gerenxinxi_hobby:
				Intent intent4 = new Intent(getActivity(),AlterHobbyActivity.class);
				startActivityForResult(intent4, Constant.REQUESTCODE_CROP_HOBBY);
				break;
			case R.id.rl_gerenxinxi_signature:
				Intent intent5 = new Intent(getActivity(),AlterSignatureActivity.class);
				startActivityForResult(intent5, Constant.REQUESTCODE_CROP_SIGNATURE);
				break;
			default:
				break;
			}
		}
    	 
     }
     public void onActivityResult(int requestCode, int resultCode, Intent data) {
 		super.onActivityResult(requestCode, resultCode, data);
 			switch (requestCode) {
 			/*
 			 * 用户名
 			 */
 			case Constant.REQUESTCODE_GRXX_USERNAMAE:
 				String username=preferencesTool.getUserPrefences().get("username");
 				tv_gerenxinxi_username.setText(username);
 				break;
 			/*
 			 * 性别
 			 */
 			case Constant.REQUESTCODE__GRXX_GENDER:
 				String gender=preferencesTool.getUserPrefences().get("gender");
 				tv_gerenxinxi_gender.setText(gender);
 				break;
 			/*
 			 *身高 
 			 */
 			case Constant.REQUESTCODE_GRXX_HEIGHT:
 				String height=preferencesTool.getUserPrefences().get("height");
 				tv_gerenxinxi_height.setText(height);
 				break;
 			/*
 			 * 体重
 			 */
 			case Constant.REQUESTCODE_CROP_WEIGHT:
 				String weight=preferencesTool.getUserPrefences().get("weight");
 				tv_gerenxinxi_weight.setText(weight);
 				break;
 			/*
 			 * 兴趣
 			 */
 			case Constant.REQUESTCODE_CROP_HOBBY:
 				String hobby=preferencesTool.getUserPrefences().get("hobby");
 				tv_gerenxinxi_hobby.setText(hobby);
 				break;
 			/*
 			 * 个性签名
 			 */
 			case Constant.REQUESTCODE_CROP_SIGNATURE:
 				String signature=preferencesTool.getUserPrefences().get("signature");
 				tv_gerenxinxi_signature.setText(signature);
 				break;
			case Constant.REQUESTCODE_PHOTO:
				/*
	 			 * 相册返回事件
	 			 */
				String[] proj = {MediaStore.Images.Media.DATA};
				if(data == null){
				}
				else{
					Uri originalUri = data.getData();        //获得图片的uri 
					//好像是android多媒体数据库的封装接口，具体的看Android文档
					@SuppressWarnings("deprecation")
					Cursor cursor = getActivity().managedQuery(originalUri, proj, null, null, null); 
					//按我个人理解 这个是获得用户选择的图片的索引值
					int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					//将光标移至开头 ，这个很重要，不小心很容易引起越界
					cursor.moveToFirst();
					//最后根据索引值获取图片路径
					Constant.mUploadPhotoPath = cursor.getString(column_index);
					Bundle mBundle = new Bundle();
					mBundle.putString("path",Constant.mUploadPhotoPath);  
					Intent intent = new Intent(getActivity(),ImageFilterCropActivity.class);
					intent.putExtras(mBundle); 
					startActivityForResult(intent,Constant.REQUESTCODE_CROP_PHOTO);
				}
				break;
			case Constant.REQUESTCODE_UPLOADPHOTO_CAMERA:
				Bundle mBundle1 = new Bundle();
 				mBundle1.putString("path",Constant.mUploadPhotoPath);  
 				Intent intent1 = new Intent(getActivity(),ImageFilterCropActivity.class);
 				intent1.putExtras(mBundle1); 
 				startActivityForResult(intent1,Constant.REQUESTCODE_CROP_PHOTO);
				break;
			case Constant.REQUESTCODE_CROP_PHOTO:
				/*
	 			 * 裁剪完返回事件
	 			 */
	 				if(resultCode == getActivity().RESULT_OK){
	 					String myJpgPath = data.getStringExtra("path");
	 					Constant.IMG_PATH_AFTER_CROP=myJpgPath;
	            		 	Bitmap bm = BitmapFactory.decodeFile(myJpgPath);
	            		 	Constant.mUploadPhotoPath = myJpgPath;
	            		 	touxianig_flag = true;
	            		 	
	            		 	/*
	            			 * 修改本地数据库
	            			 */
	            			Map<String, String> map=new HashMap<String, String>();
	            			map.put("avatar","/file/1/avastar/"+Constant.IMG_NAME);
	            			preferencesTool.saveUserPrefences(map);
	            			/*
	            			 * 修改服务器数据
	            			 */
	            			changeInfoDialog = new Dialog(getActivity());
	            			changeInfoDialog.show();
	            			changeInfoDialog.setCancelable(false);
	            			changeInfoDialog.setContentView(R.layout.activity_login_loading_dialog_view);
	            			User user  = new User(); 
	                    	String weight1=preferencesTool.getUserPrefences().get("weight");
	                    	String gender1=preferencesTool.getUserPrefences().get("gender");
	                    	String phone1=preferencesTool.getUserPrefences().get("phone");
	                    	String height1=preferencesTool.getUserPrefences().get("height");
	                    	String hobby1=preferencesTool.getUserPrefences().get("hobby");
	                    	String signature1=preferencesTool.getUserPrefences().get("signature");
	                    	String regTime1=preferencesTool.getUserPrefences().get("regTime");
	                    	String userId1=preferencesTool.getUserPrefences().get("userId");
	                    	String password1=preferencesTool.getUserPrefences().get("password");
	                    	String username1=preferencesTool.getUserPrefences().get("username");
	                    	String latitude1=preferencesTool.getUserPrefences().get("latitude");
	                    	String longitude1=preferencesTool.getUserPrefences().get("longitude");
	                    	user.setLatitude(Double.parseDouble(latitude1));
	                    	user.setLongitude(Double.parseDouble(longitude1));
	                    	user.setUsername(username1);
	                    	user.setUserId(Integer.valueOf(userId1));
	                    	user.setAvatar("/file/1/avastar/"+Constant.IMG_NAME);
	                    	user.setGender(gender1);
	                    	user.setHeight(Double.valueOf(height1));
	                    	user.setWeight(Double.valueOf(weight1));
	                    	user.setHobby(hobby1);
	                    	user.setPassword(password1);
	                    	user.setPhone(phone1);
	                    	user.setSignature(signature1);
	                    	new ChangeInfoService(getActivity()).changeInfoService(user,changeInfoDialog);
	                    	/*
	                    	 * 修改本地页面的头像
	                    	 */
	            		 	iv_grxx_head.setImageBitmap(bm); 
	 				}
	 				else if(resultCode == getActivity().RESULT_CANCELED){
	 					Toast.makeText(getActivity(), "选择图片失败", Toast.LENGTH_SHORT).show();
	 				}
			default:
				break;
			}
 			
 	}
}  