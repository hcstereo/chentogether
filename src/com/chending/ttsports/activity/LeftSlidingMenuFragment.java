package com.chending.ttsports.activity;

import java.util.Map;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaowen.yixin.R;
import com.chending.ttsports.activity.MainActivity;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChatService;
import com.chending.ttsports.util.AsyncImageLoader;

//采用Fragment来实现左边的菜单

public class LeftSlidingMenuFragment extends Fragment implements
		OnClickListener {
	private TextView nickNameTextView;// 用户名
	private TextView tv_slidingmenu_gexingqianming;
	private ImageView headImageView;
	private View sportBtnLayout; // 左侧菜单的运动功能
	private View friendBtnLayout; // 左侧菜单的朋友功能
	private View yuepaoBtnLayout; // 左侧菜单的约跑功能
	private View gerenxinxiBtnLayout; // 左侧菜单的个人信息功能
	private View aboutBtnLayout; // 左侧菜单的关于app功能
	private View chatMsgBtnLayout; // 消息
	private View historyBtnLayout; // 历史
	private SharedPreferencesTool preferencesTool;
	private User user;
	public static ImageView iv_has_new;
	private MyTask myTask;
	// 获取标志位 判断用户是否登陆
	String flag = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_left_fragment, container,
				false);
		init(view);
		addListener();
		return view;
	}

	private void init(View view) {
		myTask = new MyTask();
		tv_slidingmenu_gexingqianming = (TextView) view
				.findViewById(R.id.tv_slidingmenu_gexingqianming);
		iv_has_new = (ImageView) view.findViewById(R.id.iv_has_new);
		headImageView = (ImageView) view.findViewById(R.id.headImageView);
		nickNameTextView = (TextView) view.findViewById(R.id.nickNameTextView);
		friendBtnLayout = view.findViewById(R.id.friendBtnLayout);
		yuepaoBtnLayout = view.findViewById(R.id.yuepaoBtnLayout);
		gerenxinxiBtnLayout = view.findViewById(R.id.gerenxinxiBtnLayout);
		aboutBtnLayout = view.findViewById(R.id.aboutBtnLayout);
		sportBtnLayout = view.findViewById(R.id.sportBtnLayout);
		preferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(getActivity());
		user = preferencesTool.toUserFromSharePreferences(preferencesTool
				.getUserPrefences());
		chatMsgBtnLayout = view.findViewById(R.id.chatMsgBtnLayout);
		historyBtnLayout = view.findViewById(R.id.historyBtnLayout);
		Map<String, String> params = preferencesTool.getUserPrefences();
		// 判断是否登陆过
		if (params.get("username") != null) {
			nickNameTextView.setText(params.get("username"));// 设置用户名
			tv_slidingmenu_gexingqianming.setText(params.get("signature"));// 设置个性签名
			AsyncImageLoader.loadImage(user.getAvatar(), headImageView);
		}
		myTask.execute("param");
		chatMsgBtnLayout.setSelected(false);
		sportBtnLayout.setSelected(true);
		friendBtnLayout.setSelected(false);
		yuepaoBtnLayout.setSelected(false);
		gerenxinxiBtnLayout.setSelected(false);
		aboutBtnLayout.setSelected(false);
		historyBtnLayout.setSelected(false);
	}

	/*
	 * @Override public void onResume() { super.onResume();
	 * AsyncImageLoader.loadImage
	 * (preferencesTool.getUserPrefences().get("Avatar"), headImageView); }
	 */
	private void addListener() {
		headImageView.setOnClickListener(this);
		nickNameTextView.setOnClickListener(this);
		sportBtnLayout.setOnClickListener(this);
		friendBtnLayout.setOnClickListener(this);
		yuepaoBtnLayout.setOnClickListener(this);
		gerenxinxiBtnLayout.setOnClickListener(this);
		aboutBtnLayout.setOnClickListener(this);
		chatMsgBtnLayout.setOnClickListener(this);
		historyBtnLayout.setOnClickListener(this);
	}

	// 将三个按钮的点击事件一块重写
	@Override
	public void onClick(View v) {
		Fragment newContent = null;
		int i = 0;
		String TAGMAIN = "";
		String TAGBAR = "";
		switch (v.getId()) {

		case R.id.sportBtnLayout: // 运动的点击事件 并设置为选中状态
			i = 0;
			TAGMAIN = Fragment_sport.TAG;
			TAGBAR = FragmentTitleBarMain.TAG;
			newContent = new Fragment_sport();
			chatMsgBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(true);
			friendBtnLayout.setSelected(false);
			yuepaoBtnLayout.setSelected(false);
			gerenxinxiBtnLayout.setSelected(false);
			aboutBtnLayout.setSelected(false);
			historyBtnLayout.setSelected(false);
			break;

		case R.id.friendBtnLayout: // 朋友的点击事件
			i = 1;
			TAGMAIN = Fragment_friend.TAG;
			TAGBAR = FragmentTitleBarFriend.TAG;
			newContent = new Fragment_friend();
			chatMsgBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(false);
			friendBtnLayout.setSelected(true);
			yuepaoBtnLayout.setSelected(false);
			gerenxinxiBtnLayout.setSelected(false);
			aboutBtnLayout.setSelected(false);
			historyBtnLayout.setSelected(false);
			break;
		case R.id.yuepaoBtnLayout: // 设置的点击事件
			i = 3;
			TAGMAIN = Fragment_yuepao.TAG;
			TAGBAR = FragmentTitleBarYuepao.TAG;
			newContent = new Fragment_yuepao();
			chatMsgBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(false);
			friendBtnLayout.setSelected(false);
			yuepaoBtnLayout.setSelected(true);
			gerenxinxiBtnLayout.setSelected(false);
			aboutBtnLayout.setSelected(false);
			historyBtnLayout.setSelected(false);
			break;
		case R.id.gerenxinxiBtnLayout:
			i = 5;
			TAGMAIN = Fragment_gerenxinxi.TAG;
			TAGBAR = FragmentTitleBarGerenxinxi.TAG;
			newContent = new Fragment_gerenxinxi();
			chatMsgBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(false);
			friendBtnLayout.setSelected(false);
			yuepaoBtnLayout.setSelected(false);
			gerenxinxiBtnLayout.setSelected(true);
			aboutBtnLayout.setSelected(false);
			historyBtnLayout.setSelected(false);
			break;
		case R.id.aboutBtnLayout: // 关于的点击事件
			i = 6;
			TAGMAIN = Fragment_about.TAG;
			TAGBAR = FragmentTitleBarAbout.TAG;
			newContent = new Fragment_about();
			chatMsgBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(false);
			friendBtnLayout.setSelected(false);
			yuepaoBtnLayout.setSelected(false);
			gerenxinxiBtnLayout.setSelected(false);
			aboutBtnLayout.setSelected(true);
			historyBtnLayout.setSelected(false);
			break;
		case R.id.chatMsgBtnLayout:
			i = 7;
			TAGMAIN = Fragment_ChatMsg.TAG;
			TAGBAR = FragmentTitleBarMsg.TAG;
			newContent = new Fragment_ChatMsg();
			chatMsgBtnLayout.setSelected(true);
			sportBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(false);
			friendBtnLayout.setSelected(false);
			yuepaoBtnLayout.setSelected(false);
			gerenxinxiBtnLayout.setSelected(false);
			aboutBtnLayout.setSelected(false);
			historyBtnLayout.setSelected(false);
			break;
		case R.id.historyBtnLayout: // 设置的点击事件
			i = 8;
			TAGMAIN = FragmentHistory.TAG;
			TAGBAR = FragmentTitleBarHistory.TAG;
			newContent = new FragmentHistory();
			chatMsgBtnLayout.setSelected(false);
			sportBtnLayout.setSelected(false);
			friendBtnLayout.setSelected(false);
			yuepaoBtnLayout.setSelected(false);
			gerenxinxiBtnLayout.setSelected(false);
			aboutBtnLayout.setSelected(false);
			historyBtnLayout.setSelected(true);
			break;
		case R.id.nickNameTextView:

			break;
		case R.id.headImageView:// 头像的点击事件
			i = 9;
			TAGMAIN = Fragment_gerenxinxi.TAG;
			TAGBAR = FragmentTitleBarGerenxinxi.TAG;
			newContent = new Fragment_gerenxinxi();
		default:
			break;
		}
		if (newContent != null)
			switchFragment(newContent, i, TAGMAIN, TAGBAR);
	}

	/*
	 * 切换到不同的功能内容
	 */
	private void switchFragment(Fragment fragment, int i, String tagMain,
			String tagBar) {
		if (getActivity() == null)
			return;
		MainActivity ra = (MainActivity) getActivity();
		ra.switchContent(fragment, i, tagMain, tagBar);
	}

	private class MyTask extends AsyncTask<Object, Object, Object> {
		@Override
		protected Object doInBackground(Object... params) {
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (ChatService.chatMsgVOs.size() > 0) {
					publishProgress("1");
				} else {
					publishProgress("-1");
				}
			}
		}

		@Override
		protected void onProgressUpdate(Object... values) {
			super.onProgressUpdate(values);
			int code = Integer.valueOf((String) values[0]);
			switch (code) {
			case 1:
				if (LeftSlidingMenuFragment.iv_has_new != null) {
					LeftSlidingMenuFragment.iv_has_new
							.setVisibility(View.VISIBLE);
				}
				if (FragmentTitleBarMain.iv_has_new_main != null) {
					FragmentTitleBarMain.iv_has_new_main
							.setVisibility(View.VISIBLE);
				}
				break;
			case -1:
				if (LeftSlidingMenuFragment.iv_has_new != null) {
					LeftSlidingMenuFragment.iv_has_new.setVisibility(View.GONE);
				}
				if (FragmentTitleBarMain.iv_has_new_main != null) {
					FragmentTitleBarMain.iv_has_new_main
							.setVisibility(View.GONE);
				}
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("Left OnResume()");
	}
	
}