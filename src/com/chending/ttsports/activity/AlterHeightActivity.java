package com.chending.ttsports.activity;

import java.util.HashMap;
import java.util.Map;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChangeInfoService;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AlterHeightActivity extends Activity{
	Button btn_end_alter_height;
	TextView tv_height_return_gerenxinxi;
	EditText edt_alter_gerenxinxi_height;
	private  Dialog sendInfoDialog;
	private SharedPreferencesTool preferencesTool;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alter_gerenxinxi_height);
		init();
		addListener();
		String height=preferencesTool.getUserPrefences().get("height");
		edt_alter_gerenxinxi_height.setHint(height);
	}
	private void init() {
		btn_end_alter_height = (Button)findViewById(R.id.btn_end_alter_height);
		tv_height_return_gerenxinxi = (TextView)findViewById(R.id.tv_height_return_gerenxinxi);
		edt_alter_gerenxinxi_height = (EditText)findViewById(R.id.edt_alter_gerenxinxi_height);
		preferencesTool = SharedPreferencesTool.getSharedPreferencesToolInstance(this);
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_end_alter_height.setOnClickListener(new BtnClickListener());
		tv_height_return_gerenxinxi.setOnClickListener(new BtnClickListener());
	}
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_end_alter_height:
				/*
				 * 修改本地数据库
				 */
				double temp_height1 = Double.valueOf(edt_alter_gerenxinxi_height.getText().toString());
				String temp_height = String.valueOf(temp_height1);
				if (temp_height1<60 ||temp_height1>250){
					Toast.makeText(AlterHeightActivity.this, "亲，您输入的体重有问题，请重新输入", Toast.LENGTH_SHORT).show();
				}
				else{
					Map<String, String> map=new HashMap<String, String>();
					map.put("height", temp_height);
					preferencesTool.saveUserPrefences(map);
					/*
					 * 修改服务器数据库
					 */
					sendInfoDialog = new Dialog(AlterHeightActivity.this);
					sendInfoDialog.show();
					sendInfoDialog.setCancelable(false);
					sendInfoDialog.setContentView(R.layout.activity_login_loading_dialog_view);
					User user  = new User(); 
					String avatar=preferencesTool.getUserPrefences().get("avatar");
					String weight=preferencesTool.getUserPrefences().get("weight");
					String gender=preferencesTool.getUserPrefences().get("gender");
					String phone=preferencesTool.getUserPrefences().get("phone");
					String hobby=preferencesTool.getUserPrefences().get("hobby");
					String signature=preferencesTool.getUserPrefences().get("signature");
					String regTime=preferencesTool.getUserPrefences().get("regTime");
					String userId=preferencesTool.getUserPrefences().get("userId");
					String password=preferencesTool.getUserPrefences().get("password");
					String username=preferencesTool.getUserPrefences().get("username");
					String latitude=preferencesTool.getUserPrefences().get("latitude");
					String longitude=preferencesTool.getUserPrefences().get("longitude");
					user.setLatitude(Double.parseDouble(latitude));
					user.setLongitude(Double.parseDouble(longitude));
					user.setUsername(username);
					user.setUserId(Integer.valueOf(userId));
					user.setAvatar(avatar);
					user.setGender(gender);
	        		user.setHeight(Double.valueOf(temp_height));
	        		user.setWeight(Double.valueOf(weight));
	        		user.setHobby(hobby);
	        		user.setPassword(password);
	        		user.setPhone(phone);
	        		user.setSignature(signature);
	        		new ChangeInfoService(AlterHeightActivity.this).changeInfoService(user,sendInfoDialog);
	        		finish();
				}
				break;
			case R.id.tv_height_return_gerenxinxi://返回个人信息
				finish();
				break;
			default:
				break;
			}
		}
	}
}
