package com.chending.ttsports.activity;

import com.chaowen.yixin.R;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

public class FragmentTitleBarMain extends Fragment implements OnClickListener{
	public static final String TAG="FragmentTitleBarMain";
	private LayoutInflater inflater;
	private PopupWindow menuWindow;
	private View layout;
	private ImageButton ib_sport_choose; //标题栏左边的 imageButton
	public static ImageView iv_has_new_main;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.title_bar_main, container,
				false);
		ImageButton imageButton=(ImageButton) view.findViewById(R.id.ivTitleBtnLeft);
		iv_has_new_main=(ImageView) view.findViewById(R.id.iv_has_new_main);
		ib_sport_choose = (ImageButton)view.findViewById(R.id.ib_sport_choose);
		imageButton.setOnClickListener(this);
		ib_sport_choose.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivTitleBtnLeft:
			System.out.println("sdedefohelsekafnkj");
			//点击标题左边按钮弹出左侧菜单
			MainActivity ra = (MainActivity) getActivity();
			ra.mSlidingMenu.showMenu(true);
			break;
			
		case R.id.ib_sport_choose:
			Intent intent = new Intent (getActivity(),MainTopDialog.class);			
			startActivity(intent);
			
		default:
			break;
		}
		
	}
	

}
