package com.chending.ttsports.activity;

import com.chaowen.yixin.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class FragmentTitleBarFriend extends Fragment implements OnClickListener{
	public static final String TAG="FragmentTitleBarFriend";
	public static ProgressBar pbar_friend_refresh;
	private ImageView iv_friends_add_friend;
	private ImageButton  imageButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.title_bar_friend, container,
				false);
		init(view);
		addListener();
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivTitleBtnLeft:
			//点击标题左边按钮弹出左侧菜单
			MainActivity ra = (MainActivity) getActivity();
			ra.mSlidingMenu.showMenu(true);
			break;
		case R.id.iv_friends_add_friend:
			Intent intent=new Intent(getActivity(),AddFriendActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
		
	}
	private void init(View view){
		pbar_friend_refresh=(ProgressBar) view.findViewById(R.id.pbar_friend_refresh);
		imageButton=(ImageButton) view.findViewById(R.id.ivTitleBtnLeft);
		iv_friends_add_friend=(ImageView) view.findViewById(R.id.iv_friends_add_friend);
	}
	private void addListener(){
		imageButton.setOnClickListener(this);
		iv_friends_add_friend.setOnClickListener(this);
	}
}
