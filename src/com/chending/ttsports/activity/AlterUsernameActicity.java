package com.chending.ttsports.activity;

import java.util.HashMap;
import java.util.Map;

import com.chaowen.yixin.R;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.model.User;
import com.chending.ttsports.model.UserLoginWidgetModel;
import com.chending.ttsports.service.ChangeInfoService;
import com.chending.ttsports.service.SportService;

import android.R.string;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AlterUsernameActicity extends Activity{
	Button btn_end_alter_username;
	TextView tv_username_return_gerenxinxi;
	EditText edt_alter_gerenxinxi_username;
	private  Dialog sendInfoDialog;
	private SharedPreferencesTool preferencesTool;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alter_gerenxinxi_username);
		init();
		addListener();
		String username=preferencesTool.getUserPrefences().get("username");
		edt_alter_gerenxinxi_username.setHint(username);
	}
	private void init() {
		btn_end_alter_username = (Button)findViewById(R.id.btn_end_alter_username);
		tv_username_return_gerenxinxi = (TextView)findViewById(R.id.tv_username_return_gerenxinxi);
		edt_alter_gerenxinxi_username = (EditText)findViewById(R.id.edt_alter_gerenxinxi_username);
		preferencesTool = SharedPreferencesTool.getSharedPreferencesToolInstance(this);

	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_end_alter_username.setOnClickListener(new BtnClickListener());
		tv_username_return_gerenxinxi.setOnClickListener(new BtnClickListener());
	}
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_end_alter_username://完成用户名修改
				String temp_username = edt_alter_gerenxinxi_username.getText().toString();
				if(temp_username.length() == 0 ){
					Toast.makeText(getApplicationContext(), "用户名不得为空", Toast.LENGTH_SHORT).show();
				}
				else if(temp_username.length() <= 6){
					/*
					 * 修改本地数据库
					 */
					Map<String, String> map=new HashMap<String, String>();
					map.put("username", temp_username);
					preferencesTool.saveUserPrefences(map);
					/*
					 * 修改服务器数据库
					 */
					sendInfoDialog = new Dialog(AlterUsernameActicity.this);
					sendInfoDialog.show();
					sendInfoDialog.setCancelable(false);
					sendInfoDialog.setContentView(R.layout.activity_login_loading_dialog_view);
					User user  = new User(); 
					String avatar=preferencesTool.getUserPrefences().get("avatar");
		        	String height=preferencesTool.getUserPrefences().get("height");
		        	String weight=preferencesTool.getUserPrefences().get("weight");
		        	String gender=preferencesTool.getUserPrefences().get("gender");
		        	String phone=preferencesTool.getUserPrefences().get("phone");
		        	String hobby=preferencesTool.getUserPrefences().get("hobby");
		        	String signature=preferencesTool.getUserPrefences().get("signature");
		        	String regTime=preferencesTool.getUserPrefences().get("regTime");
		        	String userId=preferencesTool.getUserPrefences().get("userId");
		        	String password=preferencesTool.getUserPrefences().get("password");
		        	String latitude=preferencesTool.getUserPrefences().get("latitude");
		        	String longitude=preferencesTool.getUserPrefences().get("longitude");
		        	user.setLatitude(Double.parseDouble(latitude));
		        	user.setLongitude(Double.parseDouble(longitude));
		        	user.setUsername(temp_username);
		        	user.setUserId(Integer.valueOf(userId));
		        	user.setAvatar(avatar);
		        	user.setGender(gender);
		        	user.setHeight(Double.valueOf(height));
		        	user.setWeight(Double.valueOf(weight));
		        	user.setHobby(hobby);
		        	user.setPassword(password);
		        	user.setPhone(phone);
		        	user.setSignature(signature);
					new ChangeInfoService(AlterUsernameActicity.this).changeInfoService(user,sendInfoDialog);
					finish();
				}
				else{
					Toast.makeText(getApplicationContext(), "用户名不得超过6个字", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.tv_username_return_gerenxinxi://返回个人信息
				finish();
				break;
			default:
				break;
			}
		}
	}
}
