package com.chending.ttsports.activity;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.model.UserLoginWidgetModel;
import com.chending.ttsports.service.ChangeInfoService;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
/*
 * 选择拍照上传还是选择图片
 */
public class AlterPhotoActivity extends Activity{
	private LinearLayout layout;
	private Context mContext;
	public static int photo_flag = 0; 
	private Button btn_photo_choosephoto,btn_photo_paizhao,btn_photo_cancel;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alter_register_pthoto);
		init();
		addListener();
		layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "提示：点击窗口外部关闭窗口！", 
						Toast.LENGTH_SHORT).show();	
			}
		});
	}

	private void init() {
		layout = (LinearLayout)findViewById(R.id.ll_alter_photo);
		btn_photo_choosephoto = (Button)findViewById(R.id.btn_photo_choosephoto); 
		btn_photo_paizhao = (Button)findViewById(R.id.btn_photo_paizhao); 
		btn_photo_cancel = (Button)findViewById(R.id.btn_photo_cancel); 
	}
	/**
	 * 添加监听
	 */
	private void addListener(){
		btn_photo_choosephoto.setOnClickListener(new BtnClickListener());
		btn_photo_paizhao.setOnClickListener(new BtnClickListener());
		btn_photo_cancel.setOnClickListener(new BtnClickListener());
	}
	
	private class BtnClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			Intent intent = null;
			switch (v.getId()) {
			case R.id.btn_photo_paizhao://选择拍照
				intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				File dir = new File("/sdcard/ttsports/Camera/");
				if (!dir.exists()) {
					dir.mkdirs();
				}
				String touxiang = "1.jpg";
				Constant.mUploadPhotoPath = "/sdcard/ttsports/Camera/"
						+ touxiang.toString();
				//UUID.randomUUID().toString()
				File file = new File(
						Constant.mUploadPhotoPath);
				if (!file.exists()) {
					try {
						file.createNewFile();
					} catch (IOException e) {

					}
				}
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(file));
				startActivityForResult(intent,
								Constant.REQUESTCODE_UPLOADPHOTO_CAMERA);
				photo_flag = 1;
				finish();
				break;
			case R.id.btn_photo_choosephoto://选择图片
				final String IMAGE_TYPE = "image/*";
				final int IMAGE_CODE = 0;   
				Intent getAlbum = new Intent(Intent.ACTION_GET_CONTENT);
				getAlbum.setType(IMAGE_TYPE);
				startActivityForResult(getAlbum, IMAGE_CODE);
				finish();
				break;
			case R.id.btn_photo_cancel://取消
				finish();
				break;
			default:
				break;
			}
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent event){
		finish();
		return true;
	}
	
	
}
