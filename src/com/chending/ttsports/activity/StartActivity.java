package com.chending.ttsports.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import com.chaowen.yixin.R;
import com.chending.ttsports.common.Constant;

/**
 * 启动引导页
 * 
 * @author rendongwei
 * 
 */
public class StartActivity extends Activity {
	private boolean gpsisopen = false;
	private boolean threadisopen = true;
	private boolean returnThread = true;
	Context context = this;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start_activity);
			// 启动一个线程
		if(Constant.TAG_USER_HAS_LOGIN){
			 startActivity(new Intent(StartActivity.this, LoginActivity.class));
			finish();
		}else{
			new showThread().start();
		}
	}
	/*
	 * 页面停留1000ms并打开gps页面
	 */
	public class showThread extends Thread {
        @SuppressWarnings("deprecation")
		@Override
        public void run () {
            do {
                try {
                    Thread.sleep(1000);
                    threadisopen = false;
                    Message msg = new Message();
                    msg.what = Constant.GPS_OPEN;
                    mHandler.sendMessage(msg);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while(threadisopen);
        }
    }
	 @SuppressLint("HandlerLeak")
		private Handler mHandler = new Handler() {
	        @Override
	        public void handleMessage (Message msg) {
	            super.handleMessage(msg);
	            switch (msg.what) {
	                case Constant.GPS_OPEN:
	                	 if(!isOPen(context)){
	                		 gpsDialog();
	         			}
	                     else{
	                     	gpsisopen = true;
	                     	new returnThread().start();
	                     }
	                    break;
	                default:
	                    break;
	            }
	        }
	    };
	/*
	 * 返回本页面后停留1500ms 并进入登陆 
	 */
	public class returnThread extends Thread {
		@SuppressWarnings("deprecation")
		@Override
        public void run () {
            do {
                try {
                    Thread.sleep(1500);
                    startActivity(new Intent(StartActivity.this, LoginActivity.class));
    				finish();
    				returnThread = false;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while(returnThread);
        }
    }
	/**
	 * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
	 * @param context
	 * @return true 表示开启
	 */
	public static final boolean isOPen(final Context context) {
		LocationManager locationManager 
		                         = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		// 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
		boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
		if (gps ) {
			return true;
		}
		return false;
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
			/*
			 * 返回事件
			 */
			if(requestCode == 5){
				new returnThread().start();
			}
		}
	/**
	 * GPS打开对话框
	 */
	private void gpsDialog() {
		AlertDialog.Builder builder = new Builder(context);
		builder.setTitle("是否需要开启GPS");
		/*
		builder.setItems(new String[] { "温馨提示", "是否需要开启GPS" },
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Intent intent = null;
						switch (which) {
						case 0:
							break;

						case 1:
							break;
						}
					}
				});
				*/
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				new returnThread().start();
			}
		});
		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
 				startActivityForResult(intent, 5);
			}
		});
		builder.create().show();
	}
}