package com.chending.ttsports.common;

import java.util.List;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.ChartsVO;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.util.AsyncImageLoader.ImageCallback;
import com.chending.ttsports.util.ChartsViewHolder;

public class ChartsAdapter extends ArrayAdapter<ChartsVO>{
	private ListView listView;
	private AsyncImageLoader asyncImageLoader;
	private List<ChartsVO> chartsVo;

	public ChartsAdapter(Activity activity,
			List<ChartsVO> chartsVo, ListView listView) {
		super(activity, 0, chartsVo);
		this.listView = listView;
		this.chartsVo = chartsVo;
		asyncImageLoader = new AsyncImageLoader();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Activity activity = (Activity) getContext();

		// Inflate the views from XML
		View rowView = convertView;
		ChartsViewHolder viewHolder;
		if (rowView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.paihangbang_listview_item, null);
			viewHolder = new ChartsViewHolder(rowView);
			rowView.setTag(viewHolder);
		} else {
			viewHolder =  (ChartsViewHolder) rowView.getTag();
		}
		ChartsVO chartsvo = getItem(position);

		// Load the image and set it on the ImageView
		String imageUrl = chartsvo.getAvatar();
		ImageView imageView = viewHolder.getImageView();
		imageView.setTag(imageUrl);
		
		Drawable cachedImage = asyncImageLoader.loadDrawable(imageUrl,
				new ImageCallback() {
					public void imageLoaded(Drawable imageDrawable,
							String imageUrl) {
						ImageView imageViewByTag = (ImageView) listView
								.findViewWithTag(imageUrl);
						if (imageViewByTag != null) {
							System.out.println("------------------" + imageUrl);
							imageViewByTag.setImageDrawable(imageDrawable);
						}
					}
				});
		
		if (cachedImage == null) {
			imageView.setImageResource(R.drawable.touxiang);
		} else {
			imageView.setImageDrawable(cachedImage);
		}
		// Set the text on the TextView
		TextView tvUserName = viewHolder.getTvUserName();
		tvUserName.setText(chartsvo.getUsername());
		TextView tvSignature = viewHolder.getSignature();
		tvSignature.setText(chartsvo.getSignature());
		TextView tvMeters = viewHolder.getTvMeters();
		tvMeters.setText(String.valueOf(chartsvo.getDistance()));
		TextView tvNumber = viewHolder.getTvNumber();
		tvNumber.setText(String.valueOf(position+1));
		return rowView;
	}
	
}
