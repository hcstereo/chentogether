package com.chending.ttsports.common;

import android.R.integer;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.chaowen.yixin.R;
import com.chending.ttsports.util.AsyncImageLoader;

/* 
 * 要处理overlay点击事件时需要继承ItemizedOverlay 
 * 不处理点击事件时可直接生成ItemizedOverlay. 
 */  
public class MyOverlayItem extends ItemizedOverlay<OverlayItem> {  
    //用MapView构造ItemizedOverlay
	public boolean istap = false;
    public MyOverlayItem(Drawable mark,MapView mapView){  
            super(mark,mapView);  
    }  
    public boolean onTap(int index) {  
        //在此处理item点击事件  
    	Constant.istop = true;
    	istap = true;
        return true;  
    }  
    /*
    public boolean onTap(GeoPoint pt, MapView mapView){  
        //在此处理MapView的点击事件，当返回 true时  
        super.onTap(pt,mapView);  
        return false;  
     }  
     */
    
}          
   
