package com.chending.ttsports.common;

import java.util.List;

import org.apache.http.client.fluent.Content;

import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendRelVO;
import com.chending.ttsports.model.ViewCacheFriendReq;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.util.AsyncImageLoader.ImageCallback;

import android.R.integer;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FriendReqAdapter extends ArrayAdapter<FriendRelVO> {
	private ListView listView;
	private AsyncImageLoader asyncImageLoader;
	@SuppressWarnings("unused")
	private List<FriendRelVO> friendRelVOs;
	private Context context;
	public FriendReqAdapter(Activity activity, List<FriendRelVO> friendRelVOs,
			ListView listView) {
		super(activity, 0, friendRelVOs);
		this.listView = listView;
		this.friendRelVOs = friendRelVOs;
		this.context=activity;
		asyncImageLoader = new AsyncImageLoader();
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		Activity activity = (Activity) getContext();

		// Inflate the views from XML
		View rowView = convertView;
		ViewCacheFriendReq viewCache;
		if (rowView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.friend_req_listview_item, null);
			viewCache = new ViewCacheFriendReq(rowView);
			rowView.setTag(viewCache);
		} else {
			viewCache = (ViewCacheFriendReq) rowView.getTag();
		}
		FriendRelVO friendRelVO = getItem(position);

		// Load the image and set it on the ImageView
		String imageUrl = friendRelVO.getAvatar();
		ImageView imageView = viewCache.getIvReqItemIcon();
		imageView.setTag(imageUrl);
		Drawable cachedImage = asyncImageLoader.loadDrawable(imageUrl,
				new ImageCallback() {
					public void imageLoaded(Drawable imageDrawable,
							String imageUrl) {
						ImageView imageViewByTag = (ImageView) listView
								.findViewWithTag(imageUrl);
						if (imageViewByTag != null) {
							imageViewByTag.setImageDrawable(imageDrawable);
						}
					}
				});
		if (cachedImage == null) {
			imageView.setImageResource(R.drawable.touxiang);
		} else {
			imageView.setImageDrawable(cachedImage);
		}
		// Set the text on the TextView
		TextView textView = viewCache.getIvReqItemUsername();
		final TextView tvAgree = viewCache.getTvReqHasAgree();
		textView.setText(friendRelVO.getUsername());// 用户名
		TextView tvSignature = viewCache.getSignature();
		tvSignature.setText(friendRelVO.getSignature());
		final Button btnReqAgree=viewCache.getBtnReqAgree();//同意按钮
		btnReqAgree.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//当用户点击同意时
				Dialog dialog=new Dialog(context);
				dialog.show();
				dialog.setContentView(R.layout.activity_login_loading_dialog_view);
				new FriendService(context).dealFriendReqService(FriendReqAdapter.this,FriendService.friendRelVOs.get(position).getRelId(), 1, dialog);
			}
		});
		return rowView;
	}
	
}
