package com.chending.ttsports.common;

import java.text.SimpleDateFormat;
import java.util.List;
import com.chaowen.yixin.R;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.model.ViewCacheStatus;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.swiplistview.SwipeListView;
import com.chending.ttsports.util.ModelTool;
import com.chending.ttsports.util.UtilWeixin;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class StatusListAdapter extends ArrayAdapter<Status> {
	
	
	private IWXAPI api;
	private SwipeListView listView;
	private List<Status> status;
	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private SqliteDatabaseHelper databaseHelper;
	private Activity activity=null;
	public StatusListAdapter(Activity activity, List<Status> status,
			ListView listView) {
		super(activity, 0, status);
		this.listView = (SwipeListView) listView;
		this.status = status;
		this.activity=activity;
		databaseHelper = new SqliteDatabaseHelper(activity);
		api = WXAPIFactory.createWXAPI(activity, Constant.APP_ID, true);//register to weixin
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final Activity activity = (Activity) getContext();
		View rowView = convertView;
		ViewCacheStatus viewCache;
		if (rowView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.history_list_item, null);
			viewCache = new ViewCacheStatus(rowView);
			rowView.setTag(viewCache);
		} else {
			viewCache = (ViewCacheStatus) rowView.getTag();
		}
		final Status stua = getItem(position);

		ImageView imageView = viewCache.getIvHisType();
		int type = stua.getType();
		switch (type) {
		case 1:
			imageView.setBackgroundResource(R.drawable.runing_top_dialog);
			break;
		case 2:
			imageView.setBackgroundResource(R.drawable.walking_top_dialog);
			break;
		case 3:
			imageView.setBackgroundResource(R.drawable.skating_top_dialog);
			break;
		case 4:
			imageView.setBackgroundResource(R.drawable.skiing_top_dialog);
			break;
		default:
			break;
		}
		TextView tv_his_dis = viewCache.getTvHisDis();
		tv_his_dis.setText(String.valueOf(stua.getDistance()));// 设置距离

		TextView tv_his_time = viewCache.getTvHisTime();
		tv_his_time.setText(ModelTool.parseRunTime(stua.getRunTime()));

		TextView tv_his_cal = viewCache.getTvHisCal();
		tv_his_cal.setText(String.valueOf(stua.getCalories()));

		TextView tv_his_create_time = viewCache.getTvhisCreatetime();
		tv_his_create_time.setText(ModelTool.dateExchange(dateFormat
				.format(stua.getCreateTime())));

		Button btnHisShare = viewCache.getBtnHisShare();
		btnHisShare.setOnClickListener(new OnClickListener() {// 分享到朋友圈等
					@Override
					public void onClick(View v) {
						listView.closeOpenedItems();
						api.registerApp(Constant.APP_ID);
						final SendMessageToWX.Req req=getReq(stua.getStatusId());
						View viewDialog=LayoutInflater.from(activity).inflate(R.layout.pop_up_win_share, null);
						final Dialog dialog=getDialog(viewDialog);
						dialog.show();//显示对话框
						LinearLayout lin_share_weixin=(LinearLayout) viewDialog.findViewById(R.id.lin_share_weixin);
						LinearLayout lin_share_friend=(LinearLayout) viewDialog.findViewById(R.id.lin_share_friend);
						//朋友圈
						lin_share_weixin.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								req.scene =SendMessageToWX.Req.WXSceneTimeline;
								api.sendReq(req);//像微信发送网址
								dialog.dismiss();//关闭对话框
							}
						});
						//分享给朋友
						lin_share_friend.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								req.scene =SendMessageToWX.Req.WXSceneSession;
								api.sendReq(req);
								dialog.dismiss();
							}
						});
					}
				});

		Button btnHisDelete = viewCache.getBtnHisDelete();
		btnHisDelete.setOnClickListener(new OnClickListener() {// 删除此条记录,同时应该删除服务器上的此条记录
					@Override
					public void onClick(View v) {
						int id = status.get(position).getStatusId();
						databaseHelper
								.execSql("delete from status where statusId="
										+ id);// 删除用户在本地的记录
						status.remove(position);
						nitifyAdapter();
						listView.closeOpenedItems();
					}
				});
		return rowView;
	}

	public void nitifyAdapter() {
		this.notifyDataSetChanged();
	}
	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}
	private Dialog getDialog(View viewDialog){
		Dialog dialog=new Dialog(activity,R.style.FullHeightDialog);
		//dialog.setTitle("分享到");
		//dialog.setCancelable(true);
		dialog.setContentView(viewDialog);
		WindowManager m = activity.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
        Window dialogWindow = dialog.getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        dialogWindow.setGravity(Gravity.CENTER | Gravity.CENTER);
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.height = (int) (d.getHeight() * 0.25);
        p.width = (int) (d.getWidth() * 0.623); 
        dialogWindow.setAttributes(p);
        return dialog;
	}
	private SendMessageToWX.Req getReq(int statusId){
		WXWebpageObject webpage = new WXWebpageObject();
		webpage.webpageUrl = "http://chendingts.duapp.com/history!getStatusRecord?statusId="+statusId;
		WXMediaMessage msg = new WXMediaMessage(webpage);
		msg.title = "Together";
		msg.description = "这是我的记录，你敢来挑战吗？";
		Bitmap thumb = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_launcher);
		msg.thumbData = UtilWeixin.bmpToByteArray(thumb, true);
		final SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("webpage");
		req.message = msg;
		return req;
	}
}
