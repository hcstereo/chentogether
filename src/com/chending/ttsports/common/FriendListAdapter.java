package com.chending.ttsports.common;

import java.util.List;
import com.chaowen.yixin.R;
import com.chending.ttsports.model.FriendInfo;
import com.chending.ttsports.model.ViewCacheFriend;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.util.AsyncImageLoader.ImageCallback;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FriendListAdapter extends ArrayAdapter<FriendInfo> {
	    private ListView listView;
	    private AsyncImageLoader asyncImageLoader;
	    private List<FriendInfo> imageAndTexts;
	    public FriendListAdapter(Activity activity, List<FriendInfo> imageAndTexts, ListView listView) {
	        super(activity, 0, imageAndTexts);
	        this.listView = listView;
	        this.imageAndTexts=imageAndTexts;
	        asyncImageLoader = new AsyncImageLoader();
	    }
	    public View getView(int position, View convertView, ViewGroup parent) {
	        Activity activity = (Activity) getContext();
	        // Inflate the views from XML
	        View rowView = convertView;
	        ViewCacheFriend viewCache;
	        if (rowView == null) {
	            LayoutInflater inflater = activity.getLayoutInflater();
	            rowView = inflater.inflate(R.layout.friend_listview_item, null);
	            viewCache = new ViewCacheFriend(rowView);
	            rowView.setTag(viewCache);
	        } else {
	            viewCache = (ViewCacheFriend) rowView.getTag();
	        }
	        FriendInfo imageAndText = getItem(position);

	        // Load the image and set it on the ImageView
	        String imageUrl = imageAndText.getImageUrl();
	        ImageView imageView = viewCache.getImageView();
	        imageView.setTag(imageUrl);
	        Drawable cachedImage=null;
        	cachedImage = asyncImageLoader.loadDrawable(imageUrl, new ImageCallback() {
	            public void imageLoaded(Drawable imageDrawable, String imageUrl) {
	                ImageView imageViewByTag = (ImageView) listView.findViewWithTag(imageUrl);
	                if (imageViewByTag != null) {
	                    imageViewByTag.setImageDrawable(imageDrawable);
	                }
	            }
	        });
        	if (cachedImage == null) {
				imageView.setImageResource(R.drawable.touxiang);
			}else{
				imageView.setImageDrawable(cachedImage);
			}
			
	        // Set the text on the TextView
	        TextView textView = viewCache.getTextView();
	        textView.setText(imageAndText.getText());
	        TextView tvSignature=viewCache.getSignature();
	        tvSignature.setText(imageAndText.getSignature());
	        return rowView;
	    }

}
