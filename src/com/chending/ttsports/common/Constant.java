package com.chending.ttsports.common;

public class Constant {
	public final static String NETWORK_NOT_AVAILABLE="网络连接出错了";
	public final static String NO_OTHERS_NEAR="荒山野岭的连个人影都没有！";
	public final static String NO_FRIEND="你还没有好友";
	public final static String NO_SEARCH_RESOULT="用户不存在";
	public final static String ADD_FRIEND_SUCCESS="发送成功，等待对方同意";
	public final static String ADD_FRIEND_FAIL="添加好友失败";
	public final static String LOGIN_USERNAME_PASSWORD_WRONG="用户名密码不匹配";
	public final static String ADD_STATUS_FAIL="运动数据发送失败";
	public final static String REGISTER_FAIL="注册失败";
	public final static String ADD_STATUS_SUCCESS="运动数据传送成功";
	public final static String REGISTER_SUCCESS="注册成功";
	public final static String ALTER_SUCCESS="修改成功";
	public final static String ALTER_FAIL="修改失败";
	public final static String SEND_MESSAGE_ERROR="信息发送失败";
	public final static String SERVER_ADDRESS="http://chendingts.duapp.com/";
	//public final static String SERVER_ADDRESS="http://192.168.191.1:8080/ttsports/";
	// Handler的参数
	public static final int MESSAGE_START = 0;
	public static final int MESSAGE_END = 1;
	public static final int MESSAGE_PAUSE = 2;
	public static final int INFO_SHOW = 3;
	public static final int PHOTO_SHOW = 4;//注册界面显示头像
	public static final int GPS_OPEN = 5;

	// 开始和结束 暂停标志位
	public static final int MARKERT_START = 0;
	public static final int MARKERT_END = 1;
	public static int start_flag = 1;//运动开始标志位
	public static int end_flag = 0; //运动结束标志位
	public static int move_flag = 1;//目标移动标志位
	/*
	 *是否按暂停按钮，0表示当前显示的是暂停，1表示当前显示继续
	 */
	public static int pausecontinue_flag = 0;
	public static boolean hasLogin=false;
	public final static int MSG_PAGE_SIZE=15;
	public static boolean IS_THREAD_START=false;//线程是否已开标识
	public static boolean IS_THREAD_SHUOULD_END=false;//线程是否应该结束标识
	public static boolean IS_MSG_THREAD_SHOULD_END=false;
	public static boolean IS_CHAT_THREAD_SHOULD_END=false;
	
	public static final String DEAL_FRIEND_REQ_SUCCESS="已同意好友请求";
	/**
	 * 存放拍照上传的照片路径
	 */
	public static String mUploadPhotoPath;
	public static final int REQUESTCODE_PHOTO = 1;
	public static final int REQUESTCODE_UPLOADPHOTO_CAMERA = 2;
	public static final int REQUESTCODE_CROP_PHOTO = 3;
	public static final int REQUESTCODE_GRXX_USERNAMAE= 4;
	public static final int REQUESTCODE__GRXX_GENDER = 5;
	public static final int REQUESTCODE_GRXX_HEIGHT = 6;
	public static final int REQUESTCODE_CROP_WEIGHT = 7;
	public static final int REQUESTCODE_CROP_HOBBY = 8;
	public static final int REQUESTCODE_CROP_SIGNATURE = 9;
	public static final String IMGPATH_ON_SERVER="file/1/avastar/";
	public static String IMG_NAME="";
	public static String IMG_PATH_AFTER_CROP="";

	public static int CURRENT_SECONDS=0;//应用打开后经过的秒数
	public static String CURRENT_TIME="";
	
	public static int TAG_ENTER_CHACK_DATA=-1;//如果用户进入了查看资料界面，该标识为1，否则为0，是为了刷新消息列表
	public static boolean istop = false;
	public static int friendnum = 0;
	
	public static boolean IS_IN_CHAT_ACTIVITY=false;
	public static String w1 = "今日天气 ：13℃/22℃";
	public static String w3 = "10月24日 多云 东南风3-4级";
	public static String w2 = "运动指数：较不适宜";
	public static String w4 = "风力较大，推荐您进行室内运动。";
	public static String Image_wether1 = "7.gif";
	public static String Image_wether2 = "10.gif";
	public static String Image_wether3 = "0.gif";
	public static String Image_wether4 = "1.gif";
	public static String city = "威海";
	
	public static boolean USER_HAS_LOGIN=false;//标识用户是否已经登录
	
	public static boolean TAG_USER_HAS_LOGIN=false;//标识用户是否已经登录
	public static final String APP_ID = "wx9c8abb89a6e9fdbf";
}
