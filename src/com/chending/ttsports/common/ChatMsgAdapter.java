package com.chending.ttsports.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import com.chaowen.yixin.R;
import com.chending.ttsports.model.ChatMsgEntity;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.util.AsyncImageLoader.ImageCallback;
import com.chending.ttsports.util.AsyncImageLoader.MsgImageCallback;

public class ChatMsgAdapter extends BaseAdapter {

	public static interface IMsgViewType {
		int IMVT_COM_MSG = 0;
		int IMVT_TO_MSG = 1;
	}

	@SuppressWarnings("unused")
	private static final String TAG = ChatMsgAdapter.class.getSimpleName();

	private List<ChatMsgEntity> coll;

	@SuppressWarnings("unused")
	private Context ctx;

	private LayoutInflater mInflater;

	private ListView listview;
	public ChatMsgAdapter(Context context, List<ChatMsgEntity> coll,ListView listview) {
		ctx = context;
		this.coll = coll;
		this.listview=listview;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return coll.size();
	}

	public Object getItem(int position) {
		return coll.get(position);
	}

	public long getItemId(int position) {
		return position;

	}

	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		ChatMsgEntity entity = coll.get(position);

		if (entity.isComMeg()) {
			return IMsgViewType.IMVT_COM_MSG;
		} else {
			return IMsgViewType.IMVT_TO_MSG;
		}

	}

	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ChatMsgEntity entity = coll.get(position);
		boolean isComMsg = entity.isComMeg();
		boolean isSameTime = entity.isSameTime();
		ViewHolder viewHolder = null;
		AsyncImageLoader imageLoader=new AsyncImageLoader();
		if (convertView == null) {
			if (isComMsg) {
				convertView = mInflater.inflate(
						R.layout.msg_chatting_item_left, null);
			} else {
				convertView = mInflater.inflate(
						R.layout.msg_chatting_item_right, null);
			}
			
			viewHolder = new ViewHolder();
			viewHolder.tvSendTime = (TextView) convertView
					.findViewById(R.id.tv_sendtime);
			viewHolder.tvUserName = (TextView) convertView
					.findViewById(R.id.tv_username);
			viewHolder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_chatcontent);
			viewHolder.isComMsg = isComMsg;
			viewHolder.isSameTime = isSameTime;
			viewHolder.ivUserhead=(ImageView) convertView.findViewById(R.id.iv_userhead);
			//viewHolder.ivUserhead.setTag(viewHolder);
			convertView.setTag(viewHolder);
			//回调函数加载图片
			imageLoader.loadDrawable(viewHolder,entity.getImagUrl(), new MsgImageCallback() {

				@Override
				public void imageLoaded(Drawable imageDrawable,
						ViewHolder viewHolder) {
					View view = (View) listview.findViewWithTag(viewHolder);
	                if (view != null) {
	                	ImageView imgview=(ImageView) view.findViewById(R.id.iv_userhead);
	                	imgview.setImageDrawable(imageDrawable);
	                }
				}
				
			});
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (isSameTime) {
			viewHolder.tvSendTime.setVisibility(View.GONE);
		}else{
			viewHolder.tvSendTime.setVisibility(View.VISIBLE);
			viewHolder.tvSendTime.setText(entity.getDate());
		}
		
		viewHolder.tvUserName.setText(entity.getName());
		viewHolder.tvContent.setText(entity.getText());
		
		return convertView;
	}

	public static class ViewHolder {
		public TextView tvSendTime;
		public TextView tvUserName;
		public TextView tvContent;
		public boolean isComMsg = true;
		public boolean isSameTime = false;
		
		public ImageView ivUserhead;
	}

}
