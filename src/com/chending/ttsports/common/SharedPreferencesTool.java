package com.chending.ttsports.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;





import com.chending.ttsports.model.User;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
/**
 * 构造单例用于存储获得用户偏好（如用户信息，用户收藏等等）
 * @author Xiaona
 *
 */
public class SharedPreferencesTool {
	private String userPreferencesXmlName="identity_user_preferences";
	private Context context;
	private SharedPreferences sharedPreferences;
	private static SharedPreferencesTool sharedPreferencesTool;
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	/**
	 * 私有构造方法，单例模式
	 * @param context
	 */
	private SharedPreferencesTool(Context context){
		this.context=context;
	}
	/**
	 * 通过该方法获得SharedPreferencesTool对象
	 * @param context
	 * @return
	 */
	public static SharedPreferencesTool getSharedPreferencesToolInstance(Context context){
		if(sharedPreferencesTool==null){
			sharedPreferencesTool=new SharedPreferencesTool(context);
		}
		return sharedPreferencesTool;
	}
	public void saveUserPrefences(Map<String, String> params){
		sharedPreferences=context.getSharedPreferences(userPreferencesXmlName, Context.MODE_PRIVATE);
		Editor editor=sharedPreferences.edit();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			editor.putString(entry.getKey(), entry.getValue());
		}
		editor.commit();
	}
	/**
	 * 读取identity_user_preferences，返回map类型的数据
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String,String> getUserPrefences(){
		sharedPreferences=context.getSharedPreferences(userPreferencesXmlName, Context.MODE_PRIVATE);
		return (Map<String,String>)sharedPreferences.getAll();
	}
	
	@SuppressLint("SimpleDateFormat")
	public User toUserFromSharePreferences(Map<String, String> params){
		SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		User user=new User();
		if(params.get("username")==null){
			return null;
		}
		user.setUserId(Integer.parseInt(params.get("userId")));
		user.setUsername(params.get("username"));
		try {
			user.setRegTime(df.parse(params.get("regTime")));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("日期转化出错！");
			e.printStackTrace();
		}
		user.setPassword(params.get("password"));
		//user.setPortrait();
		user.setGender(params.get("gender"));
		if(params.get("weight")!=null){
			user.setWeight(Double.parseDouble(params.get("weight")));
		}
		if(params.get("height")!=null){
			user.setHeight(Double.parseDouble(params.get("height")));
		}
		user.setHobby(params.get("hobby"));
		user.setAvatar(params.get("avatar"));
		user.setPhone(params.get("phone"));
		
		return user;
	}
	public void clearUserPreferences(){
		sharedPreferences=context.getSharedPreferences(userPreferencesXmlName, Context.MODE_PRIVATE);
		Editor editor=sharedPreferences.edit();
		editor.clear();
		editor.commit();
	}
	
	public void saveUser(User userJson){
		Map<String, String> params = new HashMap<String, String>();
		params.put("userId",
				String.valueOf(userJson.getUserId()));
		params.put("username", userJson.getUsername());
		params.put("gender", userJson.getGender());
		params.put("password", userJson.getPassword());
		params.put("weight",
				String.valueOf(userJson.getWeight()));
		params.put("height",
				String.valueOf(userJson.getHeight()));
		params.put("hobby", userJson.getHobby());
		params.put("avatar", userJson.getAvatar());
		params.put("phone", userJson.getPhone());
		params.put("latitude",
				String.valueOf(userJson.getLatitude()));
		params.put("longitude",
				String.valueOf(userJson.getLongitude()));
		System.out.println("hahahaha" + userJson.getLatitude()
				+ " " + userJson.getLongitude());
		params.put("regTime",
				dateFormat.format(userJson.getRegTime()));
		params.put("signature", userJson.getSignature());
		saveUserPrefences(params);
	}
}
