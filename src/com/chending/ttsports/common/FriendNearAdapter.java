package com.chending.ttsports.common;

import java.util.List;
import com.chaowen.yixin.R;
import com.chending.ttsports.VO.FriendNearVO;
import com.chending.ttsports.model.ViewCacheFriendNear;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.util.AsyncImageLoader.ImageCallback;
import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FriendNearAdapter extends ArrayAdapter<FriendNearVO> {
	private ListView listView;
	private AsyncImageLoader asyncImageLoader;
	@SuppressWarnings("unused")
	private List<FriendNearVO> friendNearVOs;
	private Context context;
	public FriendNearAdapter(Activity activity, List<FriendNearVO> friendNearVOs,
			ListView listView) {
		super(activity, 0, friendNearVOs);
		this.listView = listView;
		this.friendNearVOs = friendNearVOs;
		this.context=activity;
		asyncImageLoader = new AsyncImageLoader();
	}

	@SuppressLint("UseValueOf")
	public View getView(final int position, View convertView, ViewGroup parent) {
		Activity activity = (Activity) getContext();

		// Inflate the views from XML
		View rowView = convertView;
		ViewCacheFriendNear viewCache;
		if (rowView == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.yuepao_item_list, null);
			viewCache = new ViewCacheFriendNear(rowView);
			rowView.setTag(viewCache);
		} else {
			viewCache = (ViewCacheFriendNear) rowView.getTag();
		}
		FriendNearVO friendNearVO = getItem(position);

		// Load the image and set it on the ImageView
		String imageUrl = friendNearVO.getAvatar();
		ImageView imageView = viewCache.getIvYpHead();
		imageView.setTag(imageUrl);
		Drawable cachedImage = asyncImageLoader.loadDrawable(imageUrl,
				new ImageCallback() {
					public void imageLoaded(Drawable imageDrawable,
							String imageUrl) {
						ImageView imageViewByTag = (ImageView) listView
								.findViewWithTag(imageUrl);
						if (imageViewByTag != null) {
							imageViewByTag.setImageDrawable(imageDrawable);
						}
					}
				});
		if (cachedImage == null) {
			imageView.setImageResource(R.drawable.touxiang);
		} else {
			imageView.setImageDrawable(cachedImage);
		}
		
		ImageView ypGender=viewCache.getIvYpGender();
		if(friendNearVO.getGender().equals("男")){
			ypGender.setImageResource(R.drawable.ic_sex_male);//男
		}else{
			ypGender.setImageResource(R.drawable.ic_sex_female);//女
		}
		TextView ypUsername = viewCache.getTvYpUsername();
		ypUsername.setText(friendNearVO.getUsername());
		
		TextView ypDis = viewCache.getTvYpDis();
		ypDis.setText(String.valueOf(new Integer((int) friendNearVO.getDisFrom())));
		
		TextView ypType = viewCache.getTvYpType();
		String typrStr="";
		switch(friendNearVO.getRectype()){
		case 0:
			typrStr="运动 ";
			break;
		case 1:
			typrStr="步行 ";
			break;
		case 2:
			typrStr="跑步 ";
			break;
		case 3:
			typrStr="滑冰 ";
			break;
		case 4:
			typrStr="滑雪 ";
			break;
		case 5:
			typrStr="骑行 ";
			break;
		}
		ypType.setText(typrStr);
		
		TextView ypSportdis=viewCache.getTvYpSportdis();
		if(friendNearVO.getDistanceRecent()*10%10==0){
			ypSportdis.setText(String.valueOf(new Integer((int)friendNearVO.getDistanceRecent())));
		}else{
			ypSportdis.setText(String.valueOf(friendNearVO.getDistanceRecent()));
		}
		
		return rowView;
	}
	
}
