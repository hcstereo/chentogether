package com.chending.ttsports.common;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.chaowen.yixin.R;
import com.chending.ttsports.model.FriendMsg;
import com.chending.ttsports.model.ViewCacheFriendMsg;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.swiplistview.SwipeListView;
import com.chending.ttsports.util.AsyncImageLoader;
import com.chending.ttsports.util.ModelTool;
import com.chending.ttsports.util.AsyncImageLoader.ImageCallback;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class MsgListAdapter extends ArrayAdapter<FriendMsg> {
    private SwipeListView listView;
    private AsyncImageLoader asyncImageLoader;
    private List<FriendMsg> friendMsgs;
    private SqliteDatabaseHelper databaseHelper;
    private SharedPreferencesTool preferencesTool;
    public MsgListAdapter(Activity activity, List<FriendMsg> friendMsgs, ListView listView) {
        super(activity, 0, friendMsgs);
        this.listView = (SwipeListView) listView;
        this.friendMsgs=friendMsgs;
        databaseHelper=new SqliteDatabaseHelper(activity);
        preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(activity);
        asyncImageLoader = new AsyncImageLoader();
    }
    
    public View getView(final int position, View convertView, ViewGroup parent) {
        Activity activity = (Activity) getContext();
        // Inflate the views from XML
        View rowView = convertView;
        ViewCacheFriendMsg viewCache;
        if (rowView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            rowView = inflater.inflate(R.layout.msg_listview_item, null);
            viewCache = new ViewCacheFriendMsg(rowView);
            rowView.setTag(viewCache);
        } else {
            viewCache = (ViewCacheFriendMsg) rowView.getTag();
        }
        final FriendMsg friendMsg = getItem(position);
        
        // Load the image and set it on the ImageView
        String avatar = friendMsg.getAvatar();
        ImageView ivHead = viewCache.getIvHead();
        ivHead.setTag(avatar);
        Drawable cachedImage = asyncImageLoader.loadDrawable(avatar, new ImageCallback() {
            public void imageLoaded(Drawable imageDrawable, String imageUrl) {
                ImageView imageViewByTag = (ImageView) listView.findViewWithTag(imageUrl);
                if (imageViewByTag != null) {
                    imageViewByTag.setImageDrawable(imageDrawable);
                }
            }
        });
		if (cachedImage == null) {
			ivHead.setImageResource(R.drawable.touxiang);
		}else{
			ivHead.setImageDrawable(cachedImage);
		}
		if(friendMsg.getUnReadQuantity()!=0){
			TextView tvUnReadQuantity = viewCache.getTvUnReadQuantity();
			tvUnReadQuantity.setText(String.valueOf(friendMsg.getUnReadQuantity()));//未读消息数量
			tvUnReadQuantity.setVisibility(View.VISIBLE);//设置textview可见
		}else {
			TextView tvUnReadQuantity = viewCache.getTvUnReadQuantity();
			tvUnReadQuantity.setVisibility(View.GONE);//设置textview可见
		}
        // Set the text on the TextView
        TextView tvUsername = viewCache.getTvUsername();
        tvUsername.setText(friendMsg.getUsername());//用户名
        TextView tvRecentMsg=viewCache.getTvRecentMsg();
        tvRecentMsg.setText(friendMsg.getRecentMsg());
        TextView tvLastTime=viewCache.getTvLastTime();
        tvLastTime.setText(ModelTool.dateExchange(friendMsg.getLastTime()));
        
        Button btnMsgDelete=viewCache.getBtnMsgDelete();
        btnMsgDelete.setOnClickListener(new OnClickListener(){//匿名内部类实现
			@Override
			public void onClick(View v) {//刪除与用户的聊天信息
				String id1=String.valueOf(preferencesTool.getUserPrefences().get("userId"));//user
				String id2=String.valueOf(friendMsg.getUserId());//friend
				String id3=id1;//mainUser
				databaseHelper.execDelete("chatmsg", "(fromUserId=? and toUserId=? and mainUser=?) or (fromUserId=? and toUserId=? and mainUser=?)", new String[]{id1,id2,id3,id2,id1,id3});
				friendMsgs.remove(position);
				nitifyAdapter();
				listView.closeOpenedItems();
			}
        });
        
        return rowView;
    }
    public void nitifyAdapter(){
    	this.notifyDataSetChanged();
    }
    /**
	 * 把日期字符串转化为带有今天、昨天的时间字符串返回
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private String dateExchange(String dateStr) {
		Date date;
		Date dateNow = new Date();
		String newDateStr = null;
		try {
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = dateFormat.parse(dateStr);
			if (date.getYear() == dateNow.getYear()) {// 今年
				if (date.getMonth() == dateNow.getMonth()) {// 本月
					if (date.getDate() == dateNow.getDate()) {// 今天
						newDateStr = "今天" + date.getHours() + ":"
								+ date.getMinutes();
					} else if (date.getDate() + 1 == dateNow.getDate()) {
						newDateStr = "昨天" + date.getHours() + ":"
								+ date.getMinutes();
					} else {
						SimpleDateFormat dateFormat1 = new SimpleDateFormat(
								"MM月dd日HH:mm");
						newDateStr = dateFormat1.format(date);
					}
				} else {
					SimpleDateFormat dateFormat2 = new SimpleDateFormat(
							"MM月dd日HH:mm");
					newDateStr = dateFormat2.format(date);
				}
			} else {
				SimpleDateFormat dateFormat3 = new SimpleDateFormat(
						"yyyy年MM月dd日HH:mm");
				newDateStr = dateFormat3.format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return newDateStr;
	}
}
