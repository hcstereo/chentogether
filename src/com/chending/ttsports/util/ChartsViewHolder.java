package com.chending.ttsports.util;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chaowen.yixin.R;

public class ChartsViewHolder {
	private View baseView;
    private TextView tv_username,tv_meters,tv_number;
    private ImageView imageView;
    private TextView signature;
    public ChartsViewHolder(View baseView) {
        this.baseView = baseView;
    }

    public TextView getTvUserName() {
        if (tv_username == null) {
        	tv_username = (TextView) baseView.findViewById(R.id.iv_charts_item_username);
        }
        return tv_username;
    }
    public TextView getTvMeters(){
    	 if (tv_meters == null) {
    		 tv_meters = (TextView) baseView.findViewById(R.id.tv_charts_meters);
         }
         return tv_meters;
    }
    public TextView getTvNumber() {
        if (tv_number == null) {
        	tv_number = (TextView) baseView.findViewById(R.id.txt_charts_item_number);
        }
        return tv_number;
    }
    public TextView getSignature() {
        if (signature == null) {
        	signature = (TextView) baseView.findViewById(R.id.tv_charts_signature);
        }
        return signature;
    }
    public ImageView getImageView() {
        if (imageView == null) {
            imageView = (ImageView) baseView.findViewById(R.id.iv_charts_item_icon);
        }
        return imageView;
    }
}
