package com.chending.ttsports.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;

import com.chending.ttsports.VO.ChatMsgVO;
import com.chending.ttsports.VO.FriendRelVO;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.model.FriendInfo;
import com.chending.ttsports.service.FriendService;

@SuppressLint("SimpleDateFormat")
public class ModelTool {
	public static List<FriendInfo> toImageAndText(List<FriendsVO> friendsVOs){
		List<FriendInfo> andTexts=new ArrayList<FriendInfo>();
		for(int i=0;i<friendsVOs.size();i++){
			andTexts.add(new FriendInfo(friendsVOs.get(i).getAvatar(), friendsVOs.get(i).getUsername(), friendsVOs.get(i).getUsername(), friendsVOs.get(i).getGender(), friendsVOs.get(i).getHeight(), friendsVOs.get(i).getWeight(), friendsVOs.get(i).getHobby(), friendsVOs.get(i).getAvatar(), friendsVOs.get(i).getPhone(), friendsVOs.get(i).getRegTime(),friendsVOs.get(i).getSignature()));
		}
		return andTexts;
	}
	public static int getFriendId(List<FriendsVO> friendsVOs,String username){
		for(FriendsVO friend:friendsVOs){
			if(friend.getUsername().equals(username)){
				return friend.getFriendsId();
			}
		}
		return -1;
	}
	public static List<ChatMsgVO> getMsgList(List<ChatMsgVO> chatMsgVOs,String username){
		List<ChatMsgVO> msgs=new ArrayList<ChatMsgVO>();
		for(ChatMsgVO msg:chatMsgVOs){
			if(msg.getFromUsername().equals(username)){
				msgs.add(msg);
			}
		}
		return msgs;
	}
	public static FriendsVO getFriendsInfo(String username){
		for(FriendsVO friendsVO:FriendService.friendsVOs){
			if(friendsVO.getUsername().equals(username)){
				return friendsVO;
			}
		}
		return null;
	}
	/**
	 * 把日期字符串转化为带有今天、昨天的时间字符串返回
	 * 
	 * @param dateStr
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String dateExchange(String dateStr) {
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date;
		Date dateNow = new Date();
		String newDateStr = null;
		SimpleDateFormat df1=new SimpleDateFormat("HH:mm");
		try {
			date = dateFormat.parse(dateStr);
			if (date.getYear() == dateNow.getYear()) {// 今年
				if (date.getMonth() == dateNow.getMonth()) {// 本月
					if (date.getDate() == dateNow.getDate()) {// 今天
						newDateStr = "今天" + df1.format(date);
					} else if (date.getDate() + 1 == dateNow.getDate()) {
						newDateStr = "昨天" + df1.format(date);
					} else {
						SimpleDateFormat dateFormat1 = new SimpleDateFormat(
								"MM月dd日HH:mm");
						newDateStr = dateFormat1.format(date);
					}
				} else {
					SimpleDateFormat dateFormat2 = new SimpleDateFormat(
							"MM月dd日HH:mm");
					newDateStr = dateFormat2.format(date);
				}
			} else {
				SimpleDateFormat dateFormat3 = new SimpleDateFormat(
						"yyyy年MM月dd日HH:mm");
				newDateStr = dateFormat3.format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return newDateStr;
	}
	/**
	 * 判断该list中是否已经有该条信息（用户添加好友请求）
	 * @param friendRelVOs
	 * @param username
	 * @return
	 */
	public static boolean hasInFriendReqList(List<FriendRelVO> friendRelVOs,String username){
		for(FriendRelVO friendRelVO:friendRelVOs){
			if(friendRelVO.getUsername().equals(username)){
				return true;
			}
		}
		return false;
	}
	/**
	 * 把运动时间转化为时分秒
	 * @param runTime
	 * @return
	 */
	public static String parseRunTime(int runTime){
		String time=null;
		if(runTime/60<=0){//不够一分钟
			time=runTime+"秒";
		}else if(runTime/3600<=0){//不到一小时
			time=runTime/60+"分"+runTime%60+"秒";
		}else{
			time=runTime/3600+"小时"+(runTime%3600)/60+"分"+(runTime%3600)%60+"秒";
		}
		return time;
	}
}
