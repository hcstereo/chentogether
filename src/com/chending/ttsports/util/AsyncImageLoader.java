package com.chending.ttsports.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import com.chending.ttsports.common.BcsOperation;
import com.chending.ttsports.common.ChatMsgAdapter.ViewHolder;
import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

@SuppressLint("HandlerLeak")
public class AsyncImageLoader {

	private HashMap<String, SoftReference<Drawable>> imageCache;

	public AsyncImageLoader() {
		imageCache = new HashMap<String, SoftReference<Drawable>>();
	}
	
	/**
	 * 如果该图片已经存在本地则直接加载
	 * 否则根据url将图片保存到本地
	 * @param imageUrl
	 * @param imageCallback
	 * @return
	 */
	public Drawable loadDrawable(final String imageUrl,
			final ImageCallback imageCallback) {
		if (imageCache.containsKey(imageUrl)) {
			SoftReference<Drawable> softReference = imageCache.get(imageUrl);
			Drawable drawable = softReference.get();
			if (drawable != null) {
				return drawable;
			}
		}
		String[] fileString=imageUrl.split("/");
        int size=fileString.length;
        String fileName=fileString[size-1];//字符串分割得到头像的名字
        File sdDir = null;
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
		if (sdCardExist) {
			sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
		}
        final File imgFile=new File(sdDir.toString()+"/"+"ttsports/touxiang/"+fileName);//头像图片的本地的地址
        
		final Handler handler = new Handler() {
			public void handleMessage(Message message) {
				imageCallback.imageLoaded((Drawable) message.obj, imageUrl);
			}
		};
		if(!imgFile.exists()){//需要从网络加载
			new Thread() {
				@Override
				public void run() {
					Drawable drawable = loadImageFromUrl(imageUrl);
					imageCache.put(imageUrl, new SoftReference<Drawable>(drawable));
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			}.start();
		}else{//本地加载
			new Thread() {
				@Override
				public void run() {
					Drawable drawable=Drawable.createFromPath(imgFile.toString());
					imageCache.put(imageUrl, new SoftReference<Drawable>(drawable));
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			}.start();
		}
		return null;
	}
	/**
	 * 消息界面头像网络获取
	 * @param viewHolder
	 * @param imageUrl
	 * @param imageCallback
	 * @return
	 */
	public Drawable loadDrawable(final ViewHolder viewHolder,final String imageUrl,
			final MsgImageCallback msgImageCallback) {
		System.out.println("imageUrl"+imageUrl);
		if (imageCache.containsKey(imageUrl)) {
			SoftReference<Drawable> softReference = imageCache.get(viewHolder);
			Drawable drawable = softReference.get();
			if (drawable != null) {
				return drawable;
			}
		}
		String[] fileString=imageUrl.split("/");
        int size=fileString.length;
        String fileName=fileString[size-1];//字符串分割得到头像的名字
        File sdDir = null;
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
		if (sdCardExist) {
			sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
		}
        final File imgFile=new File(sdDir.toString()+"/"+"ttsports/touxiang/"+fileName);//头像图片的本地的地址
        
		final Handler handler = new Handler() {
			public void handleMessage(Message message) {
				msgImageCallback.imageLoaded((Drawable) message.obj, viewHolder);
			}
		};
		if(!imgFile.exists()){//需要从网络加载
			new Thread() {
				@Override
				public void run() {
					Drawable drawable = loadImageFromUrl(imageUrl);
					imageCache.put(imageUrl, new SoftReference<Drawable>(drawable));
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			}.start();
		}else{//本地加载
			new Thread() {
				@Override
				public void run() {
					Drawable drawable=Drawable.createFromPath(imgFile.toString());
					imageCache.put(imageUrl, new SoftReference<Drawable>(drawable));
					Message message = handler.obtainMessage(0, drawable);
					handler.sendMessage(message);
				}
			}.start();
		}
		return null;
	}
	/**
	 * 根据地址从网络下载图片
	 * @param url
	 * @return
	 */
	public static Drawable loadImageFromUrl(String url) {
		String[] filenameStrings=url.split("/");
		int size=filenameStrings.length;
		String fileneame=filenameStrings[size-1];
		File sdDir = null;
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
		if (sdCardExist) {
			sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
		}
		File file = new File(sdDir.toString()+"/"+"ttsports/touxiang/");
		if(!file.exists()){
			file.mkdirs();//创建目录
		}
		Drawable d=null;
		try {
			File fileImg=new File(sdDir.toString()+"/"+"ttsports/touxiang/"+fileneame);
			BcsOperation bcsOperation=BcsOperation.getInstance();
			bcsOperation.getObject(url, fileImg);
			System.out.println(url);
			InputStream is=new FileInputStream(fileImg);
			d=Drawable.createFromStream(is, "src");
			is.close();
//			FileOutputStream out=new FileOutputStream(fileImg);
//			m = new URL(url);
//			i = (InputStream) m.getContent();
//			InputStream input=(InputStream) m.getContent();
//			d = Drawable.createFromStream(i, "src");
//			int length=10240;
//			byte[] buffer=new byte[length];			
//			while(true){
//				int ins=input.read(buffer);
//				if(ins==-1){
//					input.close();
//					out.flush();
//					out.close();
//					//return newFilePath;
//				}else
//					out.write(buffer,0,ins);
//			}
//		} catch (MalformedURLException e1) {
//			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return d;
	}
	public static void loadImage(final String imageUrl,final ImageView imageView) {
		
		String[] fileString=imageUrl.split("/");
        int size=fileString.length;
        String fileName=fileString[size-1];//字符串分割得到头像的名字
        File sdDir = null;
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED); // 判断sd卡是否存在
		if (sdCardExist) {
			sdDir = Environment.getExternalStorageDirectory();// 获取跟目录
		}
        final File imgFile=new File(sdDir.toString()+"/"+"ttsports/touxiang/"+fileName);//头像图片的本地的地址
        
		final Handler handler = new Handler() {
			public void handleMessage(Message message) {
				imageView.setImageDrawable((Drawable)message.obj);
			}
		};
		if(!imgFile.exists()){//需要从网络加载
			new Thread() {
				@Override
				public void run() {
					Drawable drawable = loadImageFromUrl(imageUrl);
					Message message = new Message();
					message.obj=drawable;
					handler.sendMessage(message);
				}
			}.start();
		}else{//本地加载
			new Thread() {
				@Override
				public void run() {
					Drawable drawable=Drawable.createFromPath(imgFile.toString());
					Message message = new Message();
					message.obj=drawable;
					handler.sendMessage(message);
				}
			}.start();
		}
	}
	
	public interface ImageCallback {
		public void imageLoaded(Drawable imageDrawable, String imageUrl);
	}
	public interface MsgImageCallback {
		public void imageLoaded(Drawable imageDrawable, ViewHolder viewHolder);
	}
}
