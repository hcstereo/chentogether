package com.chending.ttsports.service;

import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;
import android.widget.Toast;

import com.chending.ttsports.VO.ChartsVO;
import com.chending.ttsports.common.ChartsAdapter;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.protocol.ChartsProtocol;
import com.chending.ttsports.util.NetTools;

public class ChartsService {
	private Context context;
	private ListView lv_charts_list;
	//public static List<ChartsVO> chartsVOs=new ArrayList<ChartsVO>();
	public static List<ChartsVO> chartsVOs=new Vector<ChartsVO>();
	
	public ChartsService(Context context){
		this.context=context;
	}
	
	@SuppressLint("HandlerLeak")
	Handler handler=new Handler(){
		@SuppressLint("SimpleDateFormat")
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -2:
				Toast.makeText(context, Constant.NO_FRIEND, Toast.LENGTH_SHORT).show();//没有好友
				break;
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();//网络连接错误
				break;
			case 1:
				ChartsAdapter adapter = new ChartsAdapter((Activity) context, chartsVOs, lv_charts_list);
				//ImageAndTextListAdapter adapter=new ImageAndTextListAdapter((Activity) context, ModelChangeTool.toImageAndText(friendsVOs), lv_charts_list);
				lv_charts_list.setAdapter(adapter);
//		    	if(adapter.getCount()==0){
//		    		Fragment_friend.tv_friends_nofriends.setVisibility(View.VISIBLE);
//		    		Fragment_friend.viewPullFresh.setVisibility(View.GONE);
//		    	}else {
//		    		Fragment_friend.tv_friends_nofriends.setVisibility(View.GONE);
//		    		Fragment_friend.viewPullFresh.setVisibility(View.VISIBLE);
//				}
				break;
			default:
				break;
			}
			//FragmentTitleBarFriend.pbar_friend_refresh.setVisibility(View.GONE);
		}
	};
	private class ChartsServiceThread extends Thread{
		private Handler handler;
		private Bundle bundle;
		public ChartsServiceThread(Handler handler){
			this.handler=handler;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle=new Bundle();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			try {
				sleep(350);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ChartsProtocol chartsProtocol=new ChartsProtocol();
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
//			}else if((chartsVOs=friendsProtocol.getFriendList(userId, context))==null){
//				sendCode(-2);//没有好友
			}else{
				chartsVOs = chartsProtocol.getChartsList(context);
				sendCode(1);//刷新成功
			}
		}
	}
	public void chartsService(ListView lv_charts_list){
		this.lv_charts_list=lv_charts_list;
		new ChartsServiceThread(handler).start();
	}
}
