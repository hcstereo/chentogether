package com.chending.ttsports.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import com.chending.ttsports.VO.FriendRelVO;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.activity.CheckDataActivity;
import com.chending.ttsports.activity.FragmentTitleBarFriend;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.FriendListAdapter;
import com.chending.ttsports.common.FriendReqAdapter;
import com.chending.ttsports.common.PullToRefreshViewFriend;
import com.chending.ttsports.model.User;
import com.chending.ttsports.protocol.FriendsProtocol;
import com.chending.ttsports.util.ModelTool;
import com.chending.ttsports.util.NetTools;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class FriendService {
	private Context context;
	private ListView lv_friend_list;
	private static PullToRefreshViewFriend pullToRefreshView;
	private static Dialog searchLoadingDialog,addFriendloadingDialog;
	//public static List<FriendsVO> friendsVOs=new ArrayList<FriendsVO>();
	public static List<FriendsVO> friendsVOs=new Vector<FriendsVO>();
	//public static List<FriendRelVO> friendRelVOs=new ArrayList<FriendRelVO>();
	public static List<FriendRelVO> friendRelVOs=new Vector<FriendRelVO>();
	private static User user;
	private Dialog dialog;
	private FriendReqAdapter adapter;
	public FriendService(Context context){
		this.context=context;
	}
	@SuppressLint("HandlerLeak")
	Handler handler=new Handler(){
		@SuppressLint("SimpleDateFormat")
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -3:
				Toast.makeText(context, "出错了", Toast.LENGTH_SHORT).show();
				break;
			case -2:
				Toast.makeText(context, Constant.NO_FRIEND, Toast.LENGTH_SHORT).show();//没有好友
				break;
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();//网络连接错误
				break;
			case 1:
				FriendListAdapter adapter=new FriendListAdapter((Activity) context, ModelTool.toImageAndText(friendsVOs), lv_friend_list);
		    	lv_friend_list.setAdapter(adapter);
				break;
			default:
				break;
			}
			if(FriendService.pullToRefreshView!=null){
				
				SimpleDateFormat df=new SimpleDateFormat("MM-dd hh:mm");
				FriendService.pullToRefreshView.onHeaderRefreshComplete(df.format(new Date()));
			}
			FragmentTitleBarFriend.pbar_friend_refresh.setVisibility(View.GONE);
		}
	};
	private class FriendServiceThread extends Thread{
		private Handler handler;
		private Bundle bundle;
		private int userId;
		public FriendServiceThread(Handler handler,int userId){
			this.handler=handler;
			this.userId=userId;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle=new Bundle();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			FriendsProtocol friendsProtocol=new FriendsProtocol();
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}else{
				List<FriendsVO> friendsVOs=null;
				friendsVOs=friendsProtocol.getFriendList(userId, context);
				if(friendsVOs==null){
					sendCode(-3);//没有好友
				}else if(friendsVOs.size()==0){
					sendCode(-2);//没有好友
				}else{
					FriendService.friendsVOs=friendsVOs;
					sendCode(1);//刷新成功
				}
			}
		}
	}
	private class noRefreshFriendServiceThread extends Thread{
		private Handler handler;
		private Bundle bundle;
		public noRefreshFriendServiceThread(Handler handler){
			this.handler=handler;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle=new Bundle();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			try {
				sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			sendCode(1);
		}
	}
	/**
	 * 搜索好友Handler
	 */
	@SuppressLint("HandlerLeak")
	private Handler handler2=new Handler(){
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			FriendService.searchLoadingDialog.dismiss();
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -2:
				Toast.makeText(context, Constant.NO_SEARCH_RESOULT, Toast.LENGTH_SHORT).show();//没有好友
				break;
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();//网络连接错误
				break;
			case 1:
				Intent intent=new Intent(context,CheckDataActivity.class);
				context.startActivity(intent);
				break;
			default:
				break;
			}
		}
		
	};
	/**
	 * 搜索好友线程
	 * @author Chen
	 *
	 */
	private class FriendSearch extends Thread{
		private String username;
		private Handler handler2;
		private Bundle bundle;
		public FriendSearch(String username,Handler handler2){
			this.username=username;
			this.handler2=handler2;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle=new Bundle();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler2.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			FriendsProtocol friendsProtocol=new FriendsProtocol();
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}else if((FriendService.user=friendsProtocol.searchFriend(username)).getUsername()==null){
				sendCode(-2);//未搜索到好友
			}else{
				sendCode(1);//搜索好友成功
			}
		}
	}
	@SuppressLint("HandlerLeak")
	private Handler handler3=new Handler(){
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			FriendService.addFriendloadingDialog.dismiss();
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -2:
				Toast.makeText(context, Constant.ADD_FRIEND_FAIL, Toast.LENGTH_SHORT).show();//添加失败
				break;
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();//网络连接错误
				break;
			case 1:
				Toast.makeText(context, Constant.ADD_FRIEND_SUCCESS, Toast.LENGTH_SHORT).show();//发送成功
				Activity activity=(Activity)context;
				activity.finish();
				break;
			default:
				break;
			}
		}
		
	};
	private class AddFriendThread extends Thread{
		private int hostId,friendId;
		private Handler handler3;
		private Bundle bundle;
		public AddFriendThread(int hostId,int friendId,Handler handler3){
			this.hostId=hostId;
			this.friendId=friendId;
			this.handler3=handler3;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle=new Bundle();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler3.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			FriendsProtocol friendsProtocol=new FriendsProtocol();
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}else if(friendsProtocol.addFriend(hostId, friendId)){
				sendCode(1);//添加成功
			}else{
				sendCode(-2);//添加失败
			}
		}
		
	}
	@SuppressLint("HandlerLeak")
	private Handler handler4=new Handler(){

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			int code=msg.getData().getInt("code");
			int relId=msg.getData().getInt("relId");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();//网络连接错误
				break;
			case 1:
				Toast.makeText(context, Constant.DEAL_FRIEND_REQ_SUCCESS, Toast.LENGTH_SHORT).show();
				dialog.dismiss();
				for(int i=0;i<FriendService.friendRelVOs.size();i++){
					if(FriendService.friendRelVOs.get(i).getRelId()==relId){
						FriendRelVO friendRelVO=FriendService.friendRelVOs.get(i);
						FriendsVO friendsVO=new FriendsVO(friendRelVO.getUsername(), friendRelVO.getGender(), friendRelVO.getHeight(), friendRelVO.getWeight(), friendRelVO.getHobby(), friendRelVO.getAvatar(), friendRelVO.getPhone(), friendRelVO.getRegTime(), friendRelVO.getSignature(), friendRelVO.getUserId());
						FriendService.friendsVOs.add(friendsVO);
						FriendService.friendRelVOs.remove(i);
						break;
					}
				}
				adapter.notifyDataSetChanged();
				break;
			default:
				break;
			}
		}
		
	};
	private class DealFriendReqqThread extends Thread{
		private Handler handler4;
		private int relId;
		private int relState;
		private Bundle bundle;
		
		public DealFriendReqqThread(Handler handler4,int relId,int relState){
			this.handler4=handler4;
			this.relId=relId;
			this.relState=relState;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle=new Bundle();
			bundle.putInt("code", code);
			bundle.putInt("relId", relId);
			message.setData(bundle);
			handler4.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}else{
				if(new FriendsProtocol().dealFriendReq(context,relId, relState)){
					sendCode(1);
				}else{
					sendCode(-2);//失败
				}
				
			}
		}
	}
	public void friendService(int userId,ListView lv_friend_list){
		this.lv_friend_list=lv_friend_list;
		new FriendServiceThread(handler, userId).start();
	}
	public void friendService(int userId,ListView lv_friend_list,PullToRefreshViewFriend pullToRefreshView){
		this.lv_friend_list=lv_friend_list;
		FriendService.pullToRefreshView=pullToRefreshView;
		new FriendServiceThread(handler, userId).start();
	}
	public void searchFriendService(String username,Dialog searchLoadingDialog){
		FriendService.searchLoadingDialog=searchLoadingDialog;
		new FriendSearch(username,handler2).start();
	}
	public void addFriendService(int hostId,int friendId,Dialog addFriendloadingDialog){
		FriendService.addFriendloadingDialog=addFriendloadingDialog;
		new AddFriendThread(hostId,friendId,handler3).start();
	}
	public void noRefreshFriendService(ListView lv_friend_list){
		this.lv_friend_list=lv_friend_list;
		new noRefreshFriendServiceThread(handler).start();
	}
	public void friendReqService(ListView lv_friendreq_list){
		
	}
	/**
	 * 处理添加好友请求
	 * @param relId
	 * @param relState
	 * @param dialog
	 * @param textView
	 * @param button
	 */
	public void dealFriendReqService(FriendReqAdapter adapter,int relId,int relState,Dialog dialog){
		this.dialog=dialog;
		this.adapter=adapter;
		new DealFriendReqqThread(handler4,relId,relState).start();
	}
	
	public static User getUser() {
		if(user==null){
			user=new User();
		}
		return user;
	}
	public static void setUser(User user) {
		FriendService.user = user;
	}
}
