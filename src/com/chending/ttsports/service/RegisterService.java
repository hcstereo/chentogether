package com.chending.ttsports.service;

import java.io.File;
import com.chending.ttsports.activity.LoginActivity;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.model.User;
import com.chending.ttsports.protocol.RegisterProtocol;
import com.chending.ttsports.protocol.UploadProtocol;
import com.chending.ttsports.util.NetTools;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class RegisterService {
	private static Dialog registerDialog;
	private Context context;
	public RegisterService(Context context){
		this.context=context;
	}
	private Handler handler=new Handler(){

		@Override
		public void handleMessage(Message msg) {
			registerDialog.dismiss();
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				break;
			case 0:
				Toast.makeText(context, Constant.REGISTER_FAIL, Toast.LENGTH_SHORT).show();
				break;
			case 1:
				Toast.makeText(context, Constant.REGISTER_SUCCESS, Toast.LENGTH_SHORT).show();
				Intent intent=new Intent(context,LoginActivity.class);
				context.startActivity(intent);
				((Activity) context).finish();
				break;
			default:
				break;
			}
		}
	};
	private class RegisterThread extends Thread{
		private Handler handler;
		private User user;
		private Message message;
		private Bundle bundle;
		public RegisterThread(User user,Handler handler){
			this.handler=handler;
			this.user=user;
			
		}
		private void sendCode(int code){
			message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			System.out.println("thread---"+user.getUsername());
			String[] paths=Constant.mUploadPhotoPath.split("/");
			String imgName=paths[paths.length-1];
			user.setAvatar("/"+Constant.IMGPATH_ON_SERVER+imgName);
			bundle=new Bundle();
			boolean bl=false;
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}
			else if(new UploadProtocol().uploadFile(new File(Constant.mUploadPhotoPath))&&new RegisterProtocol().register(user, context)){
				sendCode(1);//注册成功
			}
			else if(!bl){
				sendCode(0);//注册失败
			}
		}
	}
	/**
	 * 注册
	 * @param user
	 * @return
	 */
	public void registerService(User user,Dialog dialog){
		RegisterService.registerDialog=dialog;
		new RegisterThread(user,handler).start();
	}
}
