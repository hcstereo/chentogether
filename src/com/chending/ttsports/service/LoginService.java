package com.chending.ttsports.service;

import java.util.Map;

import com.chending.ttsports.activity.LoginActivity;
import com.chending.ttsports.activity.MainActivity;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.model.UserLoginWidgetModel;
import com.chending.ttsports.protocol.LoginProtocol;
import com.chending.ttsports.util.DESUtils;
import com.chending.ttsports.util.NetTools;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class LoginService {
	private Context context;
	private static Dialog loadingDialog;
	private static UserLoginWidgetModel userModel;
	public LoginService(Context context){
		this.context=context;
		
	}
	/**
	 * 登陆时使用的线程
	 * @author Xiaona
	 *
	 */
	private class LoginServiceThread extends Thread{
		private Handler handler;
		private Bundle bundle;
		private User user;
		public LoginServiceThread(UserLoginWidgetModel userModel,Handler handler){
			this.handler=handler;
			LoginService.userModel=userModel;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			bundle=new Bundle();
			user=new User();
			LoginProtocol loginProtocol=new LoginProtocol();
			user.setUsername(userModel.getUsername().getText().toString().trim());
			try {
				user.setPassword(DESUtils.encrypt(userModel.getPassword().getText().toString().trim()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			boolean bl=false;
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}else if((bl=loginProtocol.checkLogin(user,context,true))){
				sendCode(1);
				Constant.USER_HAS_LOGIN=true;
	//			Constant.IS_THREAD_START=true;//线程开了
//				Constant.IS_THREAD_SHUOULD_END=false;//线程不应该现在结束
//				while(!Constant.IS_THREAD_SHUOULD_END){
//					try {
						//LoginProtocol loginProtocol1=new LoginProtocol();
						//sleep(2500);
						//loginProtocol1.checkLogin(user,context,false);
//					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
			}
			else if(!bl){
				sendCode(0);
			}
		}
	}
	@SuppressLint("HandlerLeak")
	private Handler handler=new Handler(){
		@SuppressLint({ "HandlerLeak", "SimpleDateFormat" })
		public void handleMessage(Message msg) {
			loadingDialog.dismiss();
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				break;
			case 0:
				Toast.makeText(context, Constant.LOGIN_USERNAME_PASSWORD_WRONG, Toast.LENGTH_SHORT).show();
				break;
			case 1:
				Intent intent=new Intent(context,MainActivity.class);
				Bundle bundle1=new Bundle();
	            bundle1.putString("SERVER_RUN_IN_BACHGROUND", "0");
	            intent.putExtras(bundle1);
				context.startActivity(intent);
				LoginActivity activity=(LoginActivity)context;
				activity.finish();
				break;
			default:
				break;
			}
		}
	};
	/**
	 * 如果用户已经登陆过，获取用户信息，否则返回null
	 * @return
	 */
	public User getUserLogin(){
		SharedPreferencesTool sharedPreferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(context);
		Map<String, String> params=sharedPreferencesTool.getUserPrefences();
		return sharedPreferencesTool.toUserFromSharePreferences(params);
	}
	public void loginService(UserLoginWidgetModel userModel,Dialog dialog){
		LoginService.loadingDialog=dialog;
		new LoginServiceThread(userModel,handler).start();
	}
}
