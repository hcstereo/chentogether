package com.chending.ttsports.service;

import com.chaowen.yixin.R;
import com.chending.ttsports.activity.MainActivity;
import com.chending.ttsports.common.Constant;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class MsgService extends Service{
	public static final String TAG="MsgService";
	public static UserLoginThread loginThread;
    public static Notification notification;
    public static NotificationManager manager;
    public static Intent intent;
    public static PendingIntent pintent;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate() execute");
		notification = new Notification();
		notification.icon = R.drawable.ic_launcher;
		notification.tickerText = "新消息";
		notification.defaults = Notification.DEFAULT_SOUND;
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //点击顶部状态栏的消息后，welcomeactivity为需要跳转的页面
        intent = new Intent(this, MainActivity.class);
        Bundle bundle=new Bundle();
        bundle.putString("SERVER_RUN_IN_BACHGROUND", "1");
        intent.putExtras(bundle);
        pintent = PendingIntent.getActivity(this, 0, intent, 0);
        Log.d(TAG, "startThread() execute");
		loginThread=new UserLoginThread(MsgService.this,notification,manager,pintent);
		loginThread.start();//开启线程
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy() execute");
	}

	class MsgBinder extends Binder{
		
	}
}

