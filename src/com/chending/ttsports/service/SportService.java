package com.chending.ttsports.service;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.protocol.Sportprotocol;
import com.chending.ttsports.util.NetTools;

public class SportService {
	private static Dialog sportDialog;
	private Context context;
	public SportService(Context context){
		this.context=context;
	}
	@SuppressLint("HandlerLeak")
	private Handler handler=new Handler(){

		@Override
		public void handleMessage(Message msg) {
			sportDialog.dismiss();
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				break;
			case 0:
				Toast.makeText(context, Constant.ADD_STATUS_FAIL, Toast.LENGTH_SHORT).show();
				break;
			case 1:
				Toast.makeText(context, Constant.ADD_STATUS_SUCCESS, Toast.LENGTH_SHORT).show();
//				Intent intent=new Intent(context,LoginActivity.class);
//				context.startActivity(intent);
//				((Activity) context).finish();
				break;
			default:
				break;
			}
		}
	};
	private class SportThread extends Thread{
		private Handler handler;
		private Status status;
		private Message message;
		private Bundle bundle;
		public SportThread(Status status,Handler handler){
			this.handler=handler;
			this.status=status;
			
		}
		private void sendCode(int code){
			message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			bundle=new Bundle();
			boolean bl=false;
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}
			else if(new Sportprotocol().addStatus(status, context)){
				sendCode(1);//发送成功
			}
			else if(!bl){
				sendCode(0);//发送失败
			}
		}
	}
	/**
	 * 注册
	 * @param user
	 * @return
	 */
	public void sportService(Status status,Dialog dialog){
		SportService.sportDialog=dialog;
		new SportThread(status,handler).start();
	}
}
