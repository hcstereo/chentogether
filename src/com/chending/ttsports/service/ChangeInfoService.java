package com.chending.ttsports.service;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.chending.ttsports.activity.Fragment_gerenxinxi;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.model.User;
import com.chending.ttsports.protocol.InfoProtocol;
import com.chending.ttsports.protocol.UploadProtocol;
import com.chending.ttsports.util.NetTools;

public class ChangeInfoService {
	private static Dialog changeInfodDialog;
	private Context context;
	public ChangeInfoService(Context context){
		this.context=context;
	}
	@SuppressLint("HandlerLeak")
	private Handler handler=new Handler(){

		@Override
		public void handleMessage(Message msg) {
			changeInfodDialog.dismiss();
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				break;
			case 0:
				//Toast.makeText(context, Constant.ALTER_FAIL, Toast.LENGTH_SHORT).show();
				break;
			case 1:
				//Toast.makeText(context, Constant.ALTER_SUCCESS, Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
			}
		}
	};
	private class ChangeInfoThread extends Thread{
		private Handler handler;
		private User user;
		private Message message;
		private Bundle bundle;
		public ChangeInfoThread(User user,Handler handler){
			this.handler=handler;
			this.user=user;
			
		}
		private void sendCode(int code){
			message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			System.out.println("thread---"+user.getUsername());
			bundle=new Bundle();
			@SuppressWarnings("unused")
			boolean bl=false;
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}
			/*
			 * 如果头像进行了更改 则需要将头像上传到云存储中
			 */
			else if(Fragment_gerenxinxi.touxianig_flag){
				if(new UploadProtocol().uploadFile(new File(Constant.mUploadPhotoPath))&&new InfoProtocol().changeInfo(user, context)){
					sendCode(1);//修改个人信息成功
				}
			}
			else if(bl = new InfoProtocol().changeInfo(user, context)){
				sendCode(1);//修改个人信息成功
			}
			else{
				sendCode(0);//修改个人信息失败
			}
				
		}
	}
	/**
	 * 修改个人信息
	 * @param user
	 * @return
	 */
	public void changeInfoService(User user,Dialog dialog){
		ChangeInfoService.changeInfodDialog=dialog;
		new ChangeInfoThread(user,handler).start();
	}
	///////////////////////////////////////////////////add
	private class ChangeLocThread extends Thread{
		private Handler handler;
		private User user;
		private Message message;
		private Bundle bundle;
		public ChangeLocThread(User user,Handler handler){
			this.handler=handler;
			this.user=user;
			
		}
		private void sendCode(int code){
			message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			System.out.println("thread---"+user.getUsername());
			bundle=new Bundle();
			@SuppressWarnings("unused")
			boolean bl=false;
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}
			/*
			 * 如果头像进行了更改 则需要将头像上传到云存储中
			 */
			if(new InfoProtocol().changeLoc(user, context)){
				sendCode(1);//修改个人信息成功
			}
			else{
				sendCode(0);//修改个人信息失败
			}
				
		}
	}
	public void changeLocService(User user,Dialog dialog){
		ChangeInfoService.changeInfodDialog=dialog;
		new ChangeLocThread(user,handler).start();
	}
	///////////////////////////////////////////////////
}
