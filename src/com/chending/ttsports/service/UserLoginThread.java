package com.chending.ttsports.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.util.Log;

import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.protocol.HistoryProtocol;
import com.chending.ttsports.protocol.LoginProtocol;

public class UserLoginThread extends Thread{
	private Context context;
	public static final String TAG="UserLoginThread";
	private Notification notification;
	private NotificationManager manager;
	private PendingIntent intent;
	public UserLoginThread(MsgService msgService, Notification mN,
			NotificationManager mNm,PendingIntent intent) {
		super();
		this.context = msgService;
		this.notification=mN;
		this.manager=mNm;
		this.intent=intent;
	}
	@Override
	public void run() {
		super.run();
		LoginProtocol loginProtocol=new LoginProtocol(context,notification,manager,intent);
		SharedPreferencesTool sharedPreferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(context);
		if(Constant.USER_HAS_LOGIN){
			loginProtocol.checkLogin(sharedPreferencesTool.toUserFromSharePreferences(sharedPreferencesTool.getUserPrefences()), context, false);
		}else{
			loginProtocol.checkLogin(sharedPreferencesTool.toUserFromSharePreferences(sharedPreferencesTool.getUserPrefences()), context, true);
		}
		HistoryProtocol history=new HistoryProtocol();
		HistoryService.setStatus(history.getHistoryList(Integer.parseInt(sharedPreferencesTool.getUserPrefences().get("userId")), context));
		while(!Constant.IS_THREAD_SHUOULD_END){
			try {
				Log.d(TAG, "UserLoginThread is running!!");
				System.out.println("线程开始");
				loginProtocol=new LoginProtocol(context,notification,manager,intent);
				sleep(2000);
				User user=sharedPreferencesTool.toUserFromSharePreferences(sharedPreferencesTool.getUserPrefences());
				Log.d(TAG, "user");
				loginProtocol.checkLogin(user, context, false);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
