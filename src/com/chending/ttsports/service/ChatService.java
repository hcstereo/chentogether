package com.chending.ttsports.service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import com.chending.ttsports.VO.ChatMsgVO;
import com.chending.ttsports.activity.FragmentTitleBarMsg;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.MsgListAdapter;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.FriendMsg;
import com.chending.ttsports.protocol.ChatProtocol;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.util.NetTools;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;


@SuppressLint("SimpleDateFormat")
public class ChatService {
	private MsgListAdapter adapter;
	private Context context;
	public static List<ChatMsgVO> chatMsgVOs=new Vector<ChatMsgVO>();
	public static List<FriendMsg> friendMsgs = new Vector<FriendMsg>();
	private SqliteDatabaseHelper databaseHelper;
	private ListView lv_msg_list;
	private SharedPreferencesTool preferencesTool;
	public ChatService(Context context){
		this.context=context;
		databaseHelper=new SqliteDatabaseHelper(context);
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(context);
	}
	@SuppressLint("HandlerLeak")
	private Handler handler=new Handler(){
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				break;
			case 1://发送成功
				
				break;
			case 0:
				Toast.makeText(context, Constant.SEND_MESSAGE_ERROR, Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
			}
		}
	};
	/**
	 * 该线程用于向服务器发送消息
	 * @author Chen
	 *
	 */
	private class SendMsgThread extends Thread{
		private ChatMsgVO chatMsgVO;
		private Bundle bundle=new Bundle();
		
		public SendMsgThread(ChatMsgVO chatMsgVO){
			this.chatMsgVO=chatMsgVO;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			super.run();
			ChatProtocol chatProtocol=new ChatProtocol();
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);
			}else if(chatProtocol.sendMessage(chatMsgVO, context)){
				sendCode(1);
			}else{
				sendCode(0);
			}
		}
	}
	@SuppressLint("HandlerLeak")
	private Handler handler1=new Handler(){
		
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			FragmentTitleBarMsg.pbar_msg_refresh.setVisibility(View.GONE);//设置进度条不显示
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				
				break;
			case 1:
				adapter=new MsgListAdapter((Activity)context, friendMsgs, lv_msg_list);
				lv_msg_list.setAdapter(adapter);
				break;
			case 2:
				if(adapter!=null){
					adapter.notifyDataSetChanged();//更新listview
				}
				break;
			default:
				break;
			}
		}
		
	};
	private class InitFriendMsgThread extends Thread{
		private String username;
		private Bundle bundle=new Bundle();
		private Handler handler;
		private int tag;
		private int userId;
		private SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		public InitFriendMsgThread(int userId,String username,Handler handler,int tag){
			this.username=username;
			this.handler=handler;
			this.userId=userId;
			this.tag=tag;
		}
		private void sendCode(int code){
			Message message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		
		
		@SuppressLint("SimpleDateFormat")
		@Override
		public void run() {
			super.run();
			try {
				sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//1.从本地数据库获取当前用户的聊天记录
			//2.从服务器传来的未读信息列表中读取所有的未读信息，并且一直监听，如果有该用户的未读信息，那么更新与该好友的item
			if(tag==1){
				getFriendMsgFromDB(username,userId);//将数据库中的聊天记录添加到list
				sendCode(1);
			}else if(tag==-1){
				sendCode(2);
				tag=0;
			}
			while(!Constant.IS_MSG_THREAD_SHOULD_END){//设置未读信息数量
				System.out.println("Constant.IS_MSG_THREAD_SHOULD_END=true!");
				int a=0;
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(ChatService.chatMsgVOs.size()>0){//说明有未读信息
					//从数据库中取出聊天记录显示到列表
					for(int i=0;i<ChatService.friendMsgs.size();i++){//遍历list每一条记录
						FriendMsg friendMsg=ChatService.friendMsgs.get(i);
						int size=ChatService.chatMsgVOs.size();
						for(int j=0;j<size;){
							a++;
							System.out.println(a);
							ChatMsgVO chatMsgVO=ChatService.chatMsgVOs.get(j);
							if(friendMsg.getUsername().equals(chatMsgVO.getFromUsername())){//如果list已经有该用户的聊天条目了，则直接更新list
								ChatService.friendMsgs.get(i).setRecentMsg(chatMsgVO.getMsgContent());
								ChatService.friendMsgs.get(i).setUnReadQuantity(friendMsg.getUnReadQuantity()+1);//未读消息数量加1
								ChatService.friendMsgs.get(i).setLastTime(dateFormat.format(chatMsgVO.getCreateTime()));//设置时间格式
								databaseHelper
								.execSql("insert into chatmsg (fromUserId,toUserId,msgContent,fromUsername,toUsername,createTime,avatar,mainUser,avatarOp) values ("
										+ chatMsgVO.getFromUserId()
										+ ","
										+ chatMsgVO.getToUserId()
										+ ",'"
										+ chatMsgVO.getMsgContent()
										+ "','"
										+ chatMsgVO.getFromUsername()
										+ "','"
										+ username
										+ "','"
										+ dateFormat.format(chatMsgVO.getCreateTime()) 
										+ "','"
										+chatMsgVO.getAvatar()
										+ "',"
										+chatMsgVO.getMainUser()
										+ ",'"
										+preferencesTool.getUserPrefences().get("avatar")
										+"')");
								System.out.println("1____insert__++");
								if(ChatService.chatMsgVOs.size()>j){
									ChatService.chatMsgVOs.remove(j);
									j++;
								}
								System.out.println("1____insert__++_size="+ChatService.chatMsgVOs.size());
								size=ChatService.chatMsgVOs.size();
							}
						}
					}
					//将服务器返回的信息显示到列表
					if(ChatService.chatMsgVOs.size()>0){//如果大小依然大于0
						for(int i=0;i<ChatService.chatMsgVOs.size();i++){//遍历每一条信息，将其添加到列表
							FriendMsg friendMsg=new FriendMsg();
							ChatMsgVO chatMsgVO=ChatService.chatMsgVOs.get(i);
							int tag=0;
							for(int j=0;j<ChatService.friendMsgs.size();j++){//遍历list每一条记录
								if(ChatService.friendMsgs.get(j).getUsername().equals(chatMsgVO.getFromUsername())){//已经有了该用户的聊天列表
									ChatService.friendMsgs.get(j).setUnReadQuantity(ChatService.friendMsgs.get(j).getUnReadQuantity());//未读消息数目加1
									ChatService.friendMsgs.get(j).setRecentMsg(chatMsgVO.getMsgContent());//设置最新消息内容
									ChatService.friendMsgs.get(j).setLastTime(dateFormat.format(chatMsgVO.getCreateTime()));//设置时间
									tag=1;
									databaseHelper
									.execSql("insert into chatmsg (fromUserId,toUserId,msgContent,fromUsername,toUsername,createTime,avatar,mainUser,avatarOp) values ("
											+ chatMsgVO.getFromUserId()
											+ ","
											+ chatMsgVO.getToUserId()
											+ ",'"
											+ chatMsgVO.getMsgContent()
											+ "','"
											+ chatMsgVO.getFromUsername()
											+ "','"
											+ username
											+ "','"
											+ dateFormat.format(chatMsgVO.getCreateTime()) 
											+ "','"
											+chatMsgVO.getAvatar()
											+ "',"
											+chatMsgVO.getMainUser()
											+",'"
											+preferencesTool.getUserPrefences().get("avatar")
											+"')");
									System.out.println("2____insert__++");
									if(ChatService.chatMsgVOs.size()>i){
										ChatService.chatMsgVOs.remove(i);
									}
								}
							}
							if(tag==0){
								friendMsg.setUnReadQuantity(1);//设置未读消息数目为1
								friendMsg.setAvatar(chatMsgVO.getAvatar());//头像
								friendMsg.setHasUnRead(true);//设置是否有未读消息为true
								friendMsg.setLastTime(dateFormat.format(chatMsgVO.getCreateTime()));
								friendMsg.setUsername(chatMsgVO.getFromUsername());
								friendMsg.setRecentMsg(chatMsgVO.getMsgContent());
								friendMsg.setUserId(chatMsgVO.getFromUserId());
								ChatService.friendMsgs.add(friendMsg);
								String sql="insert into chatmsg (fromUserId,toUserId,msgContent,fromUsername,toUsername,createTime,avatar,mainUser,avatarOp) values ("
										+ chatMsgVO.getFromUserId()
										+ ","
										+ chatMsgVO.getToUserId()
										+ ",'"
										+ chatMsgVO.getMsgContent()
										+ "','"
										+ chatMsgVO.getFromUsername()
										+ "','"
										+ username
										+ "','"
										+ dateFormat.format(chatMsgVO.getCreateTime()) 
										+ "','"
										+chatMsgVO.getAvatar()
										+ "',"
										+chatMsgVO.getMainUser()
										+ ",'"
										+preferencesTool.getUserPrefences().get("avatar")
										+"')";
								databaseHelper
								.execSql(sql);
								System.out.println("3____insert__++");
								if(ChatService.chatMsgVOs.size()>i){
									ChatService.chatMsgVOs.remove(i);
								}
								System.out.println("有新的短消息------------------");
							}
						}
					}
					sendCode(2);
				}
			}
		}
	}
	/**
	 * 向服务器发送消息，存放到数据库
	 * @param chatMsgVO
	 */
	public void sendMsgService(ChatMsgVO chatMsgVO){
		new SendMsgThread(chatMsgVO).start();
	}
	public void initFriendMsg(ListView lv_msg_list,String username,int userId,int tag){
		this.lv_msg_list=lv_msg_list;
		new InitFriendMsgThread(userId,username,handler1,tag).start();
	}
	private void getFriendMsgFromDB(String username,int userId){
		int friendId;
		String friendName;
		// 从数据库中取出与该用户的聊过天的好友
		Cursor cursor = databaseHelper
				.execQuery("select DISTINCT(toUserId),toUsername from chatmsg where fromUserId=? and mainUser=? UNION select DISTINCT(fromUserId),fromUsername from chatmsg where toUserId=? and mainUser=?",
				new String[] {String.valueOf(userId),String.valueOf(userId),String.valueOf(userId),String.valueOf(userId)});// 从SQLite中获取用户的聊天信息
		
		//读出每一个用户id
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			friendId=cursor.getInt(0);
			friendName=cursor.getString(1);
			Cursor cursor2=databaseHelper.execQuery("select * from chatmsg where (fromUserId=? and toUserId=? and mainUser=?) or (fromUserId=? and toUserId=?  and mainUser=?) order by msgId desc limit 1", new String[]{
					String.valueOf(userId),String.valueOf(friendId),String.valueOf(userId),
					String.valueOf(friendId),String.valueOf(userId),String.valueOf(userId)
			});
			for(cursor2.moveToFirst();!cursor2.isAfterLast();cursor2.moveToNext()){
				FriendMsg friendMsg=new FriendMsg();
				String avatar=getAcatar(friendId,userId,userId);
				if(avatar!=null){
					friendMsg.setAvatar(avatar);
				}else{
					friendMsg.setAvatar(getAcatar(userId,friendId,userId));
				}
				friendMsg.setHasUnRead(false);
				friendMsg.setLastTime(cursor2.getString(cursor2.getColumnIndex("createTime")));
				friendMsg.setRecentMsg(cursor2.getString(cursor2.getColumnIndex("msgContent")));
				friendMsg.setUnReadQuantity(0);
				friendMsg.setUserId(friendId);
				friendMsg.setUsername(friendName);
				friendMsgs.add(friendMsg);//添加到list
			}
		}
	}
	private String getAcatar(int fromUserId,int toUserId,int  mainUser){
		String sqlString=null;
		if(fromUserId==mainUser){
			sqlString="select avatarOp from chatmsg where fromUserId=? and toUserId=? and mainUser=?";
		}else{
			sqlString="select avatar from chatmsg where fromUserId=? and toUserId=? and mainUser=?";
		}
		Cursor cursor=databaseHelper.execQuery(sqlString, new String[]{
				String.valueOf(fromUserId),String.valueOf(toUserId),String.valueOf(mainUser)
		});
		String avatar=null;
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			avatar = cursor.getString(0);
			break;
		}
		return avatar;
	}
}
