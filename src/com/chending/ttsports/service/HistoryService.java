package com.chending.ttsports.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.activity.FragmentTitleBarHistory;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.common.StatusListAdapter;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.protocol.HistoryProtocol;
import com.chending.ttsports.util.NetTools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("HandlerLeak")
public class HistoryService {
	private Context context;
	public static StatusListAdapter adapter;
	public ListView lv_his_list;
	private static List<Status> status = new Vector<Status>();
	private List<TextView> textViews = new ArrayList<TextView>();

	public HistoryService(Context context) {
		this.context = context;
	}

	public void sortService(int sortType) {
		HistoryProtocol historyProtocol = new HistoryProtocol();
		switch (sortType) {
		case 1:// 日期
			status.clear();
			status.addAll(historyProtocol.sortList("createTime", context));
			break;
		case 2:// 距离
			status.clear();
			status.addAll(historyProtocol.sortList("distance", context));
			break;
		case 3:// 卡路里
			status.clear();
			status.addAll(historyProtocol.sortList("calories", context));
			break;
		case 4:// 运动时间
			status.clear();
			status.addAll(historyProtocol.sortList("runTime", context));
			break;
		default:
			break;
		}
		adapter.notifyDataSetChanged();
	}

	public void historyService(int userId, ListView lv_his_list,
			List<TextView> textViews) {
		this.lv_his_list = lv_his_list;
		this.textViews = textViews;
		new HistoryThread(userId, handler).start();
	}

	private Handler handler = new Handler() {
		Bundle bundle = new Bundle();

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			bundle = msg.getData();
			int code = bundle.getInt("code");
			FragmentTitleBarHistory.pbar_his_refresh.setVisibility(View.GONE);
			SharedPreferencesTool preferencesTool;
			switch (code) {
			
			case -3:
				Toast.makeText(context, "出错了", Toast.LENGTH_SHORT).show();
				break;
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE,
						Toast.LENGTH_SHORT).show();// 网络连接错误
				break;
			case -2:
				Toast.makeText(context, "没有记录！", Toast.LENGTH_SHORT).show();
				break;
			case 1:
				// 获取成功
				adapter = new StatusListAdapter((Activity) context, status,
						lv_his_list);
				lv_his_list.setAdapter(adapter);
				preferencesTool = SharedPreferencesTool
						.getSharedPreferencesToolInstance(context);
				Map<String, String> map = preferencesTool.getUserPrefences();
				String sumDis = map.get("sumDis");
				if (sumDis == null) {
					sumDis = "0";
				}
				String sumTime = map.get("sumTime");
				if (sumTime == null) {
					sumTime = "0";
				}
				String sumCal = map.get("sumCal");
				if (sumCal == null) {
					sumCal = "0";
				}
				textViews.get(0).setText(sumDis);
				textViews.get(1).setText(sumTime);
				textViews.get(2).setText(sumCal);
				if (adapter.getCount() == 0) {
					// Fragment_friend.tv_friends_nofriends.setVisibility(View.VISIBLE);
					// Fragment_friend.viewPullFresh.setVisibility(View.GONE);
				} else {
					// Fragment_friend.tv_friends_nofriends.setVisibility(View.GONE);
					// Fragment_friend.viewPullFresh.setVisibility(View.VISIBLE);
				}
				break;
			}
		}

	};

	public class HistoryThread extends Thread {
		private Handler handler;
		private int userId;
		private Bundle bundle;

		public HistoryThread(int userId, Handler handler) {
			this.userId = userId;
			this.handler = handler;
		}

		private void sendCode(int code) {
			Message message = new Message();
			bundle = new Bundle();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}

		@Override
		public void run() {
			super.run();
			try {
				sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(HistoryService.status!=null&&HistoryService.status.size()>0){
				sendCode(1);
			}else{
				HistoryProtocol historyProtocol = new HistoryProtocol();
				if (!NetTools.isNetworkAvailable(context)) {
					sendCode(-1);// 无网络连
				} else {
					if(HistoryService.status!=null&&HistoryService.status.size()>0){
						sendCode(1);
						return;
					}else{
						List<Status> status1=null;
						status1=historyProtocol.getHistoryList(userId, context);
						if(status1==null){
							sendCode(-3);
						}else if(status1.size()==0){
							sendCode(-2);//没有好友
						}else{
							HistoryService.status=status1;
							sendCode(1);//刷新成功
						}
					}
				}
			}
		}
	}

	public static List<Status> getStatus() {
		if (status == null) {
			status = new Vector<Status>();
		}
		return status;
	}

	public static void setStatus(List<Status> status) {
		HistoryService.status = status;
	}

}
