package com.chending.ttsports.service;

import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;
import android.widget.Toast;

import com.chending.ttsports.VO.FriendNearVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.FriendNearAdapter;
import com.chending.ttsports.protocol.YuepaoProtocol;
import com.chending.ttsports.util.NetTools;

public class YuepaoService {
	private Context context;
	public static List<FriendNearVO> friendNearVOs=new Vector<FriendNearVO>();
	private ListView lv_yuepao_list;
	public YuepaoService(Context context){
		this.context=context;
	}
	@SuppressLint("HandlerLeak")
	private Handler handler=new Handler(){

		@Override
		public void handleMessage(Message msg) {
			Bundle bundle=msg.getData();
			int code=bundle.getInt("code");
			switch (code) {
			case -1:
				Toast.makeText(context, Constant.NETWORK_NOT_AVAILABLE, Toast.LENGTH_SHORT).show();
				break;
			case 0:
				Toast.makeText(context, Constant.NO_OTHERS_NEAR, Toast.LENGTH_SHORT).show();
				break;
			case 1:
				//获取成功
				FriendNearAdapter adapter=new FriendNearAdapter((Activity)context, friendNearVOs, lv_yuepao_list);
				lv_yuepao_list.setAdapter(adapter);
				break;
			default:
				break;
			}
		}
	};
	private class YuepaoThread extends Thread{
		private Handler handler;
		private Message message;
		private int areaNo;
		private Bundle bundle;
		public YuepaoThread(int areaNo,Handler handler){
			this.handler=handler;
			this.areaNo=areaNo;
		}
		private void sendCode(int code){
			message=new Message();
			bundle.putInt("code", code);
			message.setData(bundle);
			handler.sendMessage(message);
		}
		@Override
		public void run() {
			bundle=new Bundle();
			try {
				sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(!NetTools.isNetworkAvailable(context)){
				sendCode(-1);//无网络连
			}
			else if((friendNearVOs=new YuepaoProtocol().getFriendNear(areaNo, context))==null){
				sendCode(-2);//失败
			}
			else if(friendNearVOs.size()==0){
				sendCode(0);//附近没人
			}else{
				//附近有人获取附近的人成功
				sendCode(1);
			}
		}
	}
	/**
	 * 注册
	 * @param user
	 * @return
	 */
	public void initService(int areaNo,ListView lv_yuepao_list){
		this.lv_yuepao_list=lv_yuepao_list;
		new YuepaoThread(areaNo,handler).start();
	}
	/*
  	 * 计算属于哪个区域
  	 */
  	public int regional_number(double lat, double lng){//纬度  经度
  		int i = 0,j = 0,num = 0,temp,count = 0;
  		i = (int) ((lat-4)/(0.5)+1);//属于哪一行
  		j = (int) ((lng-73)/(0.6)+1);//属于哪一列
  		temp = j;
  		while(temp != 0)
  		{
  			temp = temp/10;
  			count++;
  		}
  		num = (int) (i*Math.pow(10.0,(double)count)+j);
  		return num;
  	}
}
