package com.chending.ttsports.VO;

public class ChartsVO {
	private String username;
	private String gender;
	private double height;
	private double weight;
	private String hobby;
	private String signature;
	private String avatar;
	private double distance;
	
	public ChartsVO(String username, String gender, double height,
			double weight, String hobby, String signature, String avatar,
			double distance) {
		super();
		this.username = username;
		this.gender = gender;
		this.height = height;
		this.weight = weight;
		this.hobby = hobby;
		this.signature = signature;
		this.avatar = avatar;
		this.distance = distance;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	
	
}
