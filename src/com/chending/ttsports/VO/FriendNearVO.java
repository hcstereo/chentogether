package com.chending.ttsports.VO;

import java.io.Serializable;
import java.util.Date;

public class FriendNearVO implements Serializable,Comparable<FriendNearVO>{
	private static final long serialVersionUID = -7620435178023928252L;
	private int userId;
	private String username;
	private String gender;
	private double height;
	private double weight;
	private String hobby;
	private String avatar;
	private Date regTime;
	private String signature;
	private double latitude;//经度
	private double longitude;//纬度
	private int areaNo;
	private double distanceRecent;
	private int rectype;//最近一次运动的类型
	private Date recTime;
	private double disFrom;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Date getRegTime() {
		return regTime;
	}
	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getAreaNo() {
		return areaNo;
	}
	public void setAreaNo(int areaNo) {
		this.areaNo = areaNo;
	}
	public double getDistanceRecent() {
		return distanceRecent;
	}
	public void setDistanceRecent(double distanceRecent) {
		this.distanceRecent = distanceRecent;
	}
	public int getRectype() {
		return rectype;
	}
	public void setRectype(int rectype) {
		this.rectype = rectype;
	}
	public Date getRecTime() {
		return recTime;
	}
	public void setRecTime(Date recTime) {
		this.recTime = recTime;
	}
	
	public double getDisFrom() {
		return disFrom;
	}
	public void setDisFrom(double disFrom) {
		this.disFrom = disFrom;
	}
	@Override
	public int compareTo(FriendNearVO another) {
		Double d1=Double.valueOf(another.getDisFrom());
		Double d2=Double.valueOf(this.getDisFrom());
		return d2.compareTo(d1);
	}
	
}
