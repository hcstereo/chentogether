package com.chending.ttsports.VO;

import java.util.Date;
public class FriendsVO {
	private int friendsId;
	private String username;
	private String gender;
	private double height;
	private double weight;
	private String hobby;
	private String avatar;
	private String phone;
	private Date regTime;
	private String signature;
	public FriendsVO(String username, String gender, double height,
			double weight, String hobby, String avatar, String phone,
			Date regTime,String signature,int friendsId) {
		super();
		this.username = username;
		this.gender = gender;
		this.height = height;
		this.weight = weight;
		this.hobby = hobby;
		this.avatar = avatar;
		this.phone = phone;
		this.regTime = regTime;
		this.signature=signature;
		this.friendsId=friendsId;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getRegTime() {
		return regTime;
	}
	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public int getFriendsId() {
		return friendsId;
	}

	public void setFriendsId(int friendsId) {
		this.friendsId = friendsId;
	}

	
}

