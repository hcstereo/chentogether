package com.chending.ttsports.VO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class ChatMsgVO implements Parcelable{
	private int msgId;
	private int fromUserId;
	private int toUserId;
	private String msgContent;
	private int msgType;
	private int msgStateId;
	private Date createTime;
	private String fromUsername;
	private String toUsername;
	private String avatar;
	private int mainUser;
	private String avatarOp;
	public String getFromUsername() {
		return fromUsername;
	}
	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}
	public String getToUsername() {
		return toUsername;
	}
	public void setToUsername(String toUsername) {
		this.toUsername = toUsername;
	}
	public int getMsgId() {
		return msgId;
	}
	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}
	public int getFromUserId() {
		return fromUserId;
	}
	public void setFromUserId(int fromUserId) {
		this.fromUserId = fromUserId;
	}
	public int getToUserId() {
		return toUserId;
	}
	public void setToUserId(int toUserId) {
		this.toUserId = toUserId;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public int getMsgType() {
		return msgType;
	}
	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}
	public int getMsgStateId() {
		return msgStateId;
	}
	public void setMsgStateId(int msgStateId) {
		this.msgStateId = msgStateId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	
	public String getAvatarOp() {
		return avatarOp;
	}
	public void setAvatarOp(String avatarOp) {
		this.avatarOp = avatarOp;
	}
	public int getMainUser() {
		return mainUser;
	}
	public void setMainUser(int mainUser) {
		this.mainUser = mainUser;
	}
	public ChatMsgVO(int fromUserId, int toUserId,
			String msgContent, int msgType, int msgStateId, Date createTime,
			String fromUsername, String toUsername,String avatar,int mainUser,String avatarOp) {
		super();
		this.fromUserId = fromUserId;
		this.toUserId = toUserId;
		this.msgContent = msgContent;
		this.msgType = msgType;
		this.msgStateId = msgStateId;
		this.createTime = createTime;
		this.fromUsername = fromUsername;
		this.toUsername = toUsername;
		this.avatar=avatar;
		this.mainUser=mainUser;
		this.avatarOp=avatarOp;
	}
	public ChatMsgVO(String msgContent, Date createTime, String fromUsername,
			String toUsername,String avatar,int mainUser) {
		super();
		this.msgContent = msgContent;
		this.createTime = createTime;
		this.fromUsername = fromUsername;
		this.toUsername = toUsername;
		this.avatar=avatar;
		this.mainUser=mainUser;
	}
	
	public ChatMsgVO() {
		super();
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(msgId);
		dest.writeInt(fromUserId);
		dest.writeInt(toUserId);
		dest.writeString(msgContent);
		dest.writeInt(msgType);
		dest.writeInt(msgStateId);
		dest.writeString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(createTime));
		dest.writeString(fromUsername);
		dest.writeString(toUsername);
		dest.writeString(avatar);
		dest.writeInt(mainUser);
		dest.writeString(avatarOp);

	}
	public ChatMsgVO(Parcel parcel) {
		super();
		msgId=parcel.readInt();
		fromUserId=parcel.readInt();
		fromUserId=parcel.readInt();
		msgContent=parcel.readString();
		msgType=parcel.readInt();
		msgStateId=parcel.readInt();
		try {
			createTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(parcel.readString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fromUsername=parcel.readString();
		toUsername=parcel.readString();
		avatar=parcel.readString();
		mainUser=parcel.readInt();
		avatarOp=parcel.readString();

	}
	public static final Parcelable.Creator<ChatMsgVO> CREATOR = new Parcelable.Creator<ChatMsgVO>() { 

        @Override 
        public ChatMsgVO createFromParcel(Parcel source) { 
                return new ChatMsgVO(source); 
        } 

        @Override 
        public ChatMsgVO[] newArray(int size) { 
                return new ChatMsgVO[size]; 
        } 

}; 
	
}
