package com.chending.ttsports.model;

import com.chaowen.yixin.R;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewCacheFriendNear {

	    private View baseView;
	    private ImageView ivYpHead;
	    private ImageView ivYpGender;
	    private TextView tvYpUsername;
	    private TextView tvYpDis;
	    private TextView tvYpType;
	    private TextView tvYpSportdis;
	    
	    public ViewCacheFriendNear(View baseView) {
	        this.baseView = baseView;
	    }
	    
	    public ImageView getIvYpHead() {
	        if (ivYpHead == null) {
	        	ivYpHead = (ImageView) baseView.findViewById(R.id.iv_yp_head);
	        }
	        return ivYpHead;
	    }
	    public ImageView getIvYpGender() {
	        if (ivYpGender == null) {
	        	ivYpGender = (ImageView) baseView.findViewById(R.id.iv_yp_gender);
	        }
	        return ivYpGender;
	    }
	    public TextView getTvYpUsername() {
	        if (tvYpUsername == null) {
	        	tvYpUsername = (TextView) baseView.findViewById(R.id.tv_yp_username);
	        }
	        return tvYpUsername;
	    }
	    public TextView getTvYpDis() {
	        if (tvYpDis == null) {
	        	tvYpDis = (TextView) baseView.findViewById(R.id.tv_yp_dis);
	        }
	        return tvYpDis;
	    }
	    public TextView getTvYpType() {
	        if (tvYpType == null) {
	        	tvYpType = (TextView) baseView.findViewById(R.id.tv_yp_type);
	        }
	        return tvYpType;
	    }
	    public TextView getTvYpSportdis() {
	        if (tvYpSportdis == null) {
	        	tvYpSportdis = (TextView) baseView.findViewById(R.id.tv_yp_sportdis);
	        }
	        return tvYpSportdis;
	    }
}