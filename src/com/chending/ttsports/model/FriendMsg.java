package com.chending.ttsports.model;

public class FriendMsg {
	private int userId;
	private String username;
	private String recentMsg;
	private String lastTime;
	private String avatar;
	private boolean hasUnRead=false;
	private int unReadQuantity=0;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRecentMsg() {
		return recentMsg;
	}
	public void setRecentMsg(String recentMsg) {
		this.recentMsg = recentMsg;
	}
	
	public String getLastTime() {
		return lastTime;
	}
	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public boolean isHasUnRead() {
		return hasUnRead;
	}
	public void setHasUnRead(boolean hasUnRead) {
		this.hasUnRead = hasUnRead;
	}
	public int getUnReadQuantity() {
		return unReadQuantity;
	}
	public void setUnReadQuantity(int unReadQuantity) {
		this.unReadQuantity = unReadQuantity;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
