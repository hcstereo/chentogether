package com.chending.ttsports.model;

import com.chaowen.yixin.R;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewCacheStatus {

	    private View baseView;
	    private ImageView iv_his_type;
	    private TextView tv_his_dis;
	    private TextView tv_his_time;
	    private TextView tv_his_cal;
	    private TextView tv_his_create_time;
	    private Button btnHisShare;
	    private Button btnHisDelete;
	    
	    public ViewCacheStatus(View baseView) {
	        this.baseView = baseView;
	    }

	    public ImageView getIvHisType() {
	        if (iv_his_type == null) {
	        	iv_his_type = (ImageView) baseView.findViewById(R.id.iv_his_type);
	        }
	        return iv_his_type;
	    }
	    
	    public TextView getTvHisDis() {
	        if (tv_his_dis == null) {
	        	tv_his_dis = (TextView) baseView.findViewById(R.id.tv_his_dis);
	        }
	        return tv_his_dis;
	    }
	    
	    public TextView getTvHisTime() {
	        if (tv_his_time == null) {
	        	tv_his_time = (TextView) baseView.findViewById(R.id.tv_his_time);
	        }
	        return tv_his_time;
	    }
	    
	    public TextView getTvHisCal() {
	    	if (tv_his_cal == null) {
	    		tv_his_cal = (TextView) baseView.findViewById(R.id.tv_his_cal);
	    	}
	    	return tv_his_cal;
	    }
	    public TextView getTvhisCreatetime() {
	    	if (tv_his_create_time == null) {
	    		tv_his_create_time = (TextView) baseView.findViewById(R.id.tv_his_create_time);
	    	}
	    	return tv_his_create_time;
	    }
	    public Button getBtnHisShare() {
	    	if (btnHisShare == null) {
	    		btnHisShare = (Button) baseView.findViewById(R.id.btn_his_share);
	    	}
	    	return btnHisShare;
	    }
	    public Button getBtnHisDelete() {
	    	if (btnHisDelete == null) {
	    		btnHisDelete = (Button) baseView.findViewById(R.id.btn_his_delete);
	    	}
	    	return btnHisDelete;
	    }
}