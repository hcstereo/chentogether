package com.chending.ttsports.model;

import com.chaowen.yixin.R;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewCacheFriendMsg {

	    private View baseView;
	    private TextView tvUsername;
	    private TextView tvRecentMsg;
	    private TextView tvLastTime;
	    private TextView tvUnReadQuantity;
	    private ImageView ivHead;
	    private Button btnMsgDelete;
	    
	    
	    public ViewCacheFriendMsg(View baseView) {
	        this.baseView = baseView;
	    }
	    public TextView getTvUsername(){
	    	if (tvUsername == null) {
	    		tvUsername = (TextView) baseView.findViewById(R.id.iv_msg_item_username);
	        }
	        return tvUsername;
	    }
	    
	    public TextView getTvRecentMsg(){
	    	if (tvRecentMsg == null) {
	    		tvRecentMsg = (TextView) baseView.findViewById(R.id.tv_recent_msg);
	        }
	        return tvRecentMsg;
	    }
	    
	    public TextView getTvLastTime(){
	    	if (tvLastTime == null) {
	    		tvLastTime = (TextView) baseView.findViewById(R.id.tv_msg_item_time);
	        }
	        return tvLastTime;
	    }
	    public TextView getTvUnReadQuantity(){
	    	if (tvUnReadQuantity == null) {
	    		tvUnReadQuantity = (TextView) baseView.findViewById(R.id.tv_msg_has_unread);
	        }
	        return tvUnReadQuantity;
	    }
	    public ImageView getIvHead() {
	        if (ivHead == null) {
	        	ivHead = (ImageView) baseView.findViewById(R.id.iv_msg_item_icon);
	        }
	        return ivHead;
	    }
	    public Button getBtnMsgDelete(){
	    	if (btnMsgDelete == null) {
	    		btnMsgDelete = (Button) baseView.findViewById(R.id.btn_msg_delete);
	        }
	        return btnMsgDelete;
	    }
	    
}