package com.chending.ttsports.model;

import com.chaowen.yixin.R;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewCacheFriend {

	    private View baseView;
	    private TextView textView;
	    private ImageView imageView;
	    private TextView signature;
	    public ViewCacheFriend(View baseView) {
	        this.baseView = baseView;
	    }

	    public TextView getTextView() {
	        if (textView == null) {
	            textView = (TextView) baseView.findViewById(R.id.iv_friend_item_username);
	        }
	        return textView;
	    }
	    public TextView getSignature() {
	        if (signature == null) {
	        	signature = (TextView) baseView.findViewById(R.id.tv_friend_signature);
	        }
	        return signature;
	    }
	    public ImageView getImageView() {
	        if (imageView == null) {
	            imageView = (ImageView) baseView.findViewById(R.id.iv_friend_item_icon);
	        }
	        return imageView;
	    }

}