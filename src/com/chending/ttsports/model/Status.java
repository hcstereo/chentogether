package com.chending.ttsports.model;

import java.util.Date;

public class Status {
	private int statusId;
	private int userId;
	private Date createTime;
	private double distance;
	private double calories;
	private int runTime;
	private int type;
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public double getCalories() {
		return calories;
	}
	public void setCalories(double calories) {
		this.calories = calories;
	}
	public int getRunTime() {
		return runTime;
	}
	public void setRunTime(int runTime) {
		this.runTime = runTime;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Status(int statusId, int userId, Date createTime, double distance,
			double calories, int runTime, int type) {
		super();
		this.statusId = statusId;
		this.userId = userId;
		this.createTime = createTime;
		this.distance = distance;
		this.calories = calories;
		this.runTime = runTime;
		this.type = type;
	}
	public Status() {
		super();
	}
	
	
}
