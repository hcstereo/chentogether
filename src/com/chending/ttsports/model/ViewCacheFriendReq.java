package com.chending.ttsports.model;

import com.chaowen.yixin.R;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewCacheFriendReq {

	    private View baseView;
	    private TextView ivReqItemUsername;
	    private ImageView ivReqItemIcon;
	    private TextView signature;
	    private Button btnReqAgree;
	    private TextView tvReqHasAgree;
	    
	    public ViewCacheFriendReq(View baseView) {
	        this.baseView = baseView;
	    }

	    public TextView getIvReqItemUsername() {
	        if (ivReqItemUsername == null) {
	        	ivReqItemUsername = (TextView) baseView.findViewById(R.id.iv_req_item_username);
	        }
	        return ivReqItemUsername;
	    }
	    public TextView getTvReqHasAgree() {
	        if (tvReqHasAgree == null) {
	        	tvReqHasAgree = (TextView) baseView.findViewById(R.id.tv_req_has_agree);
	        }
	        return ivReqItemUsername;
	    }
	    public TextView getSignature() {
	        if (signature == null) {
	        	signature = (TextView) baseView.findViewById(R.id.tv_req_signature);
	        }
	        return signature;
	    }
	    
	    public ImageView getIvReqItemIcon() {
	        if (ivReqItemIcon == null) {
	        	ivReqItemIcon = (ImageView) baseView.findViewById(R.id.iv_req_item_icon);
	        }
	        return ivReqItemIcon;
	    }
	    public Button getBtnReqAgree() {
	    	if (btnReqAgree == null) {
	    		btnReqAgree = (Button) baseView.findViewById(R.id.btn_req_agree);
	        }
	        return btnReqAgree;
		}
	    

}