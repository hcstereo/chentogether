package com.chending.ttsports.model;


public class ChatMsgEntity {
    @SuppressWarnings("unused")
	private static final String TAG = ChatMsgEntity.class.getSimpleName();

    private String name;

    private String date;

    private String text;

    private String imagUrl;
    
    private boolean isComMeg = true;
    
    private boolean isSameTime=false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImagUrl() {
		return imagUrl;
	}

	public void setImagUrl(String imagUrl) {
		this.imagUrl = imagUrl;
	}

	public boolean isComMeg() {
		return isComMeg;
	}

	public void setComMeg(boolean isComMeg) {
		this.isComMeg = isComMeg;
	}

	public ChatMsgEntity() {
    }
    
    public boolean isSameTime() {
		return isSameTime;
	}

	public void setSameTime(boolean isSameTime) {
		this.isSameTime = isSameTime;
	}

	
	
	public ChatMsgEntity(String name, String date, String text,
			boolean isComMeg, boolean isSameTime) {
		super();
		this.name = name;
		this.date = date;
		this.text = text;
		this.isComMeg = isComMeg;
		this.isSameTime = isSameTime;
	}
}
