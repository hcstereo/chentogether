package com.chending.ttsports.protocol;

import java.io.File;
import com.chending.ttsports.common.BcsOperation;

public class UploadProtocol extends BaseProtocol{
	
	public boolean uploadFile(File file){
		try {
			BcsOperation bcsOperation=BcsOperation.getInstance();
			bcsOperation.putObjectByFile(file);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
