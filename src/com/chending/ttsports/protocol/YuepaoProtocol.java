package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import com.chending.ttsports.VO.FriendNearVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.List_LineInfo;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
/*
 * 修改用户的个人信息
 */
public class YuepaoProtocol extends BaseProtocol{
	private final static String url = Constant.SERVER_ADDRESS+"yuepao!getFriendNear";
	private static final double EARTH_RADIUS = 6378137;//计算两点距离所需常量
	private Gson gson = new Gson();
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private SharedPreferencesTool preferencesTool;
	public List<FriendNearVO> getFriendNear(int areaNo,Context context) {
		List<FriendNearVO> friendNearVOs=null;
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(context);
		FriendNearVO friendNearVO1=null;
		try {
			addNameValuePair("areaNo", String.valueOf(areaNo));
			pack(url);
			parse();
			friendNearVOs=friendNearJSON();
			for(FriendNearVO friendNearVO:friendNearVOs){//设置距离
				String start=String.valueOf(friendNearVO.getLatitude())+","+String.valueOf(friendNearVO.getLongitude());
				String end=String.valueOf(preferencesTool.getUserPrefences().get("latitude"))+","+String.valueOf(preferencesTool.getUserPrefences().get("longitude"));
				List_LineInfo item=new List_LineInfo(end, start);
				friendNearVO.setDisFrom(calDis(item));
				if(friendNearVO.getUserId()==Integer.parseInt(preferencesTool.getUserPrefences().get("userId"))){
					friendNearVO1=friendNearVO;
				}
			}
			Collections.sort(friendNearVOs);//按距离排序
		} catch (Exception e) {
			Log.v("Exception", "Message:获取附近的人出错    method:YuepaoProtocol.getFriendNear()");
			e.printStackTrace();
		}
		friendNearVOs.remove(friendNearVO1);
		return friendNearVOs;
	}

	private List<FriendNearVO> friendNearJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("friendNearJson").toString(),
				new TypeToken<List<FriendNearVO>>(){}.getType());
	}
	private double calDis(List_LineInfo item){
		String[] endlatlng = item.getEnd_latlng().split(",");
		double endLng = Double.parseDouble(endlatlng[1]);
 		double endLat = Double.parseDouble(endlatlng[0]);
 		String[] startlatlng = item.getStart_latlng().split(",");
 		double startLng = Double.parseDouble(startlatlng[1]);
 		double startLat = Double.parseDouble(startlatlng[0]);
 		double radLat1 = rad(startLat);
        double radLat2 = rad(endLat);
        double a = radLat1 - radLat2;
        double b = rad(startLng) - rad(endLng);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + 
         Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
 		return s;
	}
	//计算距离调用的方法
	private static double rad(double d)
    {
       return d * Math.PI / 180.0;
    }
}
