package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.google.gson.JsonSyntaxException;
/*
 * 修改用户的个人信息
 */
public class InfoProtocol extends BaseProtocol{
	private final static String url = Constant.SERVER_ADDRESS+"user!changeInfo";
	private final static String url1 = Constant.SERVER_ADDRESS+"user!changeLoc";
	private String isChangeInfoSuc="false";
	private SharedPreferencesTool sharedPreferencesTool;
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public boolean changeInfo(User user, Context context) {
		sharedPreferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(context);
		try {
			addNameValuePair("userId", String.valueOf(user.getUserId()));
			addNameValuePair("username", user.getUsername());
			addNameValuePair("password", user.getPassword());
			addNameValuePair("gender", user.getGender());
			addNameValuePair("weight", String.valueOf(user.getWeight()));
			addNameValuePair("height", String.valueOf(user.getHeight()));
			addNameValuePair("hobby", user.getHobby());
			addNameValuePair("avatar", user.getAvatar());
			addNameValuePair("signature", user.getSignature());
			addNameValuePair("phone", user.getPhone());
			addNameValuePair("latitude", String.valueOf(user.getLatitude()));
			addNameValuePair("longitude",String.valueOf(user.getLongitude()));
			pack(url);
			parse();
			isChangeInfoSuc=userFromUserJSON();
			if (!isChangeInfoSuc.equals("true")) {
				return false;
			} else {
				Map<String, String> params = new HashMap<String, String>();
				params.put("userId", String.valueOf(user.getUserId()));
				params.put("username", user.getUsername());
				params.put("gender", user.getGender());
				params.put("password", user.getPassword());
				params.put("weight", String.valueOf(user.getWeight()));
				params.put("height", String.valueOf(user.getHeight()));
				params.put("hobby", user.getHobby());
				params.put("avatar", user.getAvatar());
				params.put("phone", user.getPhone());
				params.put("signature", user.getSignature());
				params.put("latitude", String.valueOf(user.getLatitude()));
				params.put("longitude", String.valueOf(user.getLongitude()));
				sharedPreferencesTool.saveUserPrefences(params);
				return true;
			}
		} catch (Exception e) {
			Log.v("Exception", "Message:修改个人信息异常    method:InfoProtocol.changeInfo()");
			e.printStackTrace();
		}
		return false;
	}
	///////////////////////////////////////////////////////////add
	public boolean changeLoc(User user, Context context) {
		sharedPreferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(context);
		try {
			addNameValuePair("userId", String.valueOf(user.getUserId()));
			addNameValuePair("latitude", String.valueOf(user.getLatitude()));
			addNameValuePair("longitude",String.valueOf(user.getLongitude()));
			addNameValuePair("areaNo",String.valueOf(user.getAreaNo()));
			pack(url1);
			parse();
			isChangeInfoSuc=userFromUserJSON();
			if (!isChangeInfoSuc.equals("true")) {
				return false;
			} else {
				Map<String, String> params = new HashMap<String, String>();
				params.put("latitude", String.valueOf(user.getLatitude()));
				params.put("longitude", String.valueOf(user.getLongitude()));
				params.put("areaNo", String.valueOf(user.getAreaNo()));
				sharedPreferencesTool.saveUserPrefences(params);
				return true;
			}
		} catch (Exception e) {
			Log.v("Exception", "Message:修改坐标异常    method:InfoProtocol.changeLoc()");
			e.printStackTrace();
		}
		return false;
	}
	///////////////////////////////////////////////////////////////
	/**
	 * 根据服务器返回的json数据生成user对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private String userFromUserJSON() throws JsonSyntaxException, JSONException {
		String isChangeInfoSuc = (String) new JSONObject(getJSONString()).get("isChangeInfoSuc");
		return isChangeInfoSuc;
	}

}
