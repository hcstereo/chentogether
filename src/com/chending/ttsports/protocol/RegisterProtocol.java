package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.util.DESUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * 验证登陆
 * 
 * @author Xiaona
 * 
 */
public class RegisterProtocol extends BaseProtocol {
	private final static String url = Constant.SERVER_ADDRESS+"user!register";
	private String isRegisterSuc="false";
	private SharedPreferencesTool sharedPreferencesTool;
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public boolean register(User user, Context context) {
		sharedPreferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(context);
		try {
			addNameValuePair("username", user.getUsername());
			System.out.println(DESUtils.encrypt(user.getPassword()));
			String ss=DESUtils.encrypt(user.getPassword());
			addNameValuePair("password", ss);
			addNameValuePair("gender", user.getGender());
			addNameValuePair("weigth", String.valueOf(user.getWeight()));
			addNameValuePair("height", String.valueOf(user.getHeight()));
			addNameValuePair("hobby", user.getHobby());
			addNameValuePair("avatar", user.getAvatar());
			addNameValuePair("signature", user.getSignature());
			addNameValuePair("phone", user.getPhone());
			pack(url);
			parse();
			System.out.println(url);
			isRegisterSuc=isRegSucJSON();
			if (!isRegisterSuc.equals("true")) {
				return false;
			} else {
				User u=userFromUserJSON();
				Map<String, String> params = new HashMap<String, String>();
				params.put("userId", String.valueOf(u.getUserId()));
				params.put("username", u.getUsername());
				params.put("gender", u.getGender());
				params.put("password", u.getPassword());
				params.put("weigth", String.valueOf(u.getWeight()));
				params.put("height", String.valueOf(u.getHeight()));
				params.put("hobby", u.getHobby());
				params.put("avatar", u.getAvatar());
				params.put("phone", u.getPhone());
				params.put("signature", u.getSignature());
				params.put("regTime", dateFormat.format(u.getRegTime()));
				sharedPreferencesTool.saveUserPrefences(params);
				Log.v("tag", "用户"+u.getUsername()+"注册成功");
				return true;
			}
		} catch (Exception e) {
			Log.v("Exception", "Message:注册出现异常    method:RegisterProtocol.register()");
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 根据服务器返回的json数据生成user对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private String isRegSucJSON() throws JsonSyntaxException, JSONException {
		String isRegisterSuc = (String) new JSONObject(getJSONString()).get("isRegisterSuc");
		return isRegisterSuc;
	}
	private User userFromUserJSON() throws JsonSyntaxException, JSONException {
		return new Gson().fromJson(getJSON().getString("userJson").toString(),
				User.class);
	}
}
