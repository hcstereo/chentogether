package com.chending.ttsports.protocol;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

@SuppressLint("SimpleDateFormat")
public class Sportprotocol extends BaseProtocol {
	private SharedPreferencesTool sharedPreferencesTool;
	private final static String url = Constant.SERVER_ADDRESS+"sport!addStatus";
	private Gson gson = new Gson();
	private String isAddSuc = "false"; 
	private SqliteDatabaseHelper databaseHelper;
	public boolean addStatus(Status status, Context context) {
		sharedPreferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(context);
		databaseHelper = new SqliteDatabaseHelper(context);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			System.out.println("sends");
			addNameValuePair("userId",sharedPreferencesTool.getUserPrefences().get("userId"));
			addNameValuePair("distance", String.valueOf(status.getDistance()));
			addNameValuePair("calories", String.valueOf(status.getCalories()));
			addNameValuePair("runTime", String.valueOf(status.getRunTime()));
			
			String dis=sharedPreferencesTool.getUserPrefences().get("sumDis");
			String cal=sharedPreferencesTool.getUserPrefences().get("sumCal");
			String time=sharedPreferencesTool.getUserPrefences().get("sumTime");
			String times=sharedPreferencesTool.getUserPrefences().get("sumTimes");
			Map<String, String> map=new HashMap<String, String>();
			if(dis==null){
				dis="0";
			}
			if(cal==null){
				cal="0";
			}
			if(time==null){
				time="0";
			}
			if(times==null){
				times="0";
			}
			map.put("sumDis", String.valueOf(Integer.parseInt(dis)+(int)status.getDistance()));
			map.put("sumCal", String.valueOf(Integer.parseInt(cal)+(int)status.getCalories()));
			map.put("sumTime", String.valueOf(Integer.parseInt(time)+(int)status.getRunTime()));
			map.put("sumTimes", String.valueOf(Integer.parseInt(times)+1));
			sharedPreferencesTool.saveUserPrefences(map);//运动的总距离总耗时总卡路里
			
			pack(url);
			parse();
			isAddSuc = fromJSON();
			if (isAddSuc.equals("false")) {
				return false;
			}else {
				int statusId=statusIdFromJSON();
				String sql="insert into status values("+statusId+","+sharedPreferencesTool.getUserPrefences().get("userId")+",'"+dateFormat.format(new Date())+"',"+dis+","+cal+","+time+")";
				databaseHelper.execSql(sql);
				return true;
			}
		} catch (Exception e) {
			Log.v("Exception", "Message:插入运动数据异常    method:Sportprotocol.addStatus()");
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 根据服务器返回的json数据生成user对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private String fromJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("isAddSuc").toString(),
				String.class);
	}
	private Integer statusIdFromJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("statusId").toString(),
				Integer.class);
	}
}
