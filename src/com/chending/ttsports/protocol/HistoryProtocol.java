package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.Status;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.sqlite.StatusDaoImpl;
import com.chending.ttsports.util.NetTools;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public class HistoryProtocol extends BaseProtocol{
	private final static String url = Constant.SERVER_ADDRESS+"history!listHis";
	private StatusDaoImpl daoImpl=new StatusDaoImpl();
	private Gson gson = new Gson();
	private SharedPreferencesTool preferencesTool=null;
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private List<Status> status;
	private SqliteDatabaseHelper databaseHelper;
	@SuppressWarnings("unchecked")
	public List<Status> getHistoryList(int userId, Context context) {
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(context);
		databaseHelper=new SqliteDatabaseHelper(context);
		try {
			addNameValuePair("userId", String.valueOf(userId));
			if (NetTools.isNetworkAvailable(context)) {
				pack(url);
				parse();
				saveSumHistory();
				status=statusFromJSON();
				daoImpl.removeAll(new String[]{String.valueOf(userId)}, databaseHelper);//刪除该用户的记录
				daoImpl.saveList(status, new String[]{String.valueOf(userId)}, databaseHelper);//保存到本地
				databaseHelper.closeSqlite();
			}else{
				if(isHasHis(context,userId)){
					status=(List<Status>) daoImpl.getFromSQLite(new String[]{String.valueOf(userId)} , databaseHelper);
					databaseHelper.closeSqlite();
				}
			}
		} catch (Exception e) {
			if(isHasHis(context,userId)){
				status=(List<Status>) daoImpl.getFromSQLite(new String[]{String.valueOf(userId)} , databaseHelper);
				databaseHelper.closeSqlite();
			}
			Log.v("Exception", "Message:获取运动历史记录出现异常    method:HistoryProtocol.getHistoryList()");
			e.printStackTrace();
		}
		return status;
	}
	private List<Status> statusFromJSON() throws JsonSyntaxException, JSONException{
		return gson.fromJson(getJSON().getString("statusJson").toString(),
				new TypeToken<List<Status>>(){}.getType());
	}
	
	private void saveSumHistory() throws JsonSyntaxException, JSONException{
		String sumDis=(String) new JSONObject(getJSONString()).get("sumDis");
		String sumTime=(String) new JSONObject(getJSONString()).get("sumTime");
		String sumCal=(String) new JSONObject(getJSONString()).get("sumCal");
		String sumTimes=(String) new JSONObject(getJSONString()).get("sumTimes");
		Map<String, String> map=new HashMap<String, String>();
		map.put("sumDis", sumDis);
		map.put("sumCal", sumCal);
		map.put("sumTime", sumTime);
		map.put("sumTimes", sumTimes);
		preferencesTool.saveUserPrefences(map);
	}
	/**
	 * 
	 */
	public boolean isHasHis(Context context,int userId){
		SqliteDatabaseHelper databaseHelper=new SqliteDatabaseHelper(context);
		String sql="select * from status where userId=?";
		Cursor cursor=databaseHelper.execQuery(sql, new String[]{String.valueOf(userId)});
		if(!cursor.moveToFirst()){
			databaseHelper.closeSqlite();
			return false;
		}
		databaseHelper.closeSqlite();
		return true;
	}
	public List<Status> getFromDB(Context context,int userId) throws ParseException{
		
		String sql="select * from status where userId=? order by createTime desc";
		Cursor cursor=databaseHelper.execQuery(sql, new String[]{String.valueOf(userId)});
		List<Status> status=new Vector<Status>();
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			Status status2=new Status();
			status2.setStatusId(cursor.getInt(0));
			status2.setUserId(cursor.getInt(1));
			status2.setCreateTime(dateFormat.parse(cursor.getString(2)));
			status2.setDistance(cursor.getDouble(3));
			status2.setCalories(cursor.getDouble(4));
			status2.setRunTime(cursor.getInt(5));
			status.add(status2);
			cursor.moveToNext();
		}
		databaseHelper.closeSqlite();
		return status;
	}
	
	public List<Status> sortList(String orderType,Context context){
		preferencesTool=SharedPreferencesTool.getSharedPreferencesToolInstance(context);
		SqliteDatabaseHelper databaseHelper=new SqliteDatabaseHelper(context);
		Map<String, String> map=preferencesTool.getUserPrefences();
		int userId=Integer.parseInt(map.get("userId"));
		String sql="select * from status where userId="+userId+" order by "+orderType+" desc";
		Cursor cursor=databaseHelper.execQuery(sql,null);
		List<Status> status=new Vector<Status>();
		//cursor.moveToFirst();
		while(cursor.moveToNext()){
			Status status2=new Status();
			status2.setStatusId(cursor.getInt(0));
			status2.setUserId(cursor.getInt(1));
			try {
				status2.setCreateTime(dateFormat.parse(cursor.getString(2)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			status2.setDistance(cursor.getDouble(3));
			status2.setCalories(cursor.getDouble(4));
			status2.setRunTime(cursor.getInt(5));
			status.add(status2);
		}
		databaseHelper.closeSqlite();
		return status;
	}
}
