package com.chending.ttsports.protocol;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseProtocol {
	private StringBuilder sb = new StringBuilder();
	private HttpClient httpClient;
	private HttpPost httpRequest;
	private HttpResponse response;
	private List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

	public BaseProtocol() {
		httpClient = new DefaultHttpClient();
	}

	/**
	 * 向服务器端发送请求
	 * 
	 * @param url
	 * @throws Exception
	 */
	protected void pack(String url) throws Exception {
		httpClient = new DefaultHttpClient();
		httpRequest = new HttpPost(url);
		//httpRequest.setHeader("", "");
		UrlEncodedFormEntity entity=new UrlEncodedFormEntity(nameValuePair,HTTP.UTF_8);
		httpRequest.setEntity(entity);
		response = httpClient.execute(httpRequest);
	}
	/**
	 * 上传文件
	 * @param url
	 * @param file1
	 * @throws Exception
	 */
	protected void  packFile(String url,File file) throws Exception{
		httpClient = new DefaultHttpClient(); 
		HttpPost httppost = new HttpPost(url); 
		FileBody fileData = new FileBody(file);
        StringBody fileName = new StringBody("a.jpg", ContentType.MULTIPART_FORM_DATA);
        HttpEntity requestEntity = MultipartEntityBuilder.create()
                .addPart("fileData", fileData)
                .addPart("fileName", fileName)
                .build();
        httppost.setEntity(requestEntity);   
        response = httpClient.execute(httppost);
	}
	/**
	 * 得到返回数据
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	protected void parse() throws Exception {
		// TODO 状态处理 500 200
		if (response.getStatusLine().getStatusCode() == 200) {
			BufferedReader bufferedReader2 = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			for (String s = bufferedReader2.readLine(); s != null; s = bufferedReader2
					.readLine()) {
				sb.append(s);
			}
		}
	}

	/**
	 * 向服务器发送信息
	 * 
	 * @param key
	 * @param value
	 */
	public void addNameValuePair(String key, String value) {
		nameValuePair.add(new BasicNameValuePair(key, value));
	}
	
	/**
	 * 返回JSONObject对象数据模型
	 * 
	 * @return
	 * @throws JSONException
	 */
	public JSONObject getJSON() throws JSONException {
		return new JSONObject(sb.toString());
	}
	protected String getJSONString(){
		return sb.toString();
	}
	public void setStringBuilder(){
		sb=new StringBuilder();
	}
}


