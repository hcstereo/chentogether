package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.chending.ttsports.VO.ChartsVO;
import com.chending.ttsports.common.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public class ChartsProtocol extends BaseProtocol{
	private final static String url = Constant.SERVER_ADDRESS+"charts!listCharts";
	private Gson gson = new Gson();
	private List<ChartsVO> chartsVOs=new ArrayList<ChartsVO>();
	
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public List<ChartsVO> getChartsList(Context context) {
		try {
			pack(url);
			parse();
			chartsVOs=fromChartsJSON();
			System.out.println("size---------"+chartsVOs.size());
		} catch (Exception e) {
			Log.v("Exception", "Message:获取朋友列表出现异常    method:LoginProtocol.checkLogin()");
			e.printStackTrace();
		}
		return chartsVOs;
	}

	/**
	 * 根据服务器返回的json数据生成list对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private List<ChartsVO> fromChartsJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("charts").toString(),
				new TypeToken<List<ChartsVO>>(){}.getType());
	}
}
