package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.model.User;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public class FriendsProtocol extends BaseProtocol{
	private final static String url1 = Constant.SERVER_ADDRESS+"friends!listFriends";
	private final static String url2=Constant.SERVER_ADDRESS+"friends!searchFriend";
	private final static String url3=Constant.SERVER_ADDRESS+"friends!addFriend";
	private final static String url4=Constant.SERVER_ADDRESS+"friends!friendReq";
	private Gson gson = new Gson();
	private List<FriendsVO> friendsVOs=null;
	
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	public List<FriendsVO> getFriendList(int userId, Context context) {
		try {
			addNameValuePair("userId", String.valueOf(userId));
			pack(url1);
			parse();
			friendsVOs=userFromFriendsJSON();
			System.out.println(friendsVOs.size());
		} catch (Exception e) {
			Log.v("Exception", "Message:获取朋友列表出现异常    method:FriendsProtocol.getFriendList()");
			e.printStackTrace();
		}
		return friendsVOs;
	}
	public User searchFriend(String username){
		User user=new User();
		try {
			addNameValuePair("username", username);
			pack(url2);
			parse();
			user=friendFromJSON();
			System.out.println(user.getAreaNo());
		} catch (Exception e) {
			Log.v("Exception", "Message:搜索朋友出现异常    method:FriendsProtocol.searchFriend()");
			e.printStackTrace();
		}
		return user;
	}
	public boolean addFriend(int hostId,int friendId){
		try {
			addNameValuePair("hostId", String.valueOf(hostId));
			addNameValuePair("friendId", String.valueOf(friendId));
			System.out.println(String.valueOf(hostId));
			System.out.println(String.valueOf(friendId));
			pack(url3);
			parse();
			String isAddFriendSuc=checkAddFromJSON();
			if(Boolean.parseBoolean(isAddFriendSuc)){
				return true;
			}
			System.out.println(friendsVOs.size());
		} catch (Exception e) {
			Log.v("Exception", "Message:添加朋友出现异常    method:FriendsProtocol.addFriend()");
			e.printStackTrace();
		}
		return false;
	}
	public boolean dealFriendReq(Context context,int relId,int relState){
		try {
			SqliteDatabaseHelper databaseHelper=new SqliteDatabaseHelper(context);
			addNameValuePair("relId", String.valueOf(relId));
			addNameValuePair("relState", String.valueOf(relState));
			pack(url4);
			parse();
			String agreeAdd=agreeAddFromJSON();
			if(agreeAdd.equals("true")){
				databaseHelper.execSql("delete from friendrel where relId="+relId);//删除sqlite中的该条记录
				return true;
			}
		} catch (Exception e) {
			Log.v("Exception", "Message:处理好友请求出现异常    method:FriendsProtocol.dealFriendReq()");
			e.printStackTrace();
			return false;
		}
		return false;
	}
	/**
	 * 根据服务器返回的json数据生成list对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private List<FriendsVO> userFromFriendsJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("friendsJson").toString(),
				new TypeToken<List<FriendsVO>>(){}.getType());
	}
	private User friendFromJSON() throws JsonSyntaxException, JSONException{
		return gson.fromJson(getJSON().getString("userJson").toString(),
				User.class);
	}
	private String checkAddFromJSON() throws JsonSyntaxException, JSONException{
		return gson.fromJson(getJSON().getString("isAddFriendSuc").toString(),
				String.class);
	}
	private String agreeAddFromJSON() throws JsonSyntaxException, JSONException{
		return gson.fromJson(getJSON().getString("agreeAddFriendSuc").toString(),
				String.class);
	}
}
