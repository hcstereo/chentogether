package com.chending.ttsports.protocol;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.chending.ttsports.VO.ChatMsgVO;
import com.chending.ttsports.common.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class ChatProtocol extends BaseProtocol{
	private final static String url = Constant.SERVER_ADDRESS+"chat!sendMessage";
	private Gson gson = new Gson();
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public boolean sendMessage(ChatMsgVO  chatMsgVO, Context context) {
		addNameValuePair("fromUsername", chatMsgVO.getFromUsername());
		addNameValuePair("toUsername", chatMsgVO.getToUsername());
		addNameValuePair("msgContent", chatMsgVO.getMsgContent());
		addNameValuePair("msgType", String.valueOf(chatMsgVO.getMsgType()));
		addNameValuePair("createTimeStr", dateFormat.format(chatMsgVO.getCreateTime()));
		try {
			pack(url);
			parse();
			if(blFromJSON()){
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v("Exception", "发送信息到服务器出现异常！");
		}
		return false;
	}

	/**
	 * 根据服务器返回的json数据生成user对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private boolean blFromJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("isSendSuc").toString(),
				Boolean.class);
	}
}
