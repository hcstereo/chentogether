package com.chending.ttsports.protocol;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import org.json.JSONException;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.os.Vibrator;
import android.util.Log;
import com.chending.ttsports.VO.ChatMsgVO;
import com.chending.ttsports.VO.FriendRelVO;
import com.chending.ttsports.VO.FriendsVO;
import com.chending.ttsports.common.Constant;
import com.chending.ttsports.common.SharedPreferencesTool;
import com.chending.ttsports.model.User;
import com.chending.ttsports.service.ChatService;
import com.chending.ttsports.service.FriendService;
import com.chending.ttsports.sqlite.FriendReqDaoImpl;
import com.chending.ttsports.sqlite.IModelDao;
import com.chending.ttsports.sqlite.ModelDaoFactory;
import com.chending.ttsports.sqlite.SqliteDatabaseHelper;
import com.chending.ttsports.util.ModelTool;
import com.chending.ttsports.util.NetTools;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * 验证登陆
 * 
 * @author Xiaona
 * 
 */
public class LoginProtocol extends BaseProtocol {
	private static final String TAG="LoginProtocol";
	private final static String url = Constant.SERVER_ADDRESS + "user!login";
	private static int MOOD_NOTIFICATIONS = 1;
	private Gson gson = new Gson();
	private User userJson;
	private List<ChatMsgVO> chatMsgVOs = new Vector<ChatMsgVO>();
	private SharedPreferencesTool sharedPreferencesTool;
	@SuppressLint("SimpleDateFormat")
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private List<FriendRelVO> friendRelVOs = new Vector<FriendRelVO>();
	private Notification notification;
	private NotificationManager manager;
	private Context context;
	private PendingIntent intent;
	private IModelDao friendInfoDao=ModelDaoFactory.createModelDao("friendsinfo");
	private FriendReqDaoImpl daoImpl=new FriendReqDaoImpl();
	public LoginProtocol(Context context, Notification notification,
			NotificationManager manager,PendingIntent intent) {
		super();
		this.notification = notification;
		this.context = context;
		this.manager = manager;
		this.intent=intent;
	}
	
	public LoginProtocol() {
		super();
	}

	@SuppressWarnings("unchecked")
	@SuppressLint("SimpleDateFormat")
	public boolean checkLogin(User user, Context context, boolean isFirstLogin) {
		Constant.TAG_USER_HAS_LOGIN=true;
		Log.v(TAG, "url = "+url);
		sharedPreferencesTool = SharedPreferencesTool
				.getSharedPreferencesToolInstance(context);
		Vibrator vibrator = (Vibrator) context.getApplicationContext()
				.getSystemService(Service.VIBRATOR_SERVICE);//震动
		String nameString = sharedPreferencesTool.getUserPrefences().get(
				"username");
		SqliteDatabaseHelper databaseHelper = new SqliteDatabaseHelper(
				context);
		if(user!=null&&user.getUsername()!=null&&user.getPassword()!=null){
			try {
				addNameValuePair("username", user.getUsername());
				addNameValuePair("password", user.getPassword());
				if (isFirstLogin) {//第一次登录，需要获取朋友列表
					Constant.USER_HAS_LOGIN=false;
					if (NetTools.isNetworkAvailable(context)) {//如果网络连接正常，那么向服务器获取朋友聊表，否则加载本地数据
						addNameValuePair("isFirstLogin",
								String.valueOf(isFirstLogin));
						pack(url);
						parse();
						Log.d(TAG, "for first login the message = "+getJSONString());
						userJson = userFromUserJSON();
						chatMsgVOs = msgFromJSON();
						friendRelVOs = friendRelFromJSON();
						if(chatMsgVOs!=null){
							if (chatMsgVOs.size() > 0) {
								vibrator.vibrate(new long[] { 180, 350 }, -1);//如果未读信息大于0则震动
							}
							ChatService.chatMsgVOs.addAll(chatMsgVOs);
						}
						if(friendRelVOs!=null&&friendRelVOs.size()>0){//清空本地朋友请求数据库，然后添加到本地数据库
							daoImpl.removeAll(new String[]{String.valueOf(user.getUserId())}, databaseHelper);//清空
							daoImpl.saveList(friendRelVOs, new String[]{String.valueOf(user.getUserId())}, databaseHelper);//保存到本地
							FriendService.friendRelVOs.clear();
							FriendService.friendRelVOs.addAll(friendRelVOs);
						}
						if (userJson == null) {
							databaseHelper.closeSqlite();
							return false;
						} else {
							// 第一次登陆时，先把本地朋友列表清空，然后把从服务器获取的朋友列表存入本地sqlite数据库
							friendInfoDao.removeAll(new String[]{user.getUsername()}, databaseHelper);
							List<FriendsVO> friendsVOs=userFromFriendsJSON();
							FriendService.friendsVOs.addAll(friendsVOs);
							friendInfoDao.saveList(friendsVOs, new String[]{user.getUsername()}, databaseHelper);
							databaseHelper.closeSqlite();
							sharedPreferencesTool.saveUser(userJson);//保存到本地
							Log.d(TAG, "用户" + userJson.getUsername() + "登陆成功");
							return true;
						}
					}else{
						//从本地回复朋友列表
						List<FriendsVO> lstFri=(List<FriendsVO>) friendInfoDao.getFromSQLite(new String[]{nameString}, databaseHelper);
						if(lstFri!=null){
							FriendService.friendsVOs=lstFri;
						}
						//从本地回复好友请求
						List<FriendRelVO> lstRel=(List<FriendRelVO>) daoImpl.getFromSQLite(new String[]{String.valueOf(user.getUserId())},databaseHelper);
						if(lstRel!=null){
							FriendService.friendRelVOs=lstRel;
						}
						databaseHelper.closeSqlite();
					}
				} else if (user.getUsername().equals(nameString)) {//不是第一次登录
					addNameValuePair("isFirstLogin", String.valueOf(isFirstLogin));
					pack(url);
					parse();
					Log.d(TAG, "for login the message = "+getJSONString());
					friendRelVOs = friendRelFromJSON();
					chatMsgVOs = msgFromJSON();
					if (chatMsgVOs.size() > 0) {
						vibrator.vibrate(new long[] { 180, 300 }, -1);// 震动
						for (ChatMsgVO chatMsgVO : chatMsgVOs) {
							if(Constant.IS_IN_CHAT_ACTIVITY==false){
								notifyUser(chatMsgVO.getMsgContent(),chatMsgVO.getFromUsername());//通知栏提醒有新的信息
							}
						}
					}
					if(friendRelVOs!=null&&friendRelVOs.size()>0){
						daoImpl.saveList(friendRelVOs, new String[]{String.valueOf(user.getUserId())}, databaseHelper);
						for (FriendRelVO friendRelVO : friendRelVOs) {
							if (!ModelTool.hasInFriendReqList(
									FriendService.friendRelVOs,
									friendRelVO.getUsername())) {// 如果没有该条信息
								FriendService.friendRelVOs.add(friendRelVO);
							}
						}
					}
					Log.d(TAG, "消息，chatMsgVOs："+chatMsgVOs.size());
					Log.d(TAG, "好友添加请求，friendRelVOs："+friendRelVOs.size());
					ChatService.chatMsgVOs.addAll(chatMsgVOs);
				}
			} catch (Exception e) {
				//从本地回复朋友列表
				List<FriendsVO> lstFri=(List<FriendsVO>) friendInfoDao.getFromSQLite(new String[]{nameString}, databaseHelper);
				if(lstFri!=null){
					FriendService.friendsVOs=lstFri;
				}
				//从本地回复好友请求
				List<FriendRelVO> lstRel=(List<FriendRelVO>) daoImpl.getFromSQLite(new String[]{String.valueOf(user.getUserId())},databaseHelper);
				if(lstRel!=null){
					FriendService.friendRelVOs=lstRel;
				}
				databaseHelper.closeSqlite();
				Log.e(TAG, "Exception "+"Message:登陆或获取信息出现异常    method:LoginProtocol.checkLogin()");
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * 根据服务器返回的json数据生成user对象
	 * 
	 * @return
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private User userFromUserJSON() throws JsonSyntaxException, JSONException {
		return gson.fromJson(getJSON().getString("userJson").toString(),
				User.class);
	}

	private List<FriendsVO> userFromFriendsJSON() throws JsonSyntaxException,
			JSONException {
		return gson.fromJson(getJSON().getString("friendsJson").toString(),
				new TypeToken<List<FriendsVO>>() {
				}.getType());
	}

	private List<ChatMsgVO> msgFromJSON() throws JsonSyntaxException,
			JSONException {
		return gson.fromJson(getJSON().getString("chatJson").toString(),
				new TypeToken<List<ChatMsgVO>>() {
				}.getType());
	}

	private List<FriendRelVO> friendRelFromJSON() throws JsonSyntaxException,
			JSONException {
		return gson.fromJson(getJSON().getString("friendRelJson").toString(),
				new TypeToken<List<FriendRelVO>>() {
				}.getType());
	}

	@SuppressWarnings("deprecation")
	private void notifyUser(String content,String username) {
		notification.tickerText=username+":"+content;
		notification.setLatestEventInfo(context,username, content, intent);
		manager.notify(MOOD_NOTIFICATIONS, notification);
	}
}
