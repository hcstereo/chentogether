package com.chending.ttsports.sqlite;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SqliteDatabaseHelper {
	private Context context;
	private SQLiteDatabase sqlitedb;
	public SqliteDatabaseHelper(Context context){
		this.context=context;
		this.sqlitedb=this.getSqlite();
		this.initSqlite();
	}
	private void initSqlite(){
		
		String createSqlFriends="CREATE TABLE IF NOT EXISTS friendsinfo ( friendsId Integer,"+
							"username varchar,"+
							"gender varchar,"+
							"height double,"+
							"weight double,"+
							"hobby varchar,"+
							"avatar varchar,"+
							"phone varchar,"+
							"regTime datetime,"+
							"signature varchar,"+
							"localname varchar,"
							+ "primary key(friendsId,localname)"
							+ ")";
		String createSqlMsg="CREATE TABLE IF NOT EXISTS chatmsg ( msgId Integer primary key,"+
							"fromUserId Integer,"+
							"toUserId Integer," + 
							"msgContent text,"+
							"msgType Integer,"+
							"msgStateId Integer,"+
  							"createTime datetime,"+
							"fromUsername varchar,"+
							"toUsername varchar,"+
							"avatar varchar,"+
							"mainUser Integer,"+
							"avatarOp varchar"+
  							")";
		String createSqlStatus="CREATE TABLE IF NOT EXISTS status ( statusId Integer primary key,"+
							"userId Integer,"+
							"createTime datetime,"+
							"distance Double,"+
							"calories Double,"+
							"runTime Integer"+
							")";
		String createSqlFriendRel="CREATE TABLE IF NOT EXISTS friendrel ( relId Integer primary key,"+
				"userId Integer,"+
				"username varchar,"+
				"gender varchar,"+
				"height double,"+
				"weight double,"+
				"hobby varchar,"+
				"avatar varchar,"+
				"phone varchar,"+
				"regTime Date,"+
				"signature varchar,"+
				"latitude double,"+
				"longitude double,"+
				"myId Integer"+
				")";
		this.execSql(createSqlFriends);//朋友列表
		this.execSql(createSqlMsg);//消息表
		this.execSql(createSqlStatus);//运动记录表
		this.execSql(createSqlFriendRel);//运动记录表
	}
	public SQLiteDatabase getSqlite(){
		return this.context.openOrCreateDatabase("ttsports015.db", Context.MODE_PRIVATE, null);
	}
	/**
	 * 执行DDl语句
	 * @param sql
	 */
	public void execSql(String sql){
		this.sqlitedb.execSQL(sql);
	}
	/**
	 * 插入数据
	 * @param sql sql语句（用？代替参数）
	 * @param objects 参数
	 */
	public void execInsertSql(String sql,Object[] objects){
		this.sqlitedb.execSQL(sql,objects);
	}
	/**
	 * 更新操作
	 * @param entity 表名
	 * @param cv ContentValues
	 * @param sql 更新sql语句（用？代替参数）
	 * @param strings 设置参数
	 */
	public void execUpdate(String entity,ContentValues cv,String sql,String[] strings){
		this.sqlitedb.update(entity,cv,sql,strings);
	}
	/**
	 * 查询
	 * @param sql select语句
	 * @param strings 参数
	 * @return 返回游标
	 */
	public Cursor execQuery(String sql,String[] strings){
		Cursor cursor = this.sqlitedb.rawQuery(sql, strings);
//		while (c.moveToNext()) {
//			int _id = c.getInt(c.getColumnIndex("_id"));
//			String name = c.getString(c.getColumnIndex("name"));
//			int age = c.getInt(c.getColumnIndex("age"));
//		}
//		c.close();
		return cursor;
	}
	/**
	 * 删除
	 * @param entity 表名
	 * @param requirement 条件 where
	 * @param strings 参数（？）
	 */
	public void execDelete(String entity,String requirement,String[] strings){
		this.sqlitedb.delete(entity, requirement, strings);
	}
	/**
	 * 返回某个表里面的记录的条数
	 * @param tableName
	 * @return
	 */
	public int execCount(String tableName){
		Cursor cursor = this.sqlitedb.rawQuery("select * from "+tableName, null);
		return cursor.getCount();
	}
	/**
	 * 关闭数据库连接
	 */
	public void closeSqlite(){
		this.sqlitedb.close();
	}
}
