package com.chending.ttsports.sqlite;


public class ModelDaoFactory {
	/**
	 * dao工厂，产生用于操作本地数据库的dao对象
	 * @param modelName
	 * @return
	 */
	public static IModelDao createModelDao(String modelName){
		//虽然Java7已经支持Switch里面使用String了，但是这里暂时还是用if/else
		IModelDao dao=null;
		if(modelName.equals("friendsinfo")){
			dao=new FriendsVODaoImpl();
		}else if(modelName.equals("chatMsg")){
			//其他的dao实现
		}
		return dao;
	}
}
