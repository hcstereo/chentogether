package com.chending.ttsports.sqlite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import android.annotation.SuppressLint;
import android.database.Cursor;
import com.chending.ttsports.VO.FriendRelVO;
import com.chending.ttsports.model.Status;

@SuppressLint("SimpleDateFormat")
public class FriendReqDaoImpl implements IModelDao{
	private SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Override
	public List<?> getFromSQLite(String[] strs,
			SqliteDatabaseHelper databaseHelper) {
		String sql="select * from friendrel where myId=?";
		Cursor cursor=databaseHelper.execQuery(sql,strs);
		List<FriendRelVO> friendRelVOs=new Vector<FriendRelVO>();
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			FriendRelVO friendRelVO=new FriendRelVO();
			friendRelVO.setRelId(cursor.getInt(0));
			friendRelVO.setUserId(cursor.getInt(1));
			friendRelVO.setUsername(cursor.getString(2));
			friendRelVO.setGender(cursor.getString(3));
			friendRelVO.setHeight(cursor.getDouble(4));
			friendRelVO.setWeight(cursor.getDouble(5));
			friendRelVO.setHobby(cursor.getString(6));
			friendRelVO.setAvatar(cursor.getString(7));
			friendRelVO.setPhone(cursor.getString(8));
			try {
				friendRelVO.setRegTime(dateFormat.parse(cursor.getString(9)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			friendRelVO.setSignature(cursor.getString(10));
			friendRelVO.setLatitude(cursor.getDouble(11));
			friendRelVO.setLongitude(cursor.getDouble(12));
			friendRelVOs.add(friendRelVO);
			cursor.moveToNext();
		}
		return friendRelVOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveList(List<?> list, String[] strs,
			SqliteDatabaseHelper databaseHelper) {
		for(FriendRelVO friendRelVO:(List<FriendRelVO>)list){
			databaseHelper.execInsertSql("insert into friendrel (relId,userId,username,gender,height,weight,hobby,avatar,phone,regTime,signature,latitude,longitude,myId) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new String[]{String.valueOf(friendRelVO.getRelId()),String.valueOf(friendRelVO.getUserId()),
					friendRelVO.getUsername(),friendRelVO.getGender(),String.valueOf(friendRelVO.getHeight()),String.valueOf(friendRelVO.getHeight()),
					friendRelVO.getHobby(),friendRelVO.getAvatar(),friendRelVO.getAvatar(),dateFormat.format(friendRelVO.getRegTime()),friendRelVO.getSignature(),
					String.valueOf(friendRelVO.getLatitude()),String.valueOf(friendRelVO.getLongitude()),strs[0]
			});
		}
	}

	@Override
	public void removeAll(String[] str, SqliteDatabaseHelper databaseHelper) {
		databaseHelper.execSql("delete from friendrel");
	}

	@Override
	public void remove(String[] str, SqliteDatabaseHelper databaseHelper) {
		databaseHelper.execSql("delete from stfriendrelatus where userId="+str[0]);
	}
}
