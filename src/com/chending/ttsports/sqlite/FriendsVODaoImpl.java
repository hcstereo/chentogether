package com.chending.ttsports.sqlite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.util.Log;

import com.chending.ttsports.VO.FriendsVO;

@SuppressLint("SimpleDateFormat")
public class FriendsVODaoImpl implements IModelDao{
	private static final String TAG="FriendsVOModelImpl";
	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	@SuppressWarnings("unchecked")
	@Override
	public void saveList(List<?> list,String[] strs,SqliteDatabaseHelper databaseHelper) {
		for (FriendsVO f : (List<FriendsVO>)list) {
			databaseHelper
					.execInsertSql(
							"insert into friendsinfo values(?,?,?,?,?,?,?,?,?,?,?)",
							new String[] {
									String.valueOf(f
											.getFriendsId()),
									f.getUsername(),
									f.getGender(),
									String.valueOf(f
											.getHeight()),
									String.valueOf(f
											.getWeight()),
									f.getHobby(),
									f.getAvatar(),
									f.getPhone(),
									dateFormat.format(f
											.getRegTime()),
									f.getSignature(),
									strs[0] });
		}
	}

	@Override
	public List<?> getFromSQLite(String[] strs,SqliteDatabaseHelper databaseHelper) {
		List<FriendsVO> friendsVOs=new Vector<FriendsVO>();
		Cursor cursor = databaseHelper.execQuery(
				"select * from friendsinfo where localname=?", strs);// 找到该用户的朋友列表
		Log.v(TAG, "count is " + cursor.getCount());
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor
				.moveToNext()) {
			String username = cursor.getString(cursor
					.getColumnIndex("username"));
			String gender = cursor.getString(cursor
					.getColumnIndex("gender"));
			double height = cursor.getDouble(cursor
					.getColumnIndex("height"));
			double weight = cursor.getDouble(cursor
					.getColumnIndex("weight"));
			String hobby = cursor.getString(cursor
					.getColumnIndex("hobby"));
			String avatar = cursor.getString(cursor
					.getColumnIndex("avatar"));
			String phone = cursor.getString(cursor
					.getColumnIndex("phone"));
			String regTime = cursor.getString(cursor
					.getColumnIndex("regTime"));
			String signature = cursor.getString(cursor
					.getColumnIndex("signature"));
			Integer friendsId = cursor.getInt(cursor
					.getColumnIndex("friendsId"));
			try {
				FriendsVO friendsVO = new FriendsVO(username, gender,
						height, weight, hobby, avatar, phone,
						dateFormat.parse(regTime), signature, friendsId);
				friendsVOs.add(friendsVO);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return friendsVOs;
	}
	/**
	 * 删除用户好友列表
	 */
	@Override
	public void removeAll(String[] str,SqliteDatabaseHelper databaseHelper) {
		databaseHelper.execSql("delete from friendsinfo where localname='"+str[0]+"'");
		Log.d(TAG, "sqlite本地朋友列表删除成功！");
	}

	@Override
	public void remove(String[] str, SqliteDatabaseHelper databaseHelper) {
		
	}
	
}
