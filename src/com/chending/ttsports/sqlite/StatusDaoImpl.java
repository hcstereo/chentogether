package com.chending.ttsports.sqlite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Vector;
import android.annotation.SuppressLint;
import android.database.Cursor;
import com.chending.ttsports.model.Status;

@SuppressLint("SimpleDateFormat")
public class StatusDaoImpl implements IModelDao{
	private SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Override
	public List<?> getFromSQLite(String[] strs,
			SqliteDatabaseHelper databaseHelper) {
		String sql="select * from status where userId=? order by createTime desc";
		Cursor cursor=databaseHelper.execQuery(sql,strs);
		List<Status> status=new Vector<Status>();
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			Status status2=new Status();
			status2.setStatusId(cursor.getInt(0));
			status2.setUserId(cursor.getInt(1));
			try {
				status2.setCreateTime(dateFormat.parse(cursor.getString(2)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			status2.setDistance(cursor.getDouble(3));
			status2.setCalories(cursor.getDouble(4));
			status2.setRunTime(cursor.getInt(5));
			status.add(status2);
			cursor.moveToNext();
		}
		databaseHelper.closeSqlite();
		return status;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void saveList(List<?> list, String[] strs,
			SqliteDatabaseHelper databaseHelper) {
		for(Status stu:(List<Status>)list){
			databaseHelper.execInsertSql("insert into status (statusId,userId,createTime,distance,calories,runTime) values (?,?,?,?,?,?)", new String[]{String.valueOf(stu.getStatusId()),String.valueOf(stu.getUserId()),dateFormat.format(stu.getCreateTime()),String.valueOf(stu.getDistance()),String.valueOf(stu.getCalories()),String.valueOf(stu.getRunTime())});
		}
	}

	@Override
	public void removeAll(String[] str, SqliteDatabaseHelper databaseHelper) {
		databaseHelper.execSql("delete from status where userId="+str[0]);
	}

	@Override
	public void remove(String[] str, SqliteDatabaseHelper databaseHelper) {
		databaseHelper.execSql("delete from status where statusId="+str[0]);
	}

}
