package com.chending.ttsports.sqlite;

import java.util.List;

public interface IModelDao {
	public List<?> getFromSQLite(String[] strs,SqliteDatabaseHelper databaseHelper);//从本地数据库取出list
	public void saveList(List<?> list,String[] strs,SqliteDatabaseHelper databaseHelper);//将数据list存入本地数据库
	public void removeAll(String[] str,SqliteDatabaseHelper databaseHelper);
	public void remove(String[] str,SqliteDatabaseHelper databaseHelper);
}
